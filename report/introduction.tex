\chapter{Introduction}\label{chap:introduction}

Images are a rich source of information merging the explicit (What objects are
in the picture? What attributes do the objects have?) with the implicit (How do
the objects in the picture relate? How do they interact?).  ``A picture is worth
a thousand words'' --- and yet humans can describe images succinctly, even when
they are of poor quality \citep{bachmann1991,oliva2000} or if they are only
visible for an instant \citep{potter1976,fei2007}.

A human can \emph{see} what objects are in an image, \emph{recognise} the
relationships between them, \emph{understand} their connections and
\emph{summarise} the most salient aspects of the image, passing over less
important parts.  For instance, seeing the image in Figure~\ref{fig:example}, a
human prioritised the boy and the interaction he has with the cat but
selectively chose to not describe the clothes the boy is wearing, the dog,
table, sun, tree, grass or sky: the image is about a boy enjoying meeting a cat,
the other aspects are only side concerns.

\begin{figure}
\centering
\includegraphics[height=0.35\textwidth]{example.png}
\par\medskip
Mike is happy to see the orange cat.
\caption{Sample image and description.}
\label{fig:example}
\end{figure}

Describing images \emph{automatically} is a formidable task at the intersection
of computer vision and natural language processing, involving many components
such as object recognition, image segmentation, attribute extraction, content
planning, content selection and surface realisation.

While challenging (perhaps as involved as the larger image-understanding problem
itself) automated image description has a number of powerful and immediate
engineering applications. For instance:

\begin{itemize}
\item Web-based image search-engines retrieve pictures in response to textual
queries.  This is achieved by exploiting meta-data such as the image's file-name
and surrounding text (noisy), human tagging (expensive) or simple visual
features like the image's predominant colour (low-level).  Generating automatic
image descriptions will enable a semantic component within the retrieval model,
thus enabling the user to find images matching a concept rather than a bag of
low-level features.  This has direct applications in the domain of illustration
\citep{barnard2001}: for instance, a journalist could use a semantically-aware
image search engine to find a photo to accompany an article \citep{feng2013}.

\item Automatic image description techniques will furthermore allow users to
search collections of images without any annotation or meta-data (such as
private photo archives).  Some progress has already been made in this area, for
instance, a system by \citet{krizhevsky2012} allows users to retrieve
unannotated images by object-occurrence.  However, this system can only be
thought of as a first step given the evidence that more semantic information
(e.g.\ relationships between objects) is necessary for effective image retrieval
\citep{armitage1997}.

\item Summarising image contents in text also has applications in the domain of
accessibility.  Currently, the visually impaired can use screen-readers to
access images.  However, this relies on content creators to provide manually
created semantic annotations for each image.  Automatic image description
techniques will remove the need for laborious manual labelling and make the vast
amount of existing but unannotated data accessible \citep{ferres2006}.
\end{itemize}

In addition, studying the vision--language interplay will lead to a deeper
understanding of how humans describe images, working towards answering the
question of ``what is important in an image.''  Insights thus-gained can be used
to guide and focus research in computer vision, giving insight on issues such as
which object-detectors might be the most important to build.

\section{Background}\label{sec:background}

A large amount of work towards building automated image understanding systems
has focused on annotating images with keywords
\citep{duygulu2002,lavrenko2003,feng2004,guillaumin2009,makadia2010}.  In this
way, a keyword-generation system might annotate the image in
Figure~\ref{fig:example} with the words: \emph{cat}, \emph{orange} and
\emph{table}.  This conveys some notion about the objects present in
the picture, however the informative details are ambiguous (is the cat orange or
the table?) and do not communicate what the picture really is about (how are the
cat and table related?).  On the other hand, a full-sentence description such as
\emph{The orange cat sits next to the table} can explicitly encode the contents
of the scene (using words) and implicitly communicate the relationships between
the objects (via the relationships between the words).

Prior work attempting to describe images using higher-level concepts such as
phrases, sentences or even multi-sentence free text can roughly be divided along
two main paradigms.  \emph{Transfer-based} or retrieval-based approaches exploit
large collections of parallel images and human-generated descriptions.  New
images are described by finding closely matching images in a collection and
transferring (parts of) their human-generated descriptions onto the unseen
image.  \emph{Generative} approaches on the other hand, combine the responses
of object-detectors with a linguistic model in order to generate completely
novel descriptions for unseen images.

The transfer-based approach has the advantage that it leverages the human's
skill at describing images: descriptions are always grammatical and fluent,
sometimes even poetic and intricately crafted.  However, all retrieved
descriptions share the property that they were originally written for images
distinct from the one that they are now being retrieved to describe.  It is
therefore unlikely that retrieved descriptions will match a new image as well as
novel descriptions custom-made for the pertinent image.  The generative approach
fixes this flaw, but at the cost of having to deal with natural language
generation (an open field with on-going research in and of itself) in addition
to image understanding.

Notable representatives of the transfer-based and generative paradigms include
the following.

% Transfer-based papers

\citet{farhadi2010} represent images and descriptions in a shared
``meaning-space'' where semantics are represented by $\left\{\text{object,
}\text{action, }\text{scene}\right\}$-triplets.  A Markov Random Field
\citep{li1995} is used to map image-features (e.g.\ object detector responses
\citep{felzenszwalb2008}, scene classification responses \citep{everingham2009}
and the gist global image descriptor \citep{oliva2006}) into the meaning-space.
Sentences are mapped into the meaning-space by producing a dependency parse and
lifting verbs to actions, subjects to objects and prepositions to scenes (while
taking word similarities into account).  New images are described by retrieving
close sentences in the meaning-space.  The system is trained on 1,000 realistic
images and 5,000 corresponding descriptions.
\newline
The authors note the difficulty of quantitatively evaluating the quality of the
generated image descriptions and settle on a human study (which finds the model
to perform admirably).  An interesting observation of the paper is that the
intermediate meaning-space representation allows descriptions to contain
information about image-elements which are present and visible but were not
picked up by the computer vision systems  --- image descriptions in natural
language can thus be used to guide and correct computer vision detections.
\newline
The paper's definition of semantics is rather crude and will lead to
image--description mismatches due to the many degrees of freedom that are left
open by only constraining three dimensions of the image descriptions.  Subtler
notions such as spatial relationships for example will only figure in the
produced descriptions by chance.

\citet{ordonez2011} use a simpler approach but a much larger data-set (1 million
images and descriptions found on the web).  Given an unseen image, related
images in the data-set are found using crude vision features such as the gist
global image descriptor and image thumbnails (carrying visual information such
as predominant colours).  The captions of the related images are then re-ranked
by taking image-contents into account (objects, stuff, people and scenes).
\newline
The authors evaluate their work using BLEU \citep{papineni2002} and report that
their model performs better given more data (i.e.\ performance is improved by
using a larger knowledge-base of images and captions with which to compare
unseen images and from which to retrieve candidate descriptions).  The model
also performs better when taking image contents into consideration (i.e.\ making
sure that captions and images are at least somewhat related improves
performance).
\newline
The model produces good results but requires very large amounts of data to work
well.  The lack of an underlying semantic model of images and descriptions makes
it hard to reason about the efficacy of the system and glean wider applications.

\citet{kuznetsova2012} build on \citet{ordonez2011}'s approach and data,
addressing some of the criticism levelled towards the transfer-based image
description paradigm.  Once again, candidate descriptions are lifted from images
containing similar visual elements.  However, the candidates are constrained to
contain a noun phrase, a verb phrase and two prepositional phrases (referring to
regions and stuff).  The phrases within the descriptions and the words within
the phrases are then reordered using integer linear programming (ILP)
\citep{karlof2005} in order to satisfy visual constraints (e.g.\ objects that
are prominently described by humans should be mentioned first) and linguistic
constraints (e.g.\ discourse coherence and ngram cohesion should be satisfied).
The ILP approach mixes and matches human-generated descriptions, thus producing
at least partially novel descriptions for new images.  This reduces the
image--description mismatch of many transfer-based image description systems.
\newline
The model is evaluated (using BLEU and a human study) against
\citet{ordonez2011}'s system and a baseline.  The baseline replaces the ILP
post-processing with a simpler approach based on Hidden Markov Models
\citep{baum1966}.  The authors find that the ILP system outperforms the other
models due to its ability to enforce linguistic discourse constraints, even if
it introduces more grammatical errors and sometimes produces nonsensical
results.
\newline
The system is interesting in that it highlights the benefits of using linguistic
processing to increase description relevance.  Ultimately however, the model
still struggles because the text it uses as inputs may or may not be relevant to
the image at hand.

% Generative papers

\citet{kulkarni2011} present a system that creates novel descriptions for images
by explicitly enumerating all the elements in the picture and their
relationships.  This produces descriptions that are exhaustive but not overly
natural sounding.  The approach is based on a Conditional Random Field (CRF)
\citep{lafferty2001} that is trained to extract meaning-representation pairs
from images.  The meaning representation consists of pairs of nouns with
attributes, linked by a spatial relation (preposition).  The meaning-pairs are
used for surface realisation via an ngram language model and manually
constructed templates.  The CRF uses standard vision inputs (object detectors,
scene classifiers, attribute detectors, spatial relationship detectors) and
simple linguistic inputs mined from text corpora (frequency of attributes,
frequency of positional language, frequency of object--attribute pairs).
\newline
The system is evaluated using BLEU and human judges.  The paper argues that BLEU
is a poor metric to evaluate image descriptions, stating that the metric does
not correlate very well with human judgement.  The paper also finds that using
templates helps to generate more realistic descriptions.  Due to the simplicity
of the natural language processing techniques used, the quality of the generated
descriptions is heavily reliant on good vision detections.

\citet{mitchell2012} treat the vision problem in a similar way as
\citet{kulkarni2011} but use a more flexible description synthesis approach,
taking into account co-occurrence statistics and syntactic structure.  The
language generation system introduced in the paper starts with a set of
vision-generated object-, attribute- and relationship-detections and combines
them into natural language description by using a probabilistic approach growing
syntactic trees bottom-up (i.e.\ reordering words, combining groups of words
into phrases, grouping and de-duplicating similar words, etc.).
\newline
Due to the flexible nature of the syntactic structure imposed on the generated
descriptions, the proposed system outperforms \citet{kulkarni2011} in a human
study.  However, the system does not handle incorrect vision detections and uses
a relatively simple maximum likelihood formulation for the language generation
process.

\citet{krishnamoorthy2013} are primarily interested in understanding videos (not
images) but their approach shares many facets with image description, whence it
is of interest here.  Like other works, the paper uses a simple
$\left\{\text{subject, }\text{verb, }\text{object}\right\}$
meaning-representation for vision detections and a template-based approach for
surface realisation.  The novelty of their approach is the use of a strong
linguistic component.  A language-model prior is used to ground vision
detections; Synonym expansion of candidate words is used to increase the space
of descriptions that are generated and considered for images.
\newline
Evaluation (using BLEU, METEOR \citep{denkowski2011} and humans) reveals that
the strong linguistic grounding helps the model outperform more vision-oriented
approaches.

Beyond transfer-based and generative approaches, full-sentence image description
generation is more rarely treated as a summarisation task (using abstractive or
extractive methods \citep{feng2013} or incorporating domain knowledge
\citep{aker2010}).

In sum, both transfer-based and generative image description approaches are
found to produce reasonable results.  It is somewhat unfortunate that (due to
the fragmentation of data-sets and evaluation methods) there is no standard way
to evaluate and compare systems.  This makes in-depth analysis of the existing
literature difficult.  Some trends do however emerge: more linguistic grounding
generally tends to produce more relevant and more grammatical descriptions.
This motivates the emphasis on language processing of the work presented here.

\section{Contributions}\label{sec:contributions}

The work presented by this report ties in with the generative image description
paradigm.  Our primary contribution is a novel template-based system that
generates sentence-length descriptions for simple images.  The novelty of our
approach is that we learn template structures and how to fill them in from
(potentially noisy) data.  Our model outperforms a non-trivial transfer-based
baseline and generally produces descriptions that are not only adequate but also
highly fluent.

Our image description system is trained on the ``Abstract Scenes Data-Set'' of
10,020 semantically similar clip-art images and 60,396 corresponding
descriptions \citep{zitnick2013}.  The images in the data-set are (trivially)
fully labelled --- the choice of data thus enables us to focus on the semantic
understanding of scenes and on the natural language generation aspects of the
image description task, without having to worry about object recognition and
image segmentation.

As a secondary contribution, we introduce a methodology (based on prior work by
\citet{zitnick2013}) to perform feature selection of visual features and word
features.

We also introduce two baseline systems for the image-description task on the
``Abstract Scenes Data-Set'' and evaluate our template-based model against them.

\section{Related Work}\label{sec:related-work}

Our work closely ties in with the existing literature in the automated image
description community, taking inspiration and learning lessons most specifically
from many authors, including the following.

\citet{kulkarni2011} argue that imposing a grammatical structure on image
descriptions (e.g.\ requiring that every description has to follow a certain
template) produces more readable results than relying on a language model to
ensure well-formedness.  \citet{mitchell2012} find that using grammatical
structures that can adapt to the image being described (instead of forcing all
descriptions to follow the same rigid rule) is an important factor in the
generation of relevant descriptions.  Therefore, our approach performs content
planning using grammatical structures that are learned from data and matched to
images on a case-by-case basis.

\citet{farhadi2009} find that attributes of objects are important for
visualness.  \citet{gupta2008} corroborate this statement by arguing for the
importance of adjectives and prepositions in the image description task.
\citet{aker2010} take the previous results one step further and state that some
grammatical relations in general (not just adjectives and prepositions) provide
useful support to generate very readable textual descriptions.
\citet{gupta2012} build on this and report that using richer semantic relations
(e.g.\ subject, object, etc.) helps in creating highly coherent and adequate
descriptions.  Given these findings, our approach uses the full range of
grammatical structure supported by the Stanford Dependency Parser
\citep{manning2008} in order to inform content selection and content planning.

\citet{yang2011} use language models in order to guide text generation and find
that this eliminates the creation of nonsensical descriptions.  Similarly,
\citet{krishnamoorthy2013} use language models as a post-processing step in
order to select the most grammatical and most realistic description from a pool
of candidates.  As a result, our approach uses a language model during surface
realisation to increase the grammaticality of generated image descriptions.

Further relevant literature will be introduced in the main text whenever
appropriate.

\section{Outline}\label{sec:outline}

The remainder of this report is structured as follows.

Chapter~\ref{chap:methodology} introduces our main contribution, i.e.\ the
development of a template-based model for automatic image description.  The
chapter also presents the baselines against which our model is compared and the
evaluation metrics used to judge the quality of the models.  Additionally, the
chapter introduces the data-set used to train the models and our methods for
feature selection.

Chapter~\ref{chap:implementation} is a companion to
Chapter~\ref{chap:methodology} in that the former provides experimental
validation for the choices of the latter.  Chapter~\ref{chap:implementation}
also reports the results of the feature-selection process previously described
as well as the results of tuning the models introduced in the previous chapter.

Chapter~\ref{chap:results} evaluates our models using automatic techniques and a
human study.  The chapter also discusses the respective performances of the
different approaches to automatic image description we presented in earlier
chapters.

Finally, Chapter~\ref{chap:conclusion} synthesises our findings, presents
conclusions, suggests how to improve our template-based model and explores
avenues for future work based on the broader results of this report.
