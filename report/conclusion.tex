\chapter{Conclusions}\label{chap:conclusion}

This report set out to investigate the vision--language interplay by studying
the ``Abstract Scenes Data-Set'' \citep{zitnick2013}, a collection of
semantically similar clip-art images and corresponding human generated
descriptions.  The simple nature of the images in the data-set allowed us to
largely abstract away considerations of object recognition and image
segmentation, thus enabling a focus on content planning, content selection and
surface realisation.

This final chapter, will review the research contributions of the report
(Section~\ref{sec:summary}), examine some results that may be of wider interest
for the automated image description community at large
(Section~\ref{sec:wider-considerations}) and discuss avenues for future work
(Section~\ref{sec:future-work}).

\section{Summary of Contributions}\label{sec:summary}

Our primary contribution of is a novel, template-based system that generates
image descriptions using a data-driven approach
(Section~\ref{sec:template-model}).  Our model sub-divides the description
generation process into three separate steps.  First, given an unseen image, a
content planner determines the most semantically salient grammatical structures
for the purpose of describing that image
(Section~\ref{sec:template-acquisition}).  Then, a content selector determines
which terms in our model's vocabulary apply to the image, respecting the
grammatical constraints imposed by the previous stage
(Section~\ref{sec:description-generation}).  Finally, a surface realiser merges
the grammatical structures of the content planner with the content selector's
terms, over-generating a bag of description and selecting the best one in terms
of grammaticality and adequacy (Section~\ref{sec:description-selection}).

Content planning and content selection were formulated as generative
classification tasks.  Surface realisation was formulated as a discriminative
classification task.  Our model is fully data-driven.  All three components of
our model were trained on visual and textual features extracted from the
``Abstract Scenes Data-Set'' (10,020 semantically related clip-art images and
60,396 unrestricted human-generated descriptions, Section~\ref{sec:data}) using
an information-theoretic measure (Section~\ref{sec:exploration},
Section~\ref{sec:feature-selection}).

Our system was tuned by optimising all components for BLEU and METEOR scores
(Section~\ref{sec:template-model-impl}).  These metrics were also used to
evaluate our model end-to-end.  We found that our template-based model
significantly outperformed a non-trivial transfer-based baseline system
(Section~\ref{sec:keyword-baseline},
Section~\ref{sec:automatic-evaluation-results}).  A human evaluation study
performed on Amazon Mechanical Turk, however, did not confirm this finding
(Section~\ref{sec:human-evaluation-results}), likely because BLEU and METEOR do
not correlate well with the human notion of image description quality
(Section~\ref{sec:template-model-performance}).

We found that our template-based model consistently produces descriptions of
high adequacy and outstanding fluency.

\section{Wider Considerations}\label{sec:wider-considerations}

The methodological elaboration and concrete implementation of our template-based
model (summarised above) further led to some insights that might be of interest
to the automated image description community at large.  These findings are
sketched below.

Analysing the textual corpus of the ``Abstract Scenes Data-Set'' in terms of
``Stanford typed dependencies'' revealed that a small set of grammatical
patterns is sufficient to cover a wide variety of image descriptions
(Section~\ref{sec:template-acquisition}).  The ``Abstract Scenes Data-Set'' is
broad in scope, encompassing descriptions of a multitude of actions, scenes,
agents, objects, poses and so forth.  It follows that our result should transfer
to other domains.  We thus provide empirical evidence to support the claim of
\citet{kulkarni2011} that descriptive language only uses a limited range of
syntactic patterns.

We also gave evidence for the old question of ``what is important in an image.''
Analysing the descriptive language used in the ``Abstract Scenes Data-Set''
revealed that a limited vocabulary of nouns, prepositions, adjectives, adverbs,
and so forth is sufficient to cover most aspects of how humans describe images
(Section~\ref{sec:word-feature-selection}).  Our work therefore corroborates
authors such as \citet{yang2011}, \citet{yao2010}, \citet{li2011} or
\citet{kulkarni2011} who restrict the target vocabulary of their image
description generation systems in order to make the language generation task
more tractable.

More importantly, the analysis of our human evaluation experiment
(Section~\ref{sec:human-evaluation-results}) revealed a discrepancy between
automated evaluation scores and the human perspective.  BLEU and METEOR gave our
system excellent scores (in excess of the inter-annotator agreement!)\ldots\ and
yet humans were still able to tell the difference between our system and the
gold-standard.  BLEU and METEOR did not pick up the kinds of errors that humans
are sensitive to.  For example, a description where the main agent (and only the
main agent) is incorrect will score highly under the automated evaluation
metrics since only one unigram is incorrect.  However, such a description is
deemed mostly useless by humans.  This rift informs two concerns.  Firstly, it
further highlights the types of image-elements that an automated description
system really ought to get right (such as the main subject of an image).
Secondly, it spotlights the unsatisfactory state of automatic evaluation in the
field.

\section{Future Work}\label{sec:future-work}

Our template-based image description model could be improved in a number of
ways, both from an engineering and from a conceptual point of view.

Low hanging fruit include engineering fixes for our model's three most recurrent
and egregious errors (described in
Section~\ref{sec:template-model-performance}).  For instance:

\begin{itemize}
\item Our model has issues with having an overly strong prior on certain words
in specific grammatical functions.  This could be addressed by tuning the
model's content selector locally rather than globally (that is, optimising the
parameters of every base-classifier $C_i$ that generates words for a particular
grammatical function $F_i$ individually rather than having one set of parameter
values for all $C$ and $F$).  Using base-classifiers more sophisticated than
Logistic Regression could additionally help with the prior-problem.  For
instance, Support Vector Machines \citep{cortes1995} are sometimes used to
generate text in the automated image description literature \citep{mooney2008}
but were not considered by us due to engineering constraints.

\item The model's by-design tendency of not handling collocations correctly
(e.g.\  truncating \emph{baseball \sout{glove}}) could be solved in one of two
ways.  A pragmatic engineering solution could be as simple as expanding the
model's vocabulary with collocations (i.e.  \emph{baseball-glove} instead of
\emph{baseball glove}).  A more theoretically sound alternative could handle
collocations by modifying the model's content selection component to generate
phrases instead of single words (e.g.\ see \citet{gupta2012}'s use of unigrams,
bigrams and trigrams to generate novel image descriptions).

\item The issue with the model's lack of ``world knowledge'' could be addressed
by using a language model trained on a larger corpus during surface realisation.
\citet{krishnamoorthy2013}, for instance, use a language model trained on the
Google ngram corpus of more than 500 billion words \citep{michel2011,lin2012}.
Alternatively, semantic grounding could be incorporated explicitly by following
an approach similar to \citet{yang2011}.
\end{itemize}

From a more conceptual standpoint, there are two main avenues for future
research based on our work:

\begin{itemize}
\item A number of authors \citep{kulkarni2011,mitchell2012}, including us
(Section~\ref{sec:template-acquisition-impl}), have stressed the importance of
being able to adapt the structure of a textual description to the image at hand.
Our novel two-phase ``template-acquisition followed by template-prediction''
methodology should be compared with other approaches in the literature.  The
Tree Substitution Grammars of \citet{mitchell2012} would be a good starting
point for this study.

\item We provided ample background for our claim that the findings entailed by
our work on the ``Abstract Scenes Data-Set'' should be transferable to
photo-realistic images \citep{heider1944,oatley1985,zitnick2013}.  In order to
validate the viability of the ``Abstract Scenes Data-Set'' as a sandbox for
semantics-focused studies of the vision--language interplay, our theoretical
arguments should be put to the test.  This could be done by developing a model
for the ``Abstract Scenes Data-Set'' (or using the model presented by this
report) and transferring the resulting methodology to a different corpus of
images and descriptions.
\end{itemize}

Perhaps most importantly, however, we highlighted the need for better automatic
evaluation metrics for image description.  A number of authors besides us
(Section~\ref{sec:human-evaluation-results}) have already stressed the
inadequacy of using BLEU to evaluate image descriptions due to the high
subjectivity and variability inherent in the task
\citep{kulkarni2011,gupta2012}.  We gave some evidence that a more syntactically
forgiving metric such as METEOR (which incorporates paraphrase and synonym
information) could work better than BLEU\@.  Additionally, we pointed out that
some parts of an image description (e.g.\ who is the main agent in the image?)
are more important to get right than others because humans are not equally
sensitive to all types of errors in image descriptions.  A more effective
approach might involve an automatic evaluation metric based on METEOR that
weights components of descriptions according to their grammatical function.
