\chapter{Results}\label{chap:results}

This chapter reports the performance of the models described in
Chapter~\ref{chap:methodology} as implemented using the methods described in
Chapter~\ref{chap:implementation}.  The models are trained on the union of the
training and validation set mentioned in Chapter~\ref{chap:implementation} and
evaluated on the remaining data.  80\% of the ``Abstract Scenes Data-Set''
(8,016 images, 48,315 descriptions) is thus used for model training and 20\% of
hitherto unseen data (2,004 images, 12,081 descriptions) is used for evaluation.

The remainder of this chapter splits performance evaluation into three main
sections.

First, Section~\ref{sec:automatic-evaluation-results} uses the automatic
evaluation techniques described in Section~\ref{sec:automatic-evaluation} to
compare and contrast the performance of the template model with the baseline
models.

In order to complement the automatic evaluation, we analyse the performance of
the models using manual inspection and report the findings in
Section~\ref{sec:performance-analysis}.

Section~\ref{sec:human-evaluation-results} then completes the evaluation of the
models by presenting and analysing the results of the human evaluation
experiment described in Section~\ref{sec:human-evaluation}.


\section{Automatic Evaluation}\label{sec:automatic-evaluation-results}

This section gives an overview of the performance of the template model, keyword
baseline model and image similarity baseline model described in
Sections~\ref{sec:template-model}--\ref{sec:baseline-models}.  The BLEU and
METEOR metrics described in Section~\ref{sec:automatic-evaluation} are used to
obtain a high-level perspective of the performance of the models.

Section~\ref{sec:automatic-evaluation} mentioned that one of the drawbacks of
the BLEU and METEOR metrics is their lack of interpretability.  Specifically,
the metrics do provide a way to compare the performance of two systems (if
system $X$ has a higher score than system $Y$, $X$ likely is the better system),
however, the metrics do not give any insight on what it \emph{means} to obtain a
score of $Z$.  In order to address this lack of interpretability, the human
performance in the image description task is measured using BLEU and METEOR\@.
The measurements are then used to ground the performance of the automatic image
description models.

Human performance in the image description task is approximated by measuring
inter-annotator agreement on the ``Abstract Scenes Data-Set.''  Said
inter-annotator agreement can be computed as follows.  Let $I$ be the
set of all images in the ``Abstract Scenes Data-Set.''  Every image $\lambda$ in
$I$ has $n^\lambda$ descriptions:
$d^\lambda_1,d^\lambda_2,\ldots,d^\lambda_{n^\lambda}$.  Let $D_i$ be
the set of all the $i$\textsuperscript{th} descriptions of all the images in the
data-set.  Now recall that BLEU and METEOR scores are obtained by computing
ngram overlaps between a set of hypothesis sentences and a set of reference
sentences.  The inter-annotator agreement $A_M$ for a metric $M$\footnote{Here,
$M$ is either BLEU or METEOR.} can therefore be computed by using the set of all
$i$\textsuperscript{th} descriptions ($D_i$) as the hypotheses and
the set of all other descriptions ($D_{-i}$) as the reference.  Then,
the result is averaged over all values of $i$.
Equation~\eqref{eq:human-agreement} summarises the computation of the human
agreement.

\begin{equation}\label{eq:human-agreement}
\begin{split}
A_M &= \frac{1}{n_{\min}} \sum\limits_{i=1}^{n_{\min}} M(D_i, D_{-i}), \\
D_i &= \left\{ d^\lambda_i \mid \lambda \in I \right\}, \\
D_{-i} &= \bigcup\limits_{j \in \left\{1,2,\ldots,n_{\min} \mid j \neq i\right\}} D_j, \\
n_{\min} &= \min \left\{n^\lambda \mid \lambda \in I \right\}
\end{split}
\end{equation}

\begin{table}
\centering
\begin{tabular}{| l | c | c |}
\hline
Model                     & BLEU score & METEOR score \\ \hline \hline
Image Similarity Baseline & 12.80      & 21.77        \\
Keyword Baseline          & 14.70      & 26.60        \\
Template Model            & 40.30      & 30.40        \\
Human Agreement           & 21.17      & 25.52        \\
\hline
\end{tabular}
\caption{Comparison of image description correctness (as measured by BLEU and
METEOR scores) for a template-based image description model and two baselines.
Human performance\protect\footnotemark\ is included as a reference to enable
better interpretation of the scores.}
\label{tab:model-comparison-metrics}
\end{table}
\footnotetext{As measured by inter-annotator agreement on the ``Abstract Scenes
Data-Set.''}

Table~\ref{tab:model-comparison-metrics} (above) uses
Equation~\eqref{eq:human-agreement} to measure how well the human performs the
image description task.  The table also shows the performances of the models
described in Section~\ref{sec:models}.  Note that the human performance is
relatively low.  As a matter of fact, one of our automated image description
models even out-performs the human annotators!  This highlights a further
pitfall of using BLEU and METEOR to judge the quality of image descriptions.
The image description task is highly subjective: humans will often describe very
different things about the same image, or at least describe similar things using
different language.  A good example for this is the image in
Figure~\ref{fig:description-variety-human}: five out of the six human-generated
descriptions of the image refer to entirely separate aspects of the image.
Human agreement is thus necessarily low: the subjectivity of image description
and the way that BLEU and METEOR measure ``goodness'' do not match well.
Figure~\ref{fig:description-variety-models} further illustrates this
shortcoming.  The template model, keyword baseline and image similarity baseline
describe the image as follows: \emph{Mike is wearing a witch hat}, \emph{A
helicopter is flying by Mike and Jenny} and \emph{There is a big cloud in the
sky} --- all perfectly fine descriptions.  Note, however, that the first and
third of these descriptions will be heavily penalised by BLEU and METEOR because
they happen to deviate in focus from the human annotators.  Thus,
Table~\ref{tab:model-comparison-metrics} highlights the need for human
evaluation as back-up and complement to automatic evaluation via BLEU and
METEOR\@.

\begin{sidewaysfigure}
\captionsetup{singlelinecheck=off}
\centering
\begin{minipage}[b]{0.5\hsize}
\centering
\includegraphics[width=0.75\textwidth]{description-variety-human.png}
\caption[]{Sample image from the ``Abstract Scenes Data-Set.''\\
The human-generated descriptions for the image are:
\begin{enumerate}[\hspace{0.5cm}$d_1:$]
\item \emph{Jenny is pretending to be a witch.}
\item \emph{Jenny is wearing the hat.}
\item \emph{Mike is eating a hot dog.}
\item \emph{Mike is eating the hot dog.}
\item \emph{The pie is on the table.}
\item \emph{The snake is using balloons to float.}
\end{enumerate}}
\label{fig:description-variety-human}
\end{minipage}%
\hfill
\begin{minipage}[b]{0.5\hsize}
\centering
\includegraphics[width=0.75\textwidth]{description-variety-models.png}
\caption[]{Sample image from the ``Abstract Scenes Data-Set.''\\
The human-generated descriptions for the image are:
\begin{enumerate}[\hspace{0.5cm}$d_1:$]
\item \emph{A big helicopter flies above Jenny.}
\item \emph{Jenny almost burned her hand on the fire.}
\item \emph{Jenny doesn't like the fire.}
\item \emph{Jenny sat close to the fire.}
\item \emph{Mike is trying to scare Jenny.}
\item \emph{Mike jumped when he saw the helicopter.}
\end{enumerate}}
\label{fig:description-variety-models}
\end{minipage}%
\end{sidewaysfigure}

Besides human agreement, Table~\ref{tab:model-comparison-metrics} also gives
BLEU and METEOR scores for the template model
(Section~\ref{sec:template-model}), keyword baseline
(Section~\ref{sec:keyword-baseline}) and image similarity baseline
(Section~\ref{sec:picture-baseline}).  We discuss these scores below.

The performance of the two baseline models is bounded by the inter-annotator
agreement.  Both baselines describe images by retrieving appropriate human
generated descriptions.  It is therefore difficult for the models to outperform
the inter-annotator agreement.  ``Difficult'', not ``impossible.''
Table~\ref{tab:model-comparison-metrics} shows that the keyword baseline's
METEOR score is about 4\% relative (or 1.08 points absolute) higher than the
inter-annotator agreement.  This can be explained observing that the keyword
baseline may retrieve a description that has a higher overlap with the set of
human descriptions of an image than any singular gold-standard description of
that image.  For example, the retrieved description might synthesise multiple
gold-standard descriptions of the image.  However, the chances that such a great
description exists and is retrieved is low since a sufficiently similar image
must have been seen during model training time and that image must have been
described in a sufficiently exhaustive manner.  Hence the statement holds: the
performance of the baseline models is bounded by the inter-annotator agreement.

The performance of the template model on the other hand, quite substantially
exceeds the baselines and inter-annotator agreement (+90\% relative BLEU score
and +19\% relative METEOR score as per
Table~\ref{tab:model-comparison-metrics}).  As mentioned previously, however
this does not mean that the template model produces better descriptions than
humans.  The good performance of the model under the automatic evaluation
metrics is simply an artifact of the subjectivity of the image description task
(i.e.\ the template model is being compared against a human agreement that is
artificially low).  Nevertheless, the template model's high scores and
specifically the fact that the template model outperforms both retrieval-based
baselines does highlight one of the model's strengths.  Unlike the baseline
models, the template model generates novel image descriptions instead of
retrieving human-generated ones.  This means that the template model is more
flexible with the content of its image descriptions.  For instance, if the
template model thinks that \emph{Jenny}, \emph{Mike}, \emph{owl} and \emph{ball}
are important words for an image, the model prioritises these words to fashion a
new description using all of them.  A human generated description, on the other
hand, is likely to only focus on one or two of the aspects.  Thus created, the
template model's description is likely to encompass multiple gold-standard
descriptions, meaning that the construction will score well under BLEU and
METEOR\@.\footnote{The observation that generation-based image description
models can produce descriptions more specifically matching images than
retrieval-based models is consistent with \citet{kulkarni2011} who also observe
this.}

This section quantified the performance of the template model, keyword baseline
model and image similarity baseline model using the BLEU and METEOR automatic
evaluation metrics.  The template model outperformed both baselines.  The
performance of all three models was further compared against human performance
in the image description task (as measured by inter-annotator agreement on the
``Abstract Scenes Data-Set'').  It was found that the template model's
performance exceeded the inter-annotator agreement due to the highly subjective
nature of the image description task (leading to low inter-annotator agreement)
and due to the template model's ability to construct descriptions that
synthesise image contents well.


\section{Performance Analysis}\label{sec:performance-analysis}

The previous section gave a high-level overview of the performance of
Section~\ref{sec:models}'s models.  This section aims to complement and
concretise that impression by analysing the performance of the three models via
providing sample images and descriptions generated by the models.

The overarching aim of the section is to introduce a better feel for the models'
performance, before subjecting the models to human evaluation in
Section~\ref{sec:human-evaluation-results}.  The section should be read in
conjunction with Appendix~\ref{app:sample-outputs} which provides additional
images illustrating the points argued below.

Investigating the image descriptions produced by the three image description
systems of Section~\ref{sec:models} suggests that some observations hold for all
three models alike.  For instance:
\begin{itemize}
\item Figure~\ref{fig:model-comparison-images1} and
Figures~\ref{fig:model-comparison-images2}--\ref{fig:model-comparison-images3}
in Appendix~\ref{app:sample-outputs} suggest that all three models can perform
quite admirably, producing descriptions that match a broad range of images very
well.
\item Figure~\ref{fig:model-comparison-images-terribad} suggests that the models
are also able to produce reasonable descriptions for strange and
out-of-the-ordinary images.
\item Figure~\ref{fig:model-comparison-images-bad} suggests that some images are
difficult to describe for all models.
\end{itemize}

Manually inspecting the descriptions produced by the model-trio generally
confirms the observations of the last section.  The template model consistently
produces the best results, the image similarity baseline is the worst model and
the keyword baseline is somewhere in between, ranging from excellent to
terrible.

The remainder of this section investigates the performance of each of the three
models individually.

\subsection{Template Model}\label{sec:template-model-performance}

The content of the descriptions generated by the template model (unsurprisingly)
follows a Zipfian distribution:\footnote{A Zipfian distribution is a discrete
power law distribution where rank is roughly inversely proportional to
frequency.  The distribution explains many phenomena in natural language
\citep[Section~1.4.3]{manning1999}.} a few phrases are used to describe lots of
images and lots of phrases are generated for only a handful of images.  It is
therefore prudent to check whether the most commonly generated descriptions are
appropriate for their corresponding images.
Figures~\ref{fig:template_model-good-mike_is_wearing_X}--\ref{fig:template_model-good-mike_and_jenny_X}
and
Figures~\ref{fig:template_model-good-mike_is_holding_X}--\ref{fig:template_model-good-jenny_is_scared_X}
in Appendix~\ref{app:sample-outputs} show a selection of images that were
described by the template model using some of the most frequent phrases in that
model's vocabulary.  The descriptions all match the images (or at least parts of
them) well.

Note that the commonly used ``description building blocks'' introduced in the
last paragraph\footnote{These building blocks are frequently occurring
description fragments such as \emph{Mike is wearing}, \emph{Mike is sitting},
\emph{Mike and Jenny} and so forth.} often cover the main action in the
described image.  The template model thus has the beneficial property that its
most frequently generated descriptions capture the most salient parts of the
described images instead of generic, always-applicable aspects (e.g.\ describing
elements of the image that are always present such as the sky or the grass).

More broadly speaking, the template model often produces very adequate
descriptions or descriptions that are at least relevant to a subset of the
described image (refer to any of the figures in this section or
Appendix~\ref{app:sample-outputs} to find examples verifying this statement).

Additionally, the template model rarely generates descriptions that are truly
wrong.  When the model produces incorrect descriptions, usually only one or two
words in the description are mistaken (e.g.\ switching \emph{Jenny} for
\emph{Mike} or using \emph{in} instead of \emph{near}).  This observation
further explains the template model's good performance under BLEU and
METEOR: one or two incorrect words will not substantially harm ngram overlap
scores whence the model will get partial credit for somewhat-correct
descriptions.

A more in-depth investigation of the errors systematically committed by the
template model reveals that the model suffers from three main problems.

First and most importantly, certain words have a strong prior in certain
grammatical functions.  For instance, humans overwhelmingly describe images that
contain the little boy by using \emph{Mike} as the active subject of their
description.  Therefore, the template model has a strong bias for using
\emph{Mike} in a subject role.  This leads to descriptions where \emph{Mike} is
erroneously used as the subject of a sentence whose focus is on a completely
different part of the image or even to descriptions where \emph{Mike} is used as
subject despite the boy not being present in the image.
Figure~\ref{fig:template_model-bad-prior} gives example image descriptions
exhibiting this default.

Secondly, unlike other image descriptors in the literature (e.g.\
\citet{yang2011}), the template model has no explicit notion of semantic
grounding.  This means that the template model does not penalise descriptions
that are clearly nonsensical such as \emph{Mike is sitting in the sky}
(Figure~\ref{fig:template_model-bad-domain_knowledge}).  Arguably, the
description generator's language model should obstruct these sentences, but
evidently it fails to do so.  This is likely due to the dearth of data that the
template model's language model was trained on.

The final systematic error committed by the template model stems from the very
foundations on which the model is built.  The model generates descriptions by
filling in templates consisting of subjects, objects, verbs and so forth.  This
means that the model has difficulties dealing with collocations.  Sometimes,
descriptions are short a word due to a lack of an available and appropriate
template-slot.  An example for this is the generated description \emph{Mike is
wearing a baseball \sout{glove}}.  Here, the template model did not provide for
a collocated word at the sentence end.  More examples of this phenomenon can be
found in Figure~\ref{fig:template_model-bad-collocations}.

Note however, that in the grand scheme of things, the template model commits
relatively few errors.  The down-side of this is that the template models'
descriptions are never truly exciting.  The model factually describes objects
and relations that are explicitly present in the image, leaving little to no
room for interpretation or imagination.  While, arguably, objectivity is a good
quality for an automated image description system, it also makes the model's
productions seem slightly dull to the human.

\subsection{Keyword Baseline}\label{sec:keyword-baseline-performance}

\begin{figure}
\captionsetup{singlelinecheck=off}
\centering
\includegraphics[height=0.35\textheight]{keyword_baseline-good-implicit1.png}
\caption[]{Sample image from the ``Abstract Scenes Data-Set.''\\
Our automatic image description systems describe this image as follows:\\
\-\hspace{0.5cm}Keyword baseline: \emph{Mike and Jenny are sad because the kite flew away.}\\
\-\hspace{0.5cm}Image Similarity baseline: \emph{Mike is reaching his arms forward.}\\
\-\hspace{0.5cm}Template model: \emph{Mike is wearing a hat.}}
\label{fig:keyword_baseline-good-implicit1}
\vspace{0.75cm}
\includegraphics[height=0.35\textheight]{keyword_baseline-good-implicit2.png}
\caption[]{Sample image from the ``Abstract Scenes Data-Set.''\\
Our automatic image description systems describe this image as follows:\\
\-\hspace{0.5cm}Keyword baseline: \emph{Mike is holding Jenny's hot-dog.}\\
\-\hspace{0.5cm}Image Similarity baseline: \emph{The baseball is in the baseball glove.}\\
\-\hspace{0.5cm}Template model: \emph{Mike is holding a hamburger.}}
\label{fig:keyword_baseline-good-implicit2}
\end{figure}

Now compare the ``correct-but-boring'' template model to the keyword baseline.
The later can produce descriptions that, to the human, sound surprisingly
intricate because they are open to interpretation (e.g.\ see
Figure~\ref{fig:keyword_baseline-good-implicit1}) or relatively complex (e.g.\
see Figure~\ref{fig:keyword_baseline-good-implicit2}).  However, this
predisposes the model to frequent failure by generating descriptions that are
not even remotely relevant to the image at hand (e.g.\ see
Figures~\ref{fig:keyword_baseline-bad-false_associations}--\ref{fig:keyword_baseline-bad-high_idf}).

There are two main types of errors that the keyword baseline commits
systematically.

Firstly, the baseline is prone to ``false associations'' i.e.\ the model
retrieves a description that matches some of the keywords for an image but not
the contents of the image itself (see
Figure~\ref{fig:keyword_baseline-bad-false_associations}: a pink dress or a blue
shirt in the image lead to descriptions about a pink bucket or a blue hat being
retrieved).  In the template model, selecting less salient keywords to describe
the image leads to only one or two incorrect words --- with the keyword
baseline, on the other hand, the entire description risks being inadequate.
This is a nice demonstration of one of the chief advantages of the
generation-based image description paradigm over the retrieval-based one.

Secondly, the baseline struggles to deal with unusual elements in images.
Unusual image elements lead to high inverse-document-frequency ($\idf$) keywords
being generated.  Consequentially, the high $\idf$ keywords bias the description
retrieval process, leading to the selection of overly specific or nonsensical
descriptions (see Figure~\ref{fig:keyword_baseline-bad-high_idf}: clouds or
balloons in an image lead to descriptions about rain or hot air balloons being
retrieved).

Thus, the keyword model is simple, sometimes wrong, but still often effective.
Note that the model can only get better as the size of the available data (and
therewith the pool of retrievable descriptions) grows
\citep{ordonez2011,krishnamoorthy2013}.  As a matter of fact, models similar to
the keyword baseline have been proposed in the literature and were found to work
remarkably well given a large enough collection of images and descriptions to
work with (refer to \citet{ordonez2011} and \citet{kuznetsova2012} for directly
comparable approaches or refer to \citet{berg2010}, \citet{kulkarni2011} and
\citet{mitchell2012} for related approaches).

\subsection{Image Similarity Baseline}\label{sec:picture-baseline-performance}

Lastly, consider the image similarity baseline.
Figure~\ref{fig:picture_baseline-bad} (and numerous other examples in this
chapter and Appendix~\ref{app:sample-outputs}) suggests that the baseline often
fails to produce adequate descriptions for images.  This is likely caused by the
two major sources of noise that the model imposes onto the description
retrieval process by design.  In order for the image similarity model to
produce a valid description for an unseen image, it needs to find a sufficiently
similar image to the unseen one and then, additionally, select a similarly well
matching description from the retrieved image.

The bulk of this chapter has argued extensively that humans describe very
different aspects of an image.  This implies that some of the descriptions of
one image will not apply to another, even if the images are somewhat similar.
This means that the description selection aspect of the image similarity
baseline is noisy.  Additionally, examining the way that the image similarity
baseline restricts the search space to find similar images (see
Figure~\ref{fig:picture_baseline-bad-buckets} in
Appendix~\ref{app:sample-outputs}) suggest that the model is also rather poor at
performing visual matches.  It is hardly surprising then, that the image
similarity baseline performs poorly and produces lots of mismatched
descriptions.

This section analysed the performance of the template model, keyword baseline
and image similarity baseline through manual inspection of the descriptions
generated by the models.  The section also investigated some of the re-occurring
sources of errors for all three models.  The findings of this section mirror the
results of the automatic evaluation: the template model produces better image
descriptions than either of the two baseline models.


\section{Human Evaluation}\label{sec:human-evaluation-results}

This section presents the results of the human evaluation study described in
Section~\ref{sec:human-evaluation}.  The study recruited 100 human judges on
Amazon Mechanical Turk to evaluate the quality of 200 descriptions generated by
the template model and keyword baseline using a five-point Likert scale.  In
order to ground the ratings of the two systems, the judges were also asked to
evaluate the quality of a randomly selected human-generated gold-standard
description for every image.

Inspection of the judges' ratings led to the rejection of 17 participants: their
scores did not look natural (e.g.\ uniformly distributed ratings).  A one-way
ANOVA\footnote{ANOVA (short for ``analysis of variance'') is a family of
statistical models to analyse studies with three or more groups.  ANOVA computes
the probability of the null hypothesis (i.e.\ the samples in all groups are
drawn from the same population) by comparing two estimates of the population
standard deviation: one based on sample variances and the other based on
differences between sample means.  If the null hypothesis is true, both of these
estimates should be the same, otherwise the estimate based on sample variances
should be higher.  The larger the difference between the two mean-estimates, the
higher the probability that the samples stem from different populations. Refer
to \cite[Chapter~11]{howell2012} for a more thorough introduction to the
procedure.} with post-hoc Tukey HSD test\footnote{Tukey HSD (short for ``honest
significant difference'') is a statistical procedure to check if the means of
different groups are significantly different from one-another.  The test
compares the mean of every group to the means of all other groups and identifies
differences greater than the standard error.  Refer to
\cite[Chapter~12]{howell2012} for a more thorough introduction to the test.} was
used to analyse the remaining human judges' ratings.

Table~\ref{tab:human-evaluation-means} shows the results of the study.  The full
HSD matrix of the tests is in Table~\ref{tab:human-evaluation-hsd}.  Both models
(keyword- and template-based) are significantly different from the human
gold-standard ($p<0.01$).  There is no statistically significant difference
between the template model and the keyword baseline.

There are several conclusions to be drawn from this study.  Firstly, note that
the human judges were unable to differentiate between the performance of the
template model and keyword baseline despite the former model's descriptions
being machine generated to the latter model's human generated descriptions.  The
humans who wrote the descriptions of the keyword baseline presumably highlighted
salient aspects of the described images.  This study thus implies that the
template model is also able to do this.

Secondarily, the discrepancy between the results of the human evaluation study
and the automatic evaluation of Section~\ref{sec:automatic-evaluation-results}
is of interest.  How can the template model vastly outperform the keyword
baseline from BLEU and METEOR's perspective and yet be indistinguishable from a
human's point of view?  One way to explain the difference is the observation
that, unlike the automated evaluation metrics, humans penalise the types of
errors made by the template model heavily (e.g.\ confusing the subject or
primary object of a description).  This behaviour was initially observed when
running the human evaluation study informally with a small amount of
face-to-face participants and holds for the study run at scale on Amazon
Mechanical Turk.  The results of this section can thus inform future work in
automated image description: getting the main word of a sentence right is
important --- automated metrics such as BLEU or METEOR will not necessarily
catch this.

This section reported the results of a study using human judges to evaluate the
performance of the keyword baseline and template model.  The study found no
statistically significant difference between the two systems.  The section
argued that this result reflects favourably on the quality of the
machine-generated descriptions produced by the template model.  The section
further argued that the finding is also indicative of a discrepancy in what
humans deem important in an image description and what automated evaluation
metrics measure.

\begin{table}
\centering
\begin{tabular}{| l | c | c | c | c |}
\hline
Model                & $n$ & $\mu$  & $\text{SD} $ & $\text{SE}$ \\ \hline\hline
Human Gold-Standard  & 83  & 3.9157 & 0.5291     & 0.0581        \\
Keyword Baseline     & 83  & 2.6675 & 0.6197     & 0.0680        \\
Template Model       & 83  & 2.4053 & 0.6329     & 0.0695        \\
\hline
\end{tabular}
\par \medskip
$\mu=\frac{1}{n}\sum\limits_{i=1}^n x_i,\;
\text{SD}=\sqrt{\frac{1}{n}\sum\limits_{i=1}^n(x_i-\mu)},\;
\text{SE}=\frac{\text{SD}}{\sqrt{n}}$.
\caption{Results of the human study.  Subjects were asked to rate the
productions of two image description systems and a randomly selected
gold-standard description (as a point of reference) on a five-point Likert
scale.  $n$ is the number of subjects in the study, $\mu$ is the mean of the
human ratings $x_i$, $\text{SD}$ is the standard deviation of the mean and
$\text{SE}$ is the standard error of the mean.}
\label{tab:human-evaluation-means}
\end{table}

\begin{table}
\centering
\begin{tabular}{| l | c c c |}
\hline
Model               & Mean     & Keyword Baseline & Template Model    \\ \hline
Human Gold-Standard & $3.9157$ & $1.2482^\dagger$ & $1.51041^\dagger$ \\
Keyword Baseline    & $2.6675$ &                  & $0.2622$          \\
Template Model      & $2.4053$ &                  &                   \\
\hline
\end{tabular}
\par \medskip
\textsuperscript{$\dagger$}
    $\text{significant at } \alpha=0.05 \text{, critical difference}=0.3925$.
\caption{Full matrix of the HSD test.}
\label{tab:human-evaluation-hsd}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%% image dump for section %%%%%%%%%%%%%%%%%%%%%%%%%%%%
\clearpage
\begin{sidewaysfigure}
\centering
\setlength{\abovecaptionskip}{0pt plus 0pt minus 0pt}
\includegraphics[width=\textwidth]{model-comparison-images1.pdf}
\caption{Some images are well described by all models.}
\label{fig:model-comparison-images1}
\end{sidewaysfigure}
% page
\begin{sidewaysfigure}
\centering
\setlength{\abovecaptionskip}{0pt plus 0pt minus 0pt}
\includegraphics[width=\textwidth]{model-comparison-images-terribad.pdf}
\caption{The models handle difficult, strange or noisy images rather well.}
\label{fig:model-comparison-images-terribad}
\end{sidewaysfigure}
% page
\begin{sidewaysfigure}
\centering
\setlength{\abovecaptionskip}{0pt plus 0pt minus 0pt}
\includegraphics[width=\textwidth]{model-comparison-images-bad.pdf}
\caption{Some images are difficult for all models.}
\label{fig:model-comparison-images-bad}
\end{sidewaysfigure}
% page
\begin{sidewaysfigure}
\centering
\setlength{\abovecaptionskip}{0pt plus 0pt minus 0pt}
\qquad
\includegraphics[width=\textwidth]{template_model-good-mike_is_wearing_X.pdf}
\caption{Template model frequent phrases --- sample images for descriptions
starting with \emph{Mike is wearing}.}
\label{fig:template_model-good-mike_is_wearing_X}
\qquad
\includegraphics[width=\textwidth]{template_model-good-mike_is_sitting_X.pdf}
\caption{Template model frequent phrases --- sample images for descriptions
starting with \emph{Mike is sitting}.}
\label{fig:template_model-good-mike_is_sitting_X}
\qquad
\includegraphics[width=\textwidth]{template_model-good-mike_and_jenny_X.pdf}
\caption{Template model frequent phrases --- sample images for descriptions
starting with \emph{Mike and Jennny}.}
\label{fig:template_model-good-mike_and_jenny_X}
\end{sidewaysfigure}
% page
\begin{sidewaysfigure}
\centering
\setlength{\abovecaptionskip}{0pt plus 0pt minus 0pt}
\qquad
\includegraphics[width=\textwidth]{template_model-bad-prior.pdf}
\caption{Template model systemic errors --- prior on objects in certain
grammatical functions leads to incorrect descriptions.}
\label{fig:template_model-bad-prior}
\qquad
\includegraphics[width=\textwidth]{template_model-bad-domain_knowledge.pdf}
\caption{Template model systemic errors --- lack of domain knowledge.}
\label{fig:template_model-bad-domain_knowledge}
\qquad
\includegraphics[width=\textwidth]{template_model-bad-collocations.pdf}
\caption{Template model systemic errors --- templates do not account for
collocations.}
\label{fig:template_model-bad-collocations}
\end{sidewaysfigure}
% page
\begin{sidewaysfigure}
\centering
\setlength{\abovecaptionskip}{0pt plus 0pt minus 0pt}
\qquad
\includegraphics[width=\textwidth]{keyword_baseline-bad-false_associations.pdf}
\caption{Keyword baseline generations --- systemic errors due to false
associations.}
\label{fig:keyword_baseline-bad-false_associations}
\qquad
\includegraphics[width=\textwidth]{keyword_baseline-bad-high_idf.pdf}
\caption{Keyword baseline generations --- systemic errors due to terms with high
$\idf$ scores.}
\label{fig:keyword_baseline-bad-high_idf}
\qquad
\includegraphics[width=\textwidth]{picture_baseline-bad.pdf}
\caption{Image similarity baseline --- the model simply rarely produces good
descriptions.}
\label{fig:picture_baseline-bad}
\end{sidewaysfigure}
%%%%%%%%%%%%%%%%%%%%%%%%% end of image dump for section %%%%%%%%%%%%%%%%%%%%%%%%
