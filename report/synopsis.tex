\documentclass[11pt]{article}
\usepackage{acl2014}
\usepackage{times}
\usepackage{amsmath}
\usepackage{microtype}
\usepackage{graphicx,rotating}
\usepackage{hyperref}
\usepackage{slashbox}

\pdfobjcompresslevel=0 % fixes 131 error with acrobat reader
\DeclareMathOperator*{\argmax}{arg\,max}

\title{A Template-based Model for Automatic Image Description}
\author{Clemens Wolff \\
The University of Edinburgh \\
{\tt s0942284@sms.ed.ac.uk}}
\date{}

\begin{document}
\maketitle

\begin{abstract}
We present a system that automatically generates simple textual
descriptions for images.\footnotemark

Our work focuses on the vision--language interplay of the image description
task. Our system learns how humans describe images by discovering patterns in a
large collection of image-descriptions. Once our method \emph{knows} how to
describe images, it looks at the image, \emph{sees} what objects are present and
\emph{generates} a human-like description.

The  parallel process of inferring grammatical structure and having the
structure guide description generation is the main novel contribution of our
work --- this approach allows us to create grammatically rich and
natural-sounding text.
\end{abstract}
\footnotetext{The work presented in this summary report was undertaken as part
of the ``fourth year undergraduate project'' at the University of Edinburgh; a
two-semester long individual research project.}

\section{Introduction}

Images are a rich source of information merging the explicit (What objects are
in the picture? What attributes do they have?) with the implicit (How do the
objects in the picture relate? How do they interact?). ``A picture is worth a
thousand words'' --- and yet humans can describe images succinctly, even when
they are of poor quality \cite{bachmann1991,oliva2000} or if they are only
visible for an instant \cite{potter1976,fei2007}.

A human can \emph{see} what objects are in an image, \emph{recognise} the
relationships between them, \emph{understand} their connections and
\emph{summarise} the most salient aspects of the image, glossing over less
important parts. For instance, seeing the image in Figure~\ref{fig:example}, a
human prioritised the boy and the interaction he has with the cat but
selectively chose to not describe the sun, the dog, the table, etc.

\begin{figure}
\centering
\includegraphics[width=.5\linewidth]{example.png}
\par\medskip
Mike is happy to see the orange cat.
\caption{Sample image and description.}
\label{fig:example}
\end{figure}

Describing images \emph{automatically} is a formidable task at the intersection
of computer vision and natural language processing, involving many components:
object recognition, image segmentation, attribute extraction, content planning,
content selection and surface realisation.

Mirroring the number of challenges in the area, the applications of automated
image descriptions are manifold:
\begin{itemize}
\item Making images accessible to the visually impaired \cite{ferres2006}.
\item Improving web-based image search through semantic awareness
\cite{barnard2001,feng2013}.
\item Enabling robust search in unlabelled collections of images such as
personal photo-archives \cite{krizhevsky2012}.
\end{itemize}
Additionally, studying the vision--language interplay will lead to a deeper
understanding of how humans describe images, working towards answering the
question of ``what is important in an image.''  Such insights can be used to
guide research in computer vision (Which object detectors should we build
next?).

Prior work in the field of automated image understanding can roughly be
categorized along three dimensions.
\begin{itemize}
\item \emph{Keyword-based} approaches annotate images with tags
\cite{duygulu2002,lavrenko2003,feng2004,guillaumin2009,makadia2010}. For
example, Figure~\ref{fig:example} could be labelled with \emph{cat},
\emph{orange} and \emph{table}. Problems with this approach include the
ambiguity of the disembodied tags (Is the cat orange or the table?) and the lack
of semantic content (How are the cat and table related?).
\item \emph{Transfer-based} approaches exploit large collections of parallel
images and human-written descriptions.  New images are described by finding
similar images and transferring (parts of) their human-generated descriptions
onto the unseen images \cite{farhadi2010,ordonez2011,kuznetsova2012}. The
resulting descriptions are fluent but may not describe the image accurately.
\item \emph{Generation-based} approaches combine the responses of
object-detectors with a linguistic model in order to create completely novel
descriptions for unseen images
\cite{kulkarni2011,mitchell2012,krishnamoorthy2013}. The generated descriptions
capture image contents adequately but often lack fluency because language
generation is a hard problem.
\end{itemize}

Our work fits in with the generation-based approach but achieves levels of
fluency normally reserved for transfer-based approaches.

\section{Data}

The work presented in this report focuses on the vision--language interplay and
language generation aspects of the image description task. This was made
possible by basing our work on the ``Abstract Scenes Data-Set'' by Zitnick and
Parikh \shortcite{zitnick2013}. This data-set is relevant for our work for three
main reasons:
\begin{enumerate}
\item The data-set uses clip-art images to make the computer-vision aspect of
image description straight-forward. The data-set comes bundled with a vast
quantity of high-quality features describing the images such as object
(co)occurrence, absolute locations of objects, relative distances between
objects, etc. This allows us to focus on language without having to worry about
noisy vision detections.
\item The data-set is large (60,396 descriptions, 10,020 images), enabling the
use of statistical and probabilistic modelling techniques.
\item The data-set was created in a way that captures the variety inherent in
how humans describe images. First, humans were asked to create 10 images for
each of 1,002 distinct ``concepts'' such as \emph{Jenny loves to play soccer but
she is worried that Mike will kick the ball too hard}. Then a different set of
experiment participants was asked to create at least 5 descriptions for every
image. This hierarchical data-creation process leads to a data-set that contains
conceptually similar images that express details in different ways.
\end{enumerate}

Figure~\ref{fig:dataset-howcreated} summarizes the creation process and shows
some sample concepts, images and descriptions.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{dataset-howcreated.pdf}
\caption{Overview of the creation process of the ``Abstract Scenes Data-Set.''}
\label{fig:dataset-howcreated}
\end{figure}

\section{Method}

Our system generates descriptions using a three-phase process
(Figure~\ref{fig:template_model-flow}).  First, given an image, a generative
model predicts grammatical structures that are typical for the way that a human
would describe the image (\emph{content planning}).  Then, another generative
model uses these structures to construct descriptions for the image
(\emph{content selection}). Lastly, the best generated description is selected
by a discriminative model (\emph{surface realisation}).

\begin{figure}
\centering
\includegraphics[height=8cm]{template_model-flow-nonum.pdf}
\caption{Overview of our template-based image description model.}
\label{fig:template_model-flow}
\end{figure}

In spirit, the process underlying our model is similar to the
``overgenerate-and-select'' approach proposed by Langkilde and Knight
\shortcite{langkilde1998} and adapted for the image description task by Mitchell
et al.\ \shortcite{mitchell2012}.  The main inspiration for the model, however,
comes from Barzilay and Lee \shortcite{barzilay2003} who mined grammatical
structures from text data and used these templates for language generation.

In order to realize the three phases (content planning, content selection,
surface realization) of our model, we require the following components:

\begin{itemize}
\item A ``template-predictor'' predicts which description template is
appropriate for what kinds of images. The classifier maximizes
Equation~\eqref{eq:image-to-template} and might learn rules such as \emph{if
there are two children in the image, a conjunction should be in the template}.
\begin{equation}\label{eq:image-to-template}
\hat{t}^{\lambda} = \argmax_{t \in T} p(\lambda,t).
\end{equation}
Here $T$ is a set of templates and $\lambda$ is an image.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{most-common-templates.png}
\caption{Most frequent typed-dependency-based templates. The most common
template is \emph{nsubj,aux,root,prep,det,dobj} which stands for
``noun-subject'', ``auxiliary'', ``root word'', ``preposition,'' ``determiner''
and ``direct-object''. For a full description of typed dependencies, refer to De
Marneffe and Manning \shortcite{demarneffe2008}.}
\label{fig:most-common-templates}
\end{figure}

Typical patterns of human image-description are acquired by analyzing the
``Abstract Scenes Data-Set'' in terms of grammatical structure as expressed by
typed dependencies \cite{demarneffe2006,demarneffe2008,manning2008}.
Figure~\ref{fig:most-common-templates} shows that a handful of these
``templates'' are sufficient to capture the structure of a large variety of
image descriptions.

\item A ``word-predictor'' predicts the words that are appropriate for a given
image, respecting some grammatical constraints. The classifier might learn rules
such as \emph{if the sun is in the image, a likely adjective is ``hot'' and a
likely noun is ``park''}. Equation~\eqref{eq:description-generation} describes
the model:
\begin{equation}\label{eq:description-generation}
\hat{w}^{\lambda}_{s} = \argmax_{w \in W_{s}} p(w,\lambda \mid s).
\end{equation}
Here $s$ is a grammatical function (e.g.\ noun, verb, adverb, etc.) and $W_s$
are the possible words in that function.

\item A language model separates well-formed text from word-salad. Our language
model interpolates a bigram and a trigram model and normalizes the scores using
a length component in order to penalize abnormally short or long descriptions
(Equation~\eqref{eq:description-selection}).
\begin{equation}\label{eq:description-selection}
\hat{d}^{\lambda} = \argmax_{s \in S^{\lambda}}
\prod\limits_{i \in \left\{2, 3, \mathrm{length}\right\}} c_{i} p_{i}(s).
\end{equation}
Here $S$ are candidate descriptions, $c_{\left\{2,3,\mathrm{length}\right\}}$
are weights, $p_{\left\{2,3\right\}}$ are bigram and trigram probabilities
(Equation~\eqref{eq:ngram-model}) and $p_\mathrm{length}$ is the maximum
likelihood estimate of the description length.
\begin{equation}\label{eq:ngram-model}
p_{n}(d) = p_{n}(w_1^m) = \prod\limits_{i=n}^{m} p(w_i|w_{i-(n-1)}^{i-1}).
\end{equation}
Here, $d=w_1^m=\left\{w_1,w_2,\cdots,w_m\right\}$ is a sentence of $m$ words and
$p(\cdot)$ is the probability of a word under the $n^\textrm{th}$-order Markov
assumption.
\end{itemize}

In order to create a description for an image $\lambda$, we ask the
\emph{template-predictor} to generate the $M$-best templates for $\lambda$. We
then ask the \emph{word-predictor} to generate the $N$-best words for every
grammatical role in the $M$ templates. Finally, we ask the \emph{language model}
to find the most well-formed description among all the possible combinations of
$N$-words to $M$-template assignments. This is the final description for the
image $\lambda$.

\section{Implementation}

We implement our framework by considering a broad range of values for $M$ and
$N$ and a wide variety of methods to implement the classifiers: Naive Bayes (NB)
\cite{rish2001,zhang2004}, Decision Tree \cite{olshen1984}, Random Forest
\cite{breiman2001} and Logistic Regression \cite{yu2011}.

We tune our model using two metrics borrowed from machine translation that are
widely used in the image description literature
\cite{farhadi2010,ordonez2011,kulkarni2011,kuznetsova2012,elliott2013,krishnamoorthy2013}:
BLEU \cite{papineni2002} and METEOR \cite{denkowski2011}.\footnotemark\ The
metrics essentially compute n-gram overlaps between generated descriptions and
reference (human-written) descriptions. Generated image descriptions that have
many words (or synonyms, or paraphrases) in common with human-written
descriptions for the same image will score highly under BLEU and METEOR\@.
\footnotetext{For brevity sake, only BLEU scores are reported in this synopsis.
Evaluating METEOR does not lead to any significantly different conclusions than
BLEU scores.}

We find that strongly regularized
(Figure~\ref{fig:description-generation-tuning}) Logistic Regression produces
the best results during the template-prediction
(Table~\ref{tab:template-acquisition-impact}) and word-prediction
(Table~\ref{tab:description-generation-performance}) steps of our framework. A
balanced language model produces the best results during surface realisation
(Figure~\ref{fig:description-selection}).

\begin{figure}
\centering
\includegraphics[width=\linewidth]{description-generation-tuning-bleuonly.png}
\caption{BLEU scores (higher is better) for a template model with different
description-generation components.  The description-generation components vary
the regularisation penalty (L1 norm in blue, L2 norm in red) and inverse
regularisation strength of the word prediction model. L1 regularization
outperforms L2 regularization because the number of features is high compared to
number of data-points \protect\cite{ng2004}. Strong regularization performs best
because it keeps the description-words relevant to the image-contents instead of
yielding to prior probabilities i.e.\ overly common words.}
\label{fig:description-generation-tuning}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\linewidth]{description-selection-bleu.png}
\caption{BLEU scores (higher is better) for a template model with different
description-selection components.  The description-selection components vary the
relative importance of the bigram (left axis), trigram (bottom axis) and length
(right axis) language models.  Better scores are denoted in red.  Worse scores
are in blue.}
\label{fig:description-selection}
\end{figure}

\begin{table}
\centering
\begin{tabular}{| l | c |}
\hline
Template Acquisition      & BLEU \\ \hline \hline
Baseline (fixed template) & 32.6 \\
Logistic Regression       & \textbf{39.9} \\
Random Forest             & 37.8 \\
Decision Tree             & 37.8 \\
Gaussian NB               & 11.3 \\
Multinomial NB            & 15.1 \\
\hline
\end{tabular}
\caption{BLEU scores (higher is better) for a template model with
different template-acquisition components.  The components use different
classifiers to predict templates for images. The baseline always uses the most
common template in the data-set: \emph{nsubj,aux,root,prep,det,dobj}.}
\label{tab:template-acquisition-impact}
\end{table}

\begin{table}
\centering
\begin{tabular}{| c | c c c c |}
\hline
\backslashbox{M}{N}
  & 1    & 2    & 3    & 4    \\ \hline \hline
1 & 15.8 & 26.2 & 32.0 & 33.8 \\
2 & 18.5 & 29.4 & 35.9 & 37.3 \\
3 & 19.0 & 29.7 & 36.4 & 38.1 \\
4 & 19.7 & 30.7 & 36.8 & \textbf{38.8} \\
5 & 19.7 & 30.7 & 36.6 & 38.4 \\
\hline
\end{tabular}
\caption{BLEU scores (higher is better) for a template model based on Logistic
Regression using up to $M=5$ candidate templates and up to $N=4$ candidate words
per template slot. Similar tables for other classifier types are omitted in this
report for reasons of brevity (Logistic Regression was found to produce the best
results).}
\label{tab:description-generation-performance}
\end{table}

\begin{table}
\centering
\begin{tabular}{| l | c |}
\hline
Model               & BLEU  \\ \hline \hline
Similarity Baseline & 12.80 \\
Keyword Baseline    & 14.70 \\
Template Model      & \textbf{40.30} \\
Human Agreement     & 21.17 \\
\hline
\end{tabular}
\caption{Comparison of image description correctness (as measured by BLEU
scores) for a template-based image description model and two baselines.
Human performance is included as a reference to enable better interpretation of
the scores.}
\label{tab:model-comparison-metrics}
\end{table}

\section{Results}

Figure~\ref{fig:model-comparison-images} shows sample descriptions generated by
our model.

We compare our model against two baselines that are rooted in the transfer-based
image description paradigm and that were inspired by the work of Farhadi et al.\
\shortcite{farhadi2010}. One of the baselines generates keywords for images and
uses those to retrieve descriptions. The other baseline uses image similarity
directly. We also follow best-practices
\cite{ordonez2011,yang2011,mitchell2012,elliott2013,krishnamoorthy2013} in the
image description community and perform a human-study on the crowd-sourcing
platform Amazon Mechanical Turk to validate our findings.

Our model out-performs the inter-annotator agreement in the ``Abstract Scenes
Data-Set'' (Table~\ref{tab:model-comparison-metrics}): our model creates
descriptions that have more commonalities with the union of all human
descriptions of an image than any singular human description on its own. This
means that our model is able to synthesize the most salient contents of multiple
human-written descriptions into a single, cohesive description.

Table~\ref{tab:template-acquisition-impact} shows that adapting the grammatical
structure of an image description to the image at hand increases the quality of
the generated descriptions. Using a classifier to find the optimal grammatical
structure for an image increases performance over a baseline that simply
describes all images using the most common grammatical structure in the
``Abstract Scenes Data-Set.'' Thus, not all images are equally well described by
all types of sentences. This finding supports our initial hypothesis that visual
attributes influence the way in which humans describe images; image-description
systems should follow suit.

We further find that our model consistently out-performs the baselines
(Table~\ref{tab:model-comparison-metrics}) and our human study finds that the
descriptions created by our model sound very natural and human-like:
participants in our experiments were unable to distinguish between the
descriptions created by our model and human-written descriptions (significant at
$p<0.05$). This means that our parallel approach to grammar-guided text
generation is effective at creating descriptions that not only capture the
essence of a picture but that are also fluently crafted.

\begin{sidewaysfigure*}
\centering
\setlength{\abovecaptionskip}{0pt plus 0pt minus 0pt}
\includegraphics[width=\textwidth]{model-comparison-images1-our.pdf}
\includegraphics[width=\textwidth]{model-comparison-images3-our.pdf}
\caption{Sample image descriptions generated by different models.}
\label{fig:model-comparison-images}
\end{sidewaysfigure*}

\clearpage
\bibliographystyle{acl}
\bibliography{bibliography}
\end{document}
