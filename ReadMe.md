# Automatic Image Description

## Project overview

The overall aim of this project is to implement a system that takes a pictures
as input and output a description of the image (in natural language).

Introductory material:

* [Project Proposal](http://goo.gl/RvzqMK)
* [Project Overview Slides](http://goo.gl/yuafxd)
* [Project Summary Slides](http://goo.gl/CMbscy)

The project is based on the "Abstract Scenes Data-Set" by [Zitnick and Parikh, 2013](http://courses.cs.washington.edu/courses/cse590v/13au/cse590v_au13_wk5_abstract_scenes.pdf).

The project is supervised by [Prof. Mirella Lapata](https://homepages.inf.ed.ac.uk/mlap/).

## Project structure

A detailed report on the project is in `report/report.pdf`.

There are a number of executable scripts in `src` that implement the models and
functionality proposed by this project:

  * `src/data_preparation/analyse_asds.sh` processes the Abstract Scenes
    Data-Set.  The script analyses the data-set's image descriptions (extract
    lemmas, parts of speech, typed dependencies, extract templates, etc) and
    creates a data-base linking visual and textual features and extracts
    templates.
  * `src/data_analysis/compute_mi.sh` computes the mutual information between
    visual features and word features in order to find out which visual features
    are predictive of word features.
    `src/data_analysis/dependency_wordcoverage` finds out which words are used
    in which dependency positions and how often.
    Taken together, these two scripts can be used for feature selection.
  * `src/template_baseline/template_baseline.py` implements the main
    contribution of the project: a template-based image description system.
    Given an image, the model first predicts some grammatical structures
    or "templates" that capture the semantics of the image (content planning).
    Then, the templates are transformed into descriptions by predicting words
    that are appropriate for the image and that match the grammatical slots in
    the templates (content selection and surface realisation).  In order to make
    the model produce more adequate and fluent descriptions, a re-ranking step
    can be used to post-process the model's outputs: `src/reranking/rerank.sh`.
    The script uses a language model to select the most sensible description
    among the template-model's outputs.
  * `src/information_retrieval_baseline/keyword_retrieval_baseline.sh`
    implements a transfer-based baseline image description generation system.
    The model maps images and descriptions into a shared meaning space defined
    by the most salient keywords for the image or description (noun, adjective,
    verb and so forth).  Within this space, tf-idf search is used to find close
    pairs.
  * `src/information_retrieval_baseline/picture_retrieval_baseline.sh`
    implements a second transfer-base baseline image description generation
    system.  The model searches for graphically similar images and lifts
    description off those images to describe new ones.
  * The output of the models can be evaluated using BLEU and METEOR by running
    `src/evaluation/evaluate.sh`.  The script
    `src/evaluation/human_agreement.py` computes the inter-annotator agreement
    on the Abstract Scenes Data-Set (used to ground the performance of the
    automatic models).

The raw experimental results reside in `data/analysis`.  Processed data and
intermediary results are stored in the project's data-base `data/sqlite3.db`.

The project has a number of dependencies that can be resolved by running
`lib/get_libs.sh`.
