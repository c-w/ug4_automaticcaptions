#!/bin/bash
# Fetches additional packages into the directory of the script
# Adds the packages to .gitignore to avoid cluttering the repository


set -e


# utility declarations
scriptdir="$(dirname $(readlink -f $0))"

function echo_emph() {
    echo -e "\e[44m$1\e[49m"
}

function python_version() {
    echo $(python -c "import sys; print '.'.join(map(str, sys.version_info[:2]))")
}

function add_to_gitignore() {
    path="$(basename $1)"
    gitignore="$scriptdir/.gitignore"
    ([[ -f "$gitignore" ]] && grep -q "$path" "$gitignore") || echo "$path" >> $gitignore
}


# -----------------------------------------------------------------------------#
# Stanford phrasal
# Reference:
#  Phrasal: A Toolkit for Statistical Machine Translation with Facilities for
#  Extraction and Incorporation of Arbitrary Model Features
#  Daniel Cer, Michel Galley, Daniel Jurafsky and Christopher Manning
#  In Proceedings of the North American Association of Computational Linguistics
#  - Demo Session (NAACL-10). 2010
# -----------------------------------------------------------------------------#
function get_stanford_phrasal() {
    local data_url='http://nlp.stanford.edu/software/phrasal/phrasal.3.3.1.tar.gz'
    local outpath="$scriptdir/$(basename $data_url .tar.gz)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Stanford Phrasal"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    tar -vzxf $tmpfile -C $scriptdir
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# Stanford core nlp
# Reference:
#   Kristina Toutanova and Christopher D. Manning. 2000. Enriching the Knowledge
#   Sources Used in a Maximum Entropy Part-of-Speech Tagger. In Proceedings of
#   the Joint SIGDAT Conference on Empirical Methods in Natural Language
#   Processing and Very Large Corpora (EMNLP/VLC-2000), pp. 63-70.
#
#   Jenny Rose Finkel, Trond Grenager, and Christopher Manning. 2005.
#   Incorporating Non-local Information into Information Extraction Systems by
#   Gibbs Sampling. Proceedings of the 43nd Annual Meeting of the Association
#   for Computational Linguistics (ACL 2005), pp. 363-370.
#   http://nlp.stanford.edu/~manning/papers/gibbscrf3.pdf
#
#  Dan Klein and Christopher D. Manning. 2003. Accurate Unlexicalized Parsing.
#  Proceedings of the 41st Meeting of the Association for Computational
#  Linguistics, pp. 423-430.
#
#  Marie-Catherine de Marneffe, Bill MacCartney and Christopher D. Manning.
#  2006. Generating Typed Dependency Parses from Phrase Structure Parses. In
#  LREC 2006.
# -----------------------------------------------------------------------------#
function get_stanford_corenlp() {
    local data_url='http://nlp.stanford.edu/software/stanford-corenlp-full-2013-11-12.zip'
    local outpath="$scriptdir/$(basename $data_url .zip)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Stanford CoreNLP"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $scriptdir
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# Apache Lucene
# -----------------------------------------------------------------------------#
function get_apache_lucene() {
    local data_url='http://www.mirrorservice.org/sites/ftp.apache.org/lucene/java/4.6.1/lucene-4.6.1.zip'
    local outpath="$scriptdir/$(basename $data_url .zip)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Apache Lucene"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $scriptdir
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# Manchester MI Toolbox
# Reference:
#   A. Pocock, JavaMI Toolbox, University of Manchester, 2012.
#
#   Conditional Likelihood Maximisation: A Unifying Framework for Information
#   Theoretic Feature Selection
#   Gavin Brown, Adam Pocock, Ming-Jie Zhao, and Mikel Luján
#      Journal of Machine Learning Research (JMLR). Volume 13, Pages 27-66, 2012.
# -----------------------------------------------------------------------------#
function get_manchester_mi_toolbox() {
    local data_url='http://www.cs.man.ac.uk/~pococka4/software/MIToolbox.zip'
    local outpath="$scriptdir/$(basename $data_url .zip)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Manchester MI Toolbox"
    local tmpfile=$(mktemp)
    mkdir -p $outpath
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $outpath
    local compile="$(find $outpath -name 'CompileMIToolbox.m')"
    pushd . > /dev/null && cd "$(dirname $compile)"
    matlab -r "$(basename $compile .m);exit"
    popd > /dev/null
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
    get_manchester_mi_toolbox_java
}
function get_manchester_mi_toolbox_java() {
    local data_url='http://www.cs.man.ac.uk/~pococka4/software/JavaMI.zip'
    local outpath="$scriptdir/$(basename $data_url .zip)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Manchester MI Toolbox (Java)"
    local tmpfile=$(mktemp)
    mkdir -p $outpath
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $outpath
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# Scikit-Learn
# Reference:
#   Scikit-learn: Machine Learning in Python, Pedregosa et al., JMLR 12, pp.
#   2825-2830, 2011.
# -----------------------------------------------------------------------------#
function get_scikit_learn() {
    local data_url='https://pypi.python.org/packages/source/s/scikit-learn/scikit-learn-0.14.1.tar.gz'
    local outpath="$scriptdir/$(basename $data_url .tar.gz)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Scikit-Learn"
    local tmpfile=$(mktemp)
    wget --no-check-certificate -O $tmpfile $data_url
    mkdir -p $outpath
    tar -vzxf $tmpfile -C $scriptdir
    pushd . > /dev/null
    cd $outpath
    python setup.py install --prefix .
    popd > /dev/null
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# Information Dynamics Toolbox
# Reference:
#   Joseph T. Lizier, "JIDT: An information-theoretic toolkit for studying the
#   dynamics of complex systems", 2013
# -----------------------------------------------------------------------------#
function get_information_dynamics_toolbox() {
    local data_url='https://information-dynamics-toolkit.googlecode.com/files/infodynamics-dist-0.1.4.zip'
    local outpath="$scriptdir/$(basename $data_url .zip)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Information Dynamics Toolbox"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $outpath
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# Multeval
# -----------------------------------------------------------------------------#
function get_multeval() {
    local data_url='https://github.com/jhclark/multeval.git'
    local outpath="$scriptdir/$(basename $data_url .git)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching MultEval"
    git clone $data_url
    pushd . > /dev/null && cd $outpath
    rm -rf .git
    ./multeval.sh eval --refs example/refs.test2010.lc.tok.en.* \
                       --hyps-baseline example/hyps.lc.tok.en.baseline.opt* \
                       --meteor.language en && errco=$? || errco=$?
    ant
    popd > /dev/null
    add_to_gitignore $outpath
    echo "done"
}


# -----------------------------------------------------------------------------#
# SQLite JDBC
# Reference:
#   Taro L. Saito, https://bitbucket.org/xerial/sqlite-jdbc
# -----------------------------------------------------------------------------#
function get_java_sqlite() {
    local data_url='https://bitbucket.org/xerial/sqlite-jdbc/downloads/sqlite-jdbc-3.7.2.jar'
    local outpath="$scriptdir/$(basename $data_url .jar)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching SQLite JDBC"
    mkdir -p $outpath
    wget -O $outpath/$(basename $data_url) $data_url
    add_to_gitignore $outpath
    echo "done"
}


# -----------------------------------------------------------------------------#
# MKSQLite
# Reference:
#   http://mksqlite.berlios.de/mksqlite_eng.html
# -----------------------------------------------------------------------------#
function get_matlab_sqlite() {
    local data_url='http://sourceforge.net/projects/mksqlite.berlios/files/mksqlite-1.12-src.zip'
    local outpath="$scriptdir/$(basename $data_url -src.zip)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching MKSQLite"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $outpath
    local compile="$(find $outpath -name 'buildit.m')"
    local fixme="$(find $outpath -name 'mksqlite.cpp')"
    sed -i 's,#include <string.h>,#include <string.h>\n#include <ctype.h>,' "$fixme"
    pushd . > /dev/null && cd "$(dirname $compile)"
    matlab -r "$(basename $compile .m);exit"
    popd > /dev/null
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# Ternary Plots
# Reference:
#   http://www.mathworks.co.uk/matlabcentral/fileexchange/7210-ternary-plots
# -----------------------------------------------------------------------------#
function get_ternary_plots() {
    local data_url='http://www.mathworks.co.uk/matlabcentral/fx_files/7210/1/ternary2.zip'
    local outpath="$scriptdir/$(basename $data_url .zip)"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Ternary Plots"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $outpath
    add_to_gitignore $outpath
    rm -f $tmpfile
    echo "done"
}

# parse arguments
[[ -z $@ ]] && echo "please specify package to fetch" && exit 1
for arg in "$@"; do
    case $arg in
        meteor|multeval) get_multeval ;;
        bleu|stanford-phrasal) get_stanford_phrasal ;;
        nlp|stanford-corenlp) get_stanford_corenlp ;;
        ml|scikit-learn) get_scikit_learn ;;
        cmi|infodynamics) get_information_dynamics_toolbox ;;
        dmi|manchester-mi) get_manchester_mi_toolbox ;;
        jsql|java-sqlite) get_java_sqlite ;;
        msql|matlab-sqlite) get_matlab_sqlite ;;
        matlab-ternary-plots) get_ternary_plots ;;
        search|apache-lucene) get_apache_lucene ;;
        *) echo "unknown package $arg"
    esac
done
