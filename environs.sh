#!/bin/bash
tld="$(git rev-parse --show-toplevel)"
libdir="$tld/lib"
srcdir="$tld/src"
bindir="$tld/bin"
datadir="$tld/data"
db="$datadir/sqlite3.db"

mlibs="$(find $libdir -name '*.m' -printf '%h\n' | sort -u | paste -sd' ')"
jlibs="$(find $libdir -name '*.jar' | sort -u | paste -sd' ')"
pylibs="$(find $libdir -name 'site-packages' | sort -u | paste -sd' ')"

pypath="$(echo $bindir $srcdir $pylibs | tr ' ' ':')"
export PYTHONPATH="$pypath:$PYTHONPATH"

mpath="$(echo $bindir $srcdir $mlibs | tr ' ' ':')"
export MATLABPATH="$mpath:$MATLABPATH"

javapath="$(echo $bindir $srcdir $jlibs | tr ' ' ':')"
export CLASSPATH="$javapath:$CLASSPATH"

binlibs=""
for d in $(find "$libdir" -type d -name "bin"); do
    binlibs="$(find $d -type d | paste -sd' ') $binlibs"
done
binpath="$(echo $binlibs | tr ' ' ':')"
export PATH="$binpath:$PATH"

# for srilm/gawk floating point arithmetic
export LC_NUMERIC=C

function log() {
    local date="$(date +%Y-%m-%d\ %H:%M:%S)"
    local CYAN="\e[0;36m"
    local END="\e[0m"
    echo -e "${CYAN}[$date,$$:INFO]${END} $@" 1>&2
}

function javac() {
    mkdir -p "$bindir"
    /usr/bin/javac -d "$bindir" -sourcepath "$srcdir" $@
}

function db_backup() {
    gzip $db --to-stdout > $db.bak.gz
}

function db_restore() {
    gunzip --to-stdout $db.bak.gz > $db
}

function db_exec() {
    echo "$1;" | sqlite3 $db
}

function db_conn() {
    sqlite3 $db
}

function run() {
    case "$1" in
        *.java)
            script=$(basename $1 .java) && shift
            javac $script.java
            java $script $@
            ;;
        *.py)
            python $@
            ;;
        *.sh)
            bash $@
            ;;
        *.m)
            pushd . > /dev/null && cd "$(dirname $1)"
            script=$(basename $1 .m) && shift
            args="$(echo "$@ " | sed "s/\([^ ][^ ]*\) /'\1', /g" | sed "s/, $//")"
            matlab -nodesktop -nodisplay -nosplash -r "${script}(${args}); exit"
            popd > /dev/null
            ;;
    esac
}
