from util.constants import COL_DELIM, COMMENT_DELIM
import fileinput


WEIGHTS = (0.3, 0.3, 0.4)  # bigram, trigram, length


def interpolate(scores, weights=WEIGHTS):
    return sum(score * weight for score, weight in zip(scores, weights))


def unpack(line):
    data = line.split(COL_DELIM)
    scores = map(float, data[2:])
    return (int(data[0]), data[1], interpolate(scores))


def filter_best(lines):
    image_id = None
    best_description = None
    best_score = None
    for line in lines:
        line = line.strip()
        if line.startswith(COMMENT_DELIM):
            yield line
        next_image_id, next_description, next_score = unpack(line)
        if image_id == next_image_id:
            if next_score > best_score:
                best_score = next_score
                best_description = next_description
        else:
            if image_id is not None:
                yield '%d%s%s' % (image_id, COL_DELIM, best_description)
            image_id = next_image_id
            best_description = next_description
            best_score = next_score
    yield '%d%s%s' % (image_id, COL_DELIM, best_description)


if __name__ == '__main__':
    for x in filter_best(fileinput.input()):
        print x
