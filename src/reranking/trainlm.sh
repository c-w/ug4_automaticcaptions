#!/bin/bash

[[ $# -lt 3 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

lmpath="$1"
model="$2"
corpuspath="$3"

pushd . > /dev/null && cd "$scriptdir"

case "$model" in
    len)
        python srilm.py -v train "$lmpath" "$corpuspath"
        ;;
    [1-9]gram)
        order="$(basename $model gram)"
        ngram-count -order "$order" -text "$corpuspath" -lm "$lmpath"
        ;;
esac

popd > /dev/null
