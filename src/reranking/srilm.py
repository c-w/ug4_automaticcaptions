from util.constants import COMMENT_DELIM
import collections
import math
import os
import re
import string
import tempfile
import util.learning as learning
import util.option_parser as option_parser
import util.paths as paths
import util.serialization as serialization
import util.utils as utils


__srilm_train__ = 'ngram-count -order {order:d} -text {corpus} -lm {lm}'
__srilm_use__ = 'ngram -debug 1 -order {order:d} -ppl {text} -lm {lm}'


class SRILMOutput(object):
    __info_t__ = collections.namedtuple(
        'SRILM_OUTPUT_T',
        'sentence zeroprobs logprob ppl ppl1 sentences words OOVs')
    __corpus_stats_re__ = re.compile(
        r"""
        (?P<sentences>[0-9]+)\s*sentences? # sentences
        ,\s*                               #
        (?P<words>[0-9]+)\s*words?         # words
        ,\s*                               #
        (?P<OOVs>[0-9]+)\s*OOVs?           # OOVs
        """, re.VERBOSE)
    __lm_stats_re__ = re.compile(
        r"""
        (?P<zeroprobs>([0-9]+|undefined))\s*zeroprobs?  # zeroprobs
        ,\s*                                            #
        logprob\s*=\s*(?P<logprob>([-0-9.]+|undefined)) # logprob
        \s*                                             #
        ppl\s*=\s*(?P<ppl>([-0-9.]+|undefined))         # ppl
        \s*                                             #
        ppl1\s*=\s*(?P<ppl1>([-0-9.]+|undefined))       # ppl1
        """, re.VERBOSE)

    def __init__(self, output):
        self._data = []
        lines = output.split('\n')
        # there are four lines of SRILM output for every input line
        # 1. the original input sentence at verbatim
        # 2. number of sentences/words/out-of-vocabulary words
        # 3. language model probabilities and perplexities
        # 4. a blank line
        for i in xrange(0, len(lines) - 2, 4):
            sentence = lines[i].strip()
            corpus_stats = SRILMOutput.__corpus_stats_re__.search(lines[i + 1])
            lm_stats = SRILMOutput.__lm_stats_re__.search(lines[i + 2])
            self._data.append(SRILMOutput.__info_t__(
                sentence=str(sentence),
                zeroprobs=numcast(lm_stats.group('zeroprobs')),
                logprob=numcast(lm_stats.group('logprob')),
                ppl=numcast(lm_stats.group('ppl')),
                ppl1=numcast(lm_stats.group('ppl1')),
                sentences=numcast(corpus_stats.group('sentences')),
                words=numcast(corpus_stats.group('words')),
                OOVs=numcast(corpus_stats.group('OOVs')),
            ))
        # the last two lines are summary statistics
        corpus_stats = SRILMOutput.__corpus_stats_re__.search(lines[-2])
        lm_stats = SRILMOutput.__lm_stats_re__.search(lines[-1])
        self.zeroprobs = numcast(lm_stats.group('zeroprobs'))
        self.logprob = numcast(lm_stats.group('logprob'))
        self.ppl = numcast(lm_stats.group('ppl'))
        self.ppl1 = numcast(lm_stats.group('ppl1'))
        self.sentences = numcast(corpus_stats.group('sentences'))
        self.words = numcast(corpus_stats.group('words'))
        self.OOVs = numcast(corpus_stats.group('OOVs'))

    def __iter__(self):
        return iter(self._data)

    def __getitem__(self, n):
        return self._data[n]


class LanguageModel(serialization.WritableObject):
    def __init__(self, order=3):
        self.order = order

    def fit(self, corpus_path):
        clean_corpus = _clean_copy(corpus_path)
        self._lm = _train_srilm(clean_corpus, order=self.order)
        self._lengths = _count_lenghts(clean_corpus)
        self.total_freq = sum(self._lengths.itervalues())
        os.remove(clean_corpus)

    def sentence_length_log_proba(self, sentences):
        length_get = self._lengths.get
        total_freq = self.total_freq
        log = math.log
        neg_inf = float('-inf')
        for sentence in sentences:
            freq = length_get(sentence_length(sentence), 0)
            try:
                logprob = log(freq) - log(total_freq)
            except ValueError:
                logprob = neg_inf
            yield logprob

    def predict_log_proba(self, sentences):
        clean_input = _clean_copy(sentences)
        output = _use_srilm(clean_input, lm=self._lm, order=self.order)
        os.remove(clean_input)
        return [p.logprob for p in output]


# helper functions for SRILM wrapper classes


def _count_lenghts(corpus):
    lengths = collections.defaultdict(int)
    with open(corpus) as f:
        for line in f:
            lengths[sentence_length(line)] += 1
    return lengths


def _train_srilm(corpus, order):
    with tempfile.NamedTemporaryFile() as outfile:
        utils.run(__srilm_train__.format(
            order=order, corpus=corpus, lm=outfile.name))
        lm = [line for line in outfile]
    return lm


def _use_srilm(corpus, lm, order):
    with tempfile.NamedTemporaryFile() as lmfile:
        lmfile.write(''.join(line for line in lm))
        lmfile.flush()  # wait for the write to finish
        stdout, _ = utils.run(__srilm_use__.format(
            order=order, text=corpus, lm=lmfile.name),
            log_stdout=False)
    return SRILMOutput(stdout)


def _clean_copy(orig_input, comment_delim='#'):
    try:
        orig_data = open(paths.normalize(orig_input))
        input_is_file = True
    except:
        input_is_file = False
        if isinstance(orig_input, list):
            orig_data = list(orig_input)
        else:
            orig_data = orig_input.split('\n')
    copy_file = tempfile.NamedTemporaryFile('w', delete=False)
    try:
        for line in orig_data:
            if line.strip().startswith(comment_delim):
                continue
            copy_file.write(line)
            if not line.endswith('\n'):
                copy_file.write('\n')
    finally:
        if input_is_file:
            orig_data.close()
        copy_file.close()
    return copy_file.name


# potentially useful functions


def sentence_length(line, punct=set(string.punctuation)):
    return sum(1 for word in line.split()
               if any(c not in punct for c in word))


def numcast(s):
    try:
        return int(s)
    except ValueError:
        pass
    try:
        return float(s)
    except ValueError:
        pass
    if s.lower() == 'undefined':
        return None
    raise NotImplementedError()


if __name__ == '__main__':
    output_types = {
        'ngram-log-prob': lambda model: model.predict_log_proba,
        'length-log-prob': lambda model: model.sentence_length_log_proba,
    }
    parser = option_parser.OptionParser(
        epilog=('\nIf mode=%s:'
                '\nUse the model at path/to/model to compute log '
                'probabilities for the sentences at path/to/corpus.'
                '\n\nIf mode=%s:'
                '\nTrain a language model using the corpus at path/to/corpus '
                'and save it at path/to/model.'
                % (learning.MODES.PREDICT, learning.MODES.TRAIN)))
    parser.add_positional_argument('mode', type='choice',
                                   choices=learning.MODES)
    parser.add_positional_argument('path/to/model', type='string')
    parser.add_positional_argument('path/to/corpus', type='file')
    parser.add_option('--order', metavar='N', type='int',
                      dest='order', default=3,
                      help='order of N-grams to use for language model')
    parser.add_option('--output-type', metavar='T', choices=list(output_types),
                      dest='output_type', default='ngram-log-prob',
                      help='change type of probabilities to output')
    opts, args = parser.parse_args()
    # unpack opts and args
    mode = args[0]
    model_path = paths.normalize(args[1])
    corpus_path = paths.normalize(args[2])
    order = opts.order
    output_fun = output_types[opts.output_type]

    # train or use model
    if mode == learning.MODES.TRAIN:
        model = LanguageModel(order=order)
        model.fit(corpus_path)
        model.write(model_path)
    elif mode == learning.MODES.PREDICT:
        model = LanguageModel.read(model_path)
        with open(corpus_path) as corpus:
            lines = (l for l in corpus if not l.startswith(COMMENT_DELIM))
            logprobs = output_fun(model)(lines)
            for p in logprobs:
                print p
