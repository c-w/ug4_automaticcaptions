#!/bin/bash

[[ $# -lt 3 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

lmpath="$1"
model="$2"
tsvpath="$3"

pushd . > /dev/null && cd "$scriptdir"

corpus="$(mktemp)"
less "$tsvpath" | cut -f2 | grep -v '^#' > "$corpus"

case "$model" in
    len)
        python srilm.py -v predict "$lmpath" "$corpus" --output-type='length-log-prob'
        ;;
    [1-9]gram)
        order="$(basename $model gram)"
        ngram -order "$order" -ppl "$corpus" -lm "$lmpath" -debug 1 \
        | sed 'N;$!P;$!D;$d' \
        | grep 'logprob=' \
        | sed 's,^.*logprob= \([-0-9.]*\).*$,\1,'
        ;;
esac

popd > /dev/null

rm -rf "$corpus"
