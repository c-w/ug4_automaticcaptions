#!/bin/bash

[[ $# -lt 1 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

tsvpath="$(readlink -f $1)"
lmdir="$tldir/bin/language_models"

function uselm() {
    local model="$1"
    run "$scriptdir/uselm.sh" "$lmdir/$model.model" "$model" "$tsvpath"
}

function echo_header() {
    less "$1" | grep '^#'
    echo "# Re-ranked:2gram,3gram,length"
}

function rerank() {
    paste \
        <(less "$1" | grep -v '^#') \
        <(paste \
            <(uselm '2gram') \
            <(uselm '3gram') \
            <(uselm 'len')) \
    | python "$scriptdir/filterbest.py"
}

echo_header "$tsvpath"
rerank "$tsvpath"
