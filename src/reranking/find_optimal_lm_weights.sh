#!/bin/bash

[[ $# -lt 2 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

readonly model="$(readlink -f $1)"
readonly outdir="$(readlink -f $2)/$(basename $model .tsv.gz)"
readonly wip="$outdir/.nums-wip"
readonly finished="$outdir/.nums-done"
readonly reranker="$tldir/src/reranking/rerank.sh"
readonly evaluator="$tldir/src/evaluation/evaluate.sh"
readonly plotter="$tldir/src/util/ternary_plot.m"

function rerank() {
    local fname="$1"
    local w2="$2"
    local w3="$3"
    local wl="$4"
    nice $reranker $fname $w2 $w3 $wl \
        > "$outdir"/2gram@${w2}_3gram@${w3}_len@${wl}.tsv
}

function evaluate() {
    local fname="$1"
    grep -qi "bleu" "$fname" && grep -qi "meteor" "$fname" && return
    nice $evaluator "$fname" >> "$fname"
}

function flag_doing() {
    log "w2=$1 w3=$2 wl=$3"
}

function flag_done() {
   echo "$w2 $w3 $wl" >> "$finished"
}

function getweight() {
    local fname="$1"
    local weight="$2"
    local w="$(basename "$fname" .tsv | cut -d'_' -f$weight | cut -d'@' -f2)"
    echo "scale=4; $w/100" | bc
}

function getscore() {
    local fname="$1"
    local metric="$2"
    grep '^#' "$fname" \
    | grep -i "$metric" \
    | sed "s,.*$metric[^0-9.-]*\([0-9.-]*\),\1,"
}

function plot() {
    local data="$1"
    local ncol="$2"
    local metric="$(head -1 $data | cut -d',' -f$ncol | tr ' ' '-')"
    local outp="$outdir/$(echo $metric | tr 'A-Z' 'a-z').png"
    local fold="$(mktemp)"
    cut -d',' -f1-3,$ncol "$data" > "$fold"
    run $plotter "$(readlink -f $fold)" "$(readlink -f $outp)"
    rm -f "$fold"
}

[[ ! -f "$wip" ]] && seq 0 10 100 > "$wip"
touch "$finished"
mkdir -p "$outdir"
rm -rf /tmp/* 2>/dev/null && errco=$? || errco=$?

while read w2; do
    while read w3; do
        while read wl; do
            # validate weights
            [[ $((w2+$w3+$wl)) -ne 100 ]] && continue
            grep -q "$w2 $w3 $wl" "$finished" && continue
            # rerank using weights
            flag_doing $w2 $w3 $wl
            rerank $model $w2 $w3 $wl
            flag_done $w2 $w3 $wl
        done < "$wip"
    done < "$wip"
done < "$wip"
log "done reranking with different weights"

for f in $outdir/*.tsv; do
    evaluate $f
done
log "done with evaluation"

scores="$outdir/summary.csv"
echo "Bigram-component weight,Trigram-component weight,Length-component weight,BLEU score,METEOR score,TER score" >> "$scores"
for f in $outdir/*.tsv; do
    w2="$(getweight $f 1)"
    w3="$(getweight $f 2)"
    wl="$(getweight $f 3)"
    bleu="$(getscore $f 'BLEU')"
    meteor="$(getscore $f 'METEOR')"
    ter="$(getscore $f 'TER')"
    echo "$w2,$w3,$wl,$bleu,$meteor,$ter" >> "$scores"
done

for ncol in 4 5 6; do
    plot $scores $ncol
done
log "done with plotting"
