#!/usr/bin/env python


import collections
import glob
import logging
import os
import parse_multeval
import shutil
import tempfile
import util.option_parser as option_parser
import util.paths as paths
import util.stats as stats
import util.utils as utils


if __name__ == '__main__':
    parser = option_parser.OptionParser()
    opts, args = parser.parse_args()

    tldir = utils.tldir()
    dbpath = os.path.join(tldir, 'data', 'sqlite3.db')
    scriptdir = os.path.dirname(os.path.realpath(__file__))
    refsdir = tempfile.mkdtemp()
    refprefix = 'ref-%s-' % utils.random_string(nchars=6)
    evaldir = os.path.join(tldir, 'lib', 'multeval')
    curdir = os.getcwd()

    utils.run(('python {scriptdir}/make_refs.py {verbose} {dbpath} {refsdir} '
               '--ref-prefix={refprefix} --largest-common-set --no-blank'
               .format(verbose=('-v ' * opts.verbose),
                       scriptdir=scriptdir,
                       refprefix=refprefix,
                       refsdir=refsdir,
                       dbpath=dbpath)))
    references = set(map(paths.normalize,
                         glob.glob(os.path.join(refsdir, refprefix + '*'))))

    os.chdir(evaldir)
    metrics = collections.defaultdict(list)
    for hyp in sorted(references):
        logging.info('hypothesis: %s' % os.path.basename(hyp))
        refs = sorted(references - set([hyp]))
        stdout = utils.run(
            ('./multeval.sh eval '
             '--refs {refs} '
             '--hyps-baseline {hyp} '
             '--meteor.language en '
             '--metrics bleu meteor ter')
            .format(refs=' '.join(refs), hyp=hyp),
            log_stderr=True)[0].split('\n')
        for metric, value in parse_multeval.parse(stdout):
            metrics[metric].append(value)

    for metric, values in metrics.iteritems():
        logging.info('%s: %s' % (metric, ', '.join(map(str, values))))
    for metric, values in metrics.iteritems():
        print '# %s:%.2f' % (metric, stats.mean(values))

    os.chdir(curdir)
    shutil.rmtree(refsdir)
