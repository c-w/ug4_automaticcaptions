import fileinput
import itertools


METRICS = ('BLEU', 'METEOR', 'TER', 'Length')
MULTEVAL_COMMENT_DELIM = '*'


def parse(lines):
    lines = (l.strip() for l in lines
             if not l.strip().startswith(MULTEVAL_COMMENT_DELIM))
    data = dict(itertools.izip(*(l.split() for l in lines)))
    for metric in METRICS:
        if metric in data:
            yield (metric, float(data[metric]))


if __name__ == '__main__':
    for metric, value in parse(fileinput.input()):
        print '# %s:%.2f' % (metric, value)
