import logging
import os
import sqlite3
import util.option_parser as option_parser
import util.paths as paths


def get_descriptions(db_path):
    descriptions = {}
    with sqlite3.connect(db_path) as db_connection:
        rows = ((int(image_id), int(description_id), description)
                for (image_id, description_id, description)
                in db_connection.execute(
                    'SELECT image_id, '
                    '       description_id, '
                    '       GROUP_CONCAT(LOWER(raw_word), " ") '
                    'FROM ( '
                    '    SELECT raw_word, image_id, description_id '
                    '    FROM words '
                    '    WHERE is_punctuation=0 '
                    '    AND image_id IN evaluation_set '
                    '    ORDER BY sentence_idx, word_idx) '
                    'GROUP BY image_id, description_id '))
        for image_id, description_id, description in rows:
            descriptions[(image_id, description_id)] = description
    logging.info('loaded %d descriptions' % len(descriptions))
    return descriptions


def burst(descriptions, refs_dir, ref_prefix='ref', no_blank=False):
    max_image_id = max(a for a, _ in descriptions.iterkeys())
    max_description_id = max(b for _, b in descriptions.iterkeys())
    logging.info('max image id: %d' % max_image_id)
    logging.info('max description id: %d' % max_description_id)
    for description_id in xrange(max_description_id + 1):
        logging.info('processing description id %d' % description_id)
        fname = os.path.join(refs_dir, '%s%d' % (ref_prefix, description_id))
        paths.makedirs(fname)
        with open(fname, 'w') as f:
            for image_id in xrange(max_image_id + 1):
                desc = descriptions.get((image_id, description_id), '')
                if no_blank and not desc:
                    logging.info(
                        'skipped blank: image_id=%d, description_id=%d'
                        % (image_id, description_id))
                    continue
                f.write(desc + '\n')


def reduce_to_largest_common_set(descriptions, low=5, no_blank=False):
    # FIXME make more general - determine |low| automatically
    oldlen = len(descriptions)
    lowdel, highdel = 0, 0
    image_ids = set((image_id for image_id, _ in descriptions.iterkeys()))
    for image_id in image_ids:
        if descriptions.get((image_id, low)) is None:
            delete_from = 0 if no_blank else (low + 1)
            lowdel += 1
        else:
            delete_from = low + 1
            highdel += 1
        while True:
            try:
                del descriptions[(image_id, delete_from)]
                delete_from += 1
            except KeyError:
                break
    logging.info('deleted %d descriptions (%d low, %d high)'
                 % (oldlen - len(descriptions), lowdel, highdel))


if __name__ == '__main__':
    parser = option_parser.OptionParser(
        epilog=(''))
    parser.add_positional_argument('path/to/db', type='file')
    parser.add_positional_argument('path/to/refs/dir', type='string')
    parser.add_option('--largest-common-set', action='store_true',
                      default=False, dest='largest_common_set',
                      help=(''))
    parser.add_option('--no-blank', action='store_true',
                      default=False, dest='no_blank',
                      help=(''))
    parser.add_option('--ref-prefix', type='string',
                      default='ref', dest='ref_prefix',
                      help=(''))
    opts, args = parser.parse_args()
    # unpack opts and args
    db_path = paths.normalize(args[0])
    refs_dir = paths.normalize(args[1])
    largest_common_set = opts.largest_common_set
    no_blank = opts.no_blank
    ref_prefix = opts.ref_prefix

    descriptions = get_descriptions(db_path)
    if largest_common_set:
        reduce_to_largest_common_set(descriptions, no_blank=no_blank)
    burst(descriptions, refs_dir, ref_prefix=ref_prefix, no_blank=no_blank)
