#!/bin/bash

[[ $# -lt 1 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

function make_refs() {
    local inpath="$1"
    local outpath="$2"
    local refsdir="$tldir/bin/refs"
    local dbpath="$tldir/data/sqlite3.db"

    [[ -d "$refsdir" ]] && rm -rf "$refsdir"
    python "$scriptdir/make_refs.py" -v "$dbpath" "$refsdir" \
           --largest-common-set

    mkdir -p "$outpath"
    sedlines=$(less $inpath | grep -v '^#' | cut -f1 | \
    python -c "import fileinput;print 'p;'.join(str(int(l.strip())+1) for l in fileinput.input())+'p;'")
    for ref in $refsdir/*; do
        sed -n "$sedlines" "$ref" > "$outpath/$(basename $ref)"
    done
}

function make_hyps() {
    less "$1" | grep -v '^#' | cut -f2 > "$2"
}

function evaluate() {
    local references="$1"
    local hypotheses="$2"
    local evaldir="$tldir/lib/multeval"

    pushd . > /dev/null && cd "$evaldir"
    ./multeval.sh eval --refs $references/* \
                       --hyps-baseline $hypotheses \
                       --meteor.language en \
                       --metrics bleu meteor ter \
    | python "$scriptdir/parse_multeval.py"
    popd > /dev/null
}

tsvpath="$(readlink -f $1)"
refs="$(mktemp -d)"
hyps="$(mktemp)"

make_refs "$tsvpath" "$refs"
make_hyps "$tsvpath" "$hyps"
evaluate "$refs" "$hyps"

rm -rf "$hyps" "$refs"
