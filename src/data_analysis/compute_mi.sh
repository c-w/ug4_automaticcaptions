#!/bin/bash

[[ $# -lt 0 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

function sum_cols() {
    paste $@ | python -c 'import sys;[sys.stdout.write(
    "%s\n" % sum(map(float, l.strip().split()))) for l in sys.stdin]'
}

sqlite="$tldir/data/sqlite3.db"
pushd . > /dev/null && cd $scriptdir

dmi="$(mktemp)"
cmi="$(mktemp)"
tmi="$(mktemp)"

run compute_discrete_mi.m "$sqlite" "$dmi"
run compute_continuous_mi.java "$sqlite" "$cmi"

echo '# Total MI,# Discrete Feature MI,# Continuous Feature MI'
sum_cols "$dmi" "$dmi" > "$tmi"
paste -d',' "$tmi" "$dmi" "$cmi"

rm -f "$dmi" "$cmi" "$tmi"
popd > /dev/null
