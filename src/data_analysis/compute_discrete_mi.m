function compute_discrete_mi(db_path, out_path)
    % load features
    fprintf(2, '[%s] reading wf\n', datestr(clock));
    wfs = DatabaseUtils.get_word_features(db_path);
    fprintf(2, '[%s] reading vfd\n', datestr(clock));
    vfs = DatabaseUtils.get_visual_features_discrete(db_path);
    % binarize word features: we don't care about exact count
    wfs(wfs > 0) = 1;
    % compute and output mi
    dmi = discrete_mi(wfs, vfs);
    fprintf(2, '[%s] writing mi\n', datestr(clock));
    dlmwrite(out_path, dmi, ',');
end


function mis = discrete_mi(wfs, vfs)
    [~, n_wfs] = size(wfs);
    [~, n_vfs] = size(vfs);
    mis = zeros(n_wfs, 1);
    for i = 1:n_wfs
        fprintf(2, '[%s] computing mi for wf %d/%dx%d\n', ...
                datestr(clock), i, n_wfs, n_vfs);
        wf = wfs(:,i);
        for k = 1:n_vfs
           vf = vfs(:,k);
           mis(i) = mis(i) + mi(wf, vf);
        end
    end
end
