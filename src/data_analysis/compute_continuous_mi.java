import infodynamics.measures.continuous.MutualInfoCalculatorMultiVariateWithDiscrete;
import infodynamics.measures.continuous.kernel.MutualInfoCalculatorMultiVariateWithDiscreteKernel;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;
import util.ArrayUtils;
import util.DatabaseUtils;
import util.OptionParser;


public class compute_continuous_mi {
    private compute_continuous_mi() {}
    private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());

    public static void main(String[] args)
    throws Exception {
        // parse arguments
        OptionParser parser = new DatabaseUtils.DatabaseScriptOptionParser();
        parser.add_positional_argument("out-path", "out_path", "string", null);
        parser.parse_args(args);
        String path_to_db = parser.get_file_argument(DatabaseUtils.DatabaseScriptOptionParser.PATH_TO_DB_ARG).getAbsolutePath();
        String out_path = parser.get_string_argument("out_path");

        // create a database connection
        Class.forName("org.sqlite.JDBC");
        Connection db_connection = null;
        int[][] wfs = null;
        double[][] vfc = null;
        try {
            db_connection = DriverManager.getConnection("jdbc:sqlite:" + path_to_db);
            // load features, binarize word features
            wfs = ArrayUtils.binarize(DatabaseUtils.get_word_features(db_connection), 0);
            LOGGER.info("loaded " + (wfs.length) + "x" + (wfs[0].length) + " wf matrix");
            vfc = DatabaseUtils.get_visual_features_continuous(db_connection);
            LOGGER.info("loaded " + (vfc.length) + "x" + (vfc[0].length) + " vfc matrix");
        } finally { if (db_connection != null) { db_connection.close(); } }

        // compute mi
        double[] cmi = new double[wfs[0].length];
        for (int i = 0; i < cmi.length; i++) {
            LOGGER.info("computing mi for wf " + (i + 1) + "/" + wfs[0].length + "x" + vfc[0].length);
            int[] wf = ArrayUtils.get_column(i, wfs);
            cmi[i] = continuous_mi(wf, vfc);
        }

        // output mi
        LOGGER.info("writing mi");
        ArrayUtils.write(cmi, out_path);
    }

    private static double continuous_mi(int[] wf, double[][] vfc)
    throws Exception {
        int continuous_dimensions = vfc[0].length;
        int discrete_dimensions = 2;
        MutualInfoCalculatorMultiVariateWithDiscrete c = new MutualInfoCalculatorMultiVariateWithDiscreteKernel();
        c.initialise(continuous_dimensions, discrete_dimensions);
        c.setObservations(vfc, wf);
        return c.computeAverageLocalOfObservations();
    }
}
