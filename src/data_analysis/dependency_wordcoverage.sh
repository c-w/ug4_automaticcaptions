#!/bin/bash
# Analyzes which words are used for which dependency tags
#
# Usage:
#   dependency_wordcoverage.sh path/to/output/dir

[[ $# -lt 1 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

outdir="$1"

tags="$(db_exec 'SELECT template FROM template_histogram')"
for dependency_tag in $(echo "$tags" | tr ',' '\n' | sort | uniq); do
    tmptable="$(mktemp -u freq_XXXX)"
    output="$outdir/$dependency_tag-freq.tsv"
    echo "processing $dependency_tag" 1>&2
    echo -e "# Coverage\t# Frequency\t# Lemma\t# Candidates" > "$output"
    db_exec "CREATE TABLE $tmptable AS
             SELECT COUNT(*) AS f,
                    lemma,
                    GROUP_CONCAT(DISTINCT LOWER(raw_word)) AS candidates
             FROM words
             WHERE dependency LIKE '$dependency_tag'
             GROUP BY lemma"
    db_exec "SELECT 100.*f/(SELECT SUM(f)+0. FROM $tmptable) AS coverage,
                    f as frequency,
                    lemma,
                    candidates
             FROM $tmptable
             ORDER BY frequency DESC" | tr '|' '\t' >> "$output"
    db_exec "DROP TABLE $tmptable"
    processed=$((processed+1))
done
