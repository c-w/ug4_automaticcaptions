#!/bin/bash

[[ $# -lt 0 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

datadir="$tldir/data"
dataset="$datadir/AbstractScenes_v1"
sqlite="$datadir/sqlite3.db"
folds="0.7 0.2 0.09 0.01"

pushd . > /dev/null && cd $scriptdir

run cleanup_dataset.sh "$dataset" -v
run db_putimages.py "$dataset" "$sqlite" -v
run db_putdescriptions.py "$dataset" "$sqlite" -v
run db_putvisualfeatures.sh "$dataset" "$sqlite" -v
run db_sentenceanalyze.java "$sqlite" -v
run db_lemmavectorize.java "$sqlite" -v
run db_templategenerate.py "$sqlite" -v
run db_split.py "$sqlite" $folds -v

popd > /dev/null
