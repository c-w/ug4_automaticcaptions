import glob
import logging
import os
import sqlite3
import util.option_parser as option_parser


IMAGE_EXT = 'png'
IMAGES_DIR = 'RenderedScenes'


def images2db(data_dir, db_connection):
    db_connection.execute(
        'CREATE TABLE IF NOT EXISTS images ('
        '  image_id INTEGER,'
        '  image_path TEXT,'
        '  PRIMARY KEY (image_id)'
        ')')
    paths = glob.glob(os.path.join(data_dir, IMAGES_DIR, '*.' + IMAGE_EXT))
    logging.info('loading images from %d paths' % len(paths))
    for path in sorted(paths):
        image_id = _get_image_ident(path)
        image_path = os.path.abspath(path)
        db_connection.execute(
            'INSERT OR IGNORE INTO images VALUES (?, ?)',
            (image_id, image_path))
    db_connection.commit()


def _get_image_ident(path):
    # the image "SceneA_B.png" is the Bth variant of the Ath concept
    scene_ident = os.path.basename(path).strip('Scene.%s' % IMAGE_EXT)
    scene_class, scene_index = map(int, scene_ident.split('_'))
    image_id = scene_class * 10 + scene_index
    return image_id


if __name__ == '__main__':
    # parse arguments
    parser = option_parser.OptionParser(
        epilog=('\nPuts the images from the AbstractScenes_v1 dataset into a '
                'sqlite database.'
                '\n\n~~~~~Schema~~~~~'
                '\nimages(*image_id, image_path)'))
    parser.add_positional_argument('path/to/abstract-scenes-v1', type='file')
    parser.add_positional_argument('path/to/db', type='string')
    parser.add_option('--image-ext', metavar='E', type='string',
                      dest='image_ext', action='store', default=IMAGE_EXT,
                      help='specify file extension of image files')
    opts, args = parser.parse_args()
    # unpack opts and args
    IMAGE_EXT = opts.image_ext
    data_dir = os.path.abspath(args[0])
    db_path = os.path.abspath(args[1])

    try:
        # connect to db
        db_connection = sqlite3.connect(db_path)
        # process data and store in db
        images2db(data_dir, db_connection)
    finally:
        db_connection.close()
