[[ $# -lt 1 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

datadir="$1"
vf="$datadir/VisualFeatures"

# cleanup visual features
for f in "$vf"/*.txt; do
    dos2unix -q "$f"
    sed -i "s,\t$,," "$f"
    log "cleaned up: $f"
done
