import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import util.DatabaseUtils;
import util.LingUtils;
import util.OptionParser;
import util.Utils;


public class db_sentenceanalyze {
    private db_sentenceanalyze() {}
    private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());

    public static void main(String[] argv)
    throws Exception {
        // process arguments
        OptionParser parser = new DatabaseUtils.DatabaseScriptOptionParser(
            "Analyzes the sentences in the sqlite database:" +
            "\n  - Sentence- and word-level tokenization" +
            "\n  - Part of speech tagging" +
            "\n  - Dependency parsing" +
            "\n  - Lemmatization" +
            "\n  - Stopwording" +
            "\n  - Named-entity-recognition" +
            "\n" +
            "\n~~~~~Schema~~~~~" +
            "\nsentences(*image_id, *description_id, *sentence_idx, " +
            "raw_sentence, parse_tree)" +
            "\nwords(*image_id, *description_id, *sentence_idx, *word_idx, " +
            "is_punctuation, is_function_word, raw_word, lemma, " +
            "part_of_speech, dependency, named_entity)");
        parser.parse_args(argv);
        Integer limit = parser.get_integer_option(DatabaseUtils.DatabaseScriptOptionParser.LIMIT_OPT);
        Integer offset = parser.get_integer_option(DatabaseUtils.DatabaseScriptOptionParser.OFFSET_OPT);
        Integer batch_size = parser.get_integer_option(DatabaseUtils.DatabaseScriptOptionParser.BATCH_SIZE_OPT);
        String path_to_db = parser.get_file_argument(DatabaseUtils.DatabaseScriptOptionParser.PATH_TO_DB_ARG).getAbsolutePath();

        // create a database connection
        Class.forName("org.sqlite.JDBC");
        Connection db_connection = null;
        try {
            db_connection = DriverManager.getConnection("jdbc:sqlite:" + path_to_db);
            db_connection.setAutoCommit(false);

            // setup nlp pipeline
            Properties props = new Properties();
            props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse");
            StanfordCoreNLP nlp_pipeline = new StanfordCoreNLP(props);

            try {
                // setup tables and statements
                PreparedStatement insert_sent = create_sentences_table(db_connection);
                PreparedStatement insert_word = create_words_table(db_connection);
                Statement s = db_connection.createStatement();

                // fetch and process all descriptions
                try {
                    ResultSet r = s.executeQuery(
                        "SELECT image_id, description_id, raw_text FROM descriptions" +
                        ((limit != null)  ? (" LIMIT " + limit) : "") +
                        ((offset != null) ? (" OFFSET " + offset) : ""));
                    try {
                        for (int row_num = 1; r.next(); row_num++) {
                            // fetch next description
                            int image_id = r.getInt("image_id");
                            int description_id = r.getInt("description_id");
                            String raw_text = r.getString("raw_text");
                            // annotate using nlp pipeline
                            Annotation text = new Annotation(raw_text);
                            nlp_pipeline.annotate(text);

                            // process sentences
                            int sentence_idx = 0;
                            for (CoreMap sentence : text.get(SentencesAnnotation.class)) {
                                String raw_sentence = sentence.get(TextAnnotation.class);
                                Tree parse_tree = sentence.get(TreeAnnotation.class);
                                // insert sentence into db
                                insert_sent.clearParameters();
                                insert_sent.setInt(1, image_id);
                                insert_sent.setInt(2, description_id);
                                insert_sent.setInt(3, sentence_idx);
                                insert_sent.setString(4, raw_sentence);
                                insert_sent.setString(5, parse_tree.toString());
                                insert_sent.executeUpdate();

                                // process words
                                Map<String, String> dependencies = get_typed_dependencies(parse_tree);
                                int word_idx = 0;
                                for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                                    String raw_word = token.get(TextAnnotation.class).toLowerCase();
                                    String lemma = token.get(LemmaAnnotation.class).toLowerCase();
                                    int is_punctuation = Utils.bool2int(
                                        LingUtils.is_punctuation(raw_word) ||
                                        LingUtils.is_punctuation(lemma));
                                    int is_function_word = Utils.bool2int(
                                        LingUtils.is_function_word(raw_word) ||
                                        LingUtils.is_function_word(lemma));
                                    String part_of_speech = token.get(PartOfSpeechAnnotation.class);
                                    String named_entity = token.get(NamedEntityTagAnnotation.class);
                                    String dependency = dependencies.get(raw_word);
                                    // insert word into db
                                    insert_word.clearParameters();
                                    insert_word.setInt(1, image_id);
                                    insert_word.setInt(2, description_id);
                                    insert_word.setInt(3, sentence_idx);
                                    insert_word.setInt(4, word_idx);
                                    insert_word.setInt(5, is_punctuation);
                                    insert_word.setInt(6, is_function_word);
                                    insert_word.setString(7, raw_word);
                                    insert_word.setString(8, lemma);
                                    insert_word.setString(9, part_of_speech);
                                    insert_word.setString(10, dependency);
                                    insert_word.setString(11, named_entity);
                                    insert_word.executeUpdate();
                                    word_idx++;
                                }
                                sentence_idx++;
                            }
                            if (row_num % batch_size == 0) {
                                db_connection.commit();
                                LOGGER.info("processed " + row_num);
                            }
                        }
                    } finally { r.close(); }
                } finally { s.close(); insert_word.close(); insert_sent.close(); }
            } finally { db_connection.commit(); LOGGER.info("processed rest"); }
        } finally {
            if (db_connection != null) { db_connection.close(); }
        }
    }

    /* NLP utilities */

    private static Map<String, String> get_typed_dependencies(Tree tree) {
        TreebankLanguagePack tlp = new PennTreebankLanguagePack();
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
        Collection<TypedDependency> tdl = gs.typedDependencies();
        Map<String, String> tdm = new HashMap<>(tdl.size());
        for (TypedDependency td : tdl) {
            tdm.put(td.dep().label().originalText(), td.reln().getShortName());
        }
        return tdm;
    }

    /* Database utilities */

    private static PreparedStatement create_sentences_table(Connection db_connection)
    throws SQLException {
        Statement s = db_connection.createStatement();
        try {
            s.executeUpdate(
                "CREATE TABLE IF NOT EXISTS sentences (" +
                "  image_id INTEGER," +
                "  description_id INTEGER," +
                "  sentence_idx INTEGER," +
                "  raw_sentence TEXT," +
                "  parse_tree TEXT," +
                "  PRIMARY KEY (image_id, description_id, sentence_idx)," +
                "  FOREIGN KEY (image_id, description_id)" +
                "    REFERENCES descriptions ON DELETE CASCADE" +
                ")");
            if (!db_connection.getAutoCommit()) { db_connection.commit(); }
        } finally { s.close(); }
        PreparedStatement insert_sent = db_connection.prepareStatement(
            "INSERT OR IGNORE INTO sentences VALUES (?, ?, ?, ?, ?)");
        return insert_sent;
    }

    private static PreparedStatement create_words_table(Connection db_connection)
    throws SQLException {
        Statement s = db_connection.createStatement();
        try {
            s.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS words (" +
                    "  image_id INTEGER," +
                    "  description_id INTEGER," +
                    "  sentence_idx INTEGER," +
                    "  word_idx INTEGER," +
                    "  is_punctuation INTEGER," +
                    "  is_function_word INTEGER," +
                    "  raw_word TEXT," +
                    "  lemma TEXT," +
                    "  part_of_speech TEXT," +
                    "  dependency TEXT," +
                    "  named_entity TEXT," +
                    "  PRIMARY KEY (image_id, description_id, sentence_idx, word_idx)," +
                    "  FOREIGN KEY (image_id, description_id, sentence_idx)" +
                    "    REFERENCES sentences ON DELETE CASCADE" +
                    ")");
            if (!db_connection.getAutoCommit()) { db_connection.commit(); }
        } finally { s.close(); }
        PreparedStatement insert_word = db_connection.prepareStatement(
            "INSERT OR IGNORE INTO words VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        return insert_word;
    }
}
