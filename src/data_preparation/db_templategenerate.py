import logging
import os
import sqlite3
import util.option_parser as option_parser


COL_DELIM = ','
MIN_FREQ = 500


def generate_templates(db_connection):
    logging.info('finding templates')
    templates = db_connection.execute(
        '  SELECT image_id, description_id, '
        '         GROUP_CONCAT(dependency, "' + COL_DELIM + '") '
        '           AS template '
        '  FROM ( '
        '    SELECT * '
        '    FROM words '
        '    WHERE is_punctuation = 0 '
        '    ORDER BY sentence_idx, word_idx) '
        '  GROUP BY image_id, description_id').fetchall()
    logging.info('adding templates to descriptions table')
    db_connection.execute('ALTER TABLE descriptions ADD template TEXT')
    for image_id, description_id, template in templates:
        db_connection.execute(
            'UPDATE descriptions '
            'SET template = ? '
            'WHERE image_id = ? AND '
            '      description_id = ?',
            (template, image_id, description_id))
    db_connection.commit()


def create_template_histogram(db_connection, min_freq):
    logging.info('building template frequency histogram')
    db_connection.execute(
        'CREATE VIEW IF NOT EXISTS template_histogram AS '
        '  SELECT template, frequency '
        '  FROM ( '
        '    SELECT template, COUNT(*) AS frequency '
        '    FROM descriptions '
        '    GROUP BY template) '
        '  WHERE frequency >= ' + str(min_freq) + ' '
        '  ORDER BY frequency DESC')
    db_connection.commit()


if __name__ == '__main__':
    # parse arguments
    parser = option_parser.OptionParser(
        epilog=('\nPools previous knowledge to analyze the descriptions in '
                'the sqlite database. '
                '\nMerges the dependencies of non-punctuation tokens in every '
                'description into a vector. This procedure removes '
                'description content, leaving behind description form. '
                '\n\n~~~~~Schema~~~~~'
                '\ndescriptions(*image_id, *desription_id, template)'
                '\n\n~~~~~Notes~~~~~'
                '\nThe `template` field in |descriptions| is a '
                'tab-separated string representing an array.'))
    parser.add_positional_argument('path/to/db', type='file')
    parser.add_option('--min-freq', metavar='M', type='int',
                      dest='min_freq', action='store', default=MIN_FREQ,
                      help='ignore templates with frequencies below M')
    opts, args = parser.parse_args()
    # unpack opts and args
    min_freq = opts.min_freq or MIN_FREQ
    db_path = os.path.abspath(args[0])

    # connect to db
    db_connection = sqlite3.connect(db_path)
    try:
        generate_templates(db_connection)
        create_template_histogram(db_connection, min_freq)
    except Exception as e:
        db_connection.rollback()
        raise e
    else:
        db_connection.commit()
    finally:
        db_connection.close()
