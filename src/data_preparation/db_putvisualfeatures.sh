[[ $# -lt 2 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

DISCRETE="discrete"
CONTINUOUS="continuous"

datadir="$1"
dbfile="$2"
vf="$datadir/VisualFeatures"

# categorize visual features
for f in $(ls "$vf"/*.txt | grep -v "_names"); do
    grep -q '\.' "$f" && lndir="$vf/$CONTINUOUS" || lndir="$vf/$DISCRETE"
    mkdir -p "$lndir"
    dir="$(readlink -e $(dirname $f))"
    name="$(basename $f '.txt')"
    ln -fs "$dir/${name}.txt" "$lndir"
    ln -fs "$dir/${name}_names.txt" "$lndir"
done
log "categorized features into $DISCRETE and $CONTINUOUS"

# put visual features into database
for feature_type in "$DISCRETE" "$CONTINUOUS"; do
    # collate names
    feature_names=""
    for names_file in $(ls "$vf/$feature_type"/*_names.txt | sort); do
        base_name="$(basename $names_file _names.txt)"
        base_name="${base_name#10K_}"
        sub_names="$(cut -f1 $names_file \
            | paste -sd',' \
            | sed s/,/,${base_name}@/g)"
        sub_names="${base_name}@${sub_names}"
        feature_names="${feature_names}${sub_names},"
    done
    visual_vector_names="${feature_names%','}"
    # collate data
    featuresfile="$(mktemp)"
    paste $(ls "$vf/$feature_type"/*.txt | grep -v "_names" | sort) \
    | tr '\t' ',' > "$featuresfile"
    log "aggregated $feature_type feature"
    # create table
    db_exec "
        CREATE TABLE IF NOT EXISTS visual_features_$feature_type (
          image_id INTEGER,
          visual_vector_names TEXT,
          visual_vector TEXT,
          PRIMARY KEY (image_id, visual_vector_names)
        )"
    # insert data
    image_id=0
    num_images=$(wc -l "$featuresfile" | cut -d' ' -f1)
    sqlfile="$(mktemp)"
    while read visual_vector; do
        echo "INSERT INTO visual_features_$feature_type
              VALUES (
                '$image_id',
                '$visual_vector_names',
                '$visual_vector'
              );" >>  "$sqlfile"
        if [[ $(($((image_id+1))%500)) -eq 0 ]]; then
            sqlite3 "$dbfile" < "$sqlfile"
            log "inserted <=$image_id/$num_images ($feature_type)"
            > "$sqlfile"
        fi
        image_id=$((image_id+1))
    done < "$featuresfile"
    sqlite3 "$dbfile" < "$sqlfile"
    log "inserted <=$image_id/$num_images ($feature_type)"
    rm -f "$sqlfile" "$featuresfile"
done
