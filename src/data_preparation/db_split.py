import collections
import logging
import os
import random
import sqlite3
import sys
import util.learning as learning
import util.option_parser as option_parser


FLT_EPS = sys.float_info.epsilon


def create_split(db_connection, split_ratios):
    # create new tables to hold subsets of data
    for table_name in split_ratios:
        db_connection.execute(
            'CREATE TABLE IF NOT EXISTS %s ('
            '  image_id INTEGER,'
            '  PRIMARY KEY (image_id),'
            '  FOREIGN KEY (image_id) REFERENCES images ON DELETE CASCADE'
            ')' % table_name)
        db_connection.execute('DELETE FROM %s' % table_name)
    # split data
    all_ids = collections.deque(db_connection.execute(
        'SELECT image_id FROM images'))
    random.shuffle(all_ids)
    total_ids = len(all_ids)
    for table_name, split_size in split_ratios.iteritems():
        num_ids = int(split_size * total_ids)
        split_ids = sorted((all_ids.pop()[0], ) for _ in xrange(num_ids))
        db_connection.executemany(
            'INSERT INTO %s VALUES (?)' % table_name, split_ids)
    rest_ids = [(all_ids.pop()[0], ) for _ in xrange(len(all_ids))]
    db_connection.executemany(
        'INSERT INTO %s VALUES (?)' % split_ratios.keys()[0], rest_ids)
    db_connection.commit()
    for table_name in split_ratios:
        row_count = db_connection.execute(
            'SELECT COUNT(*) FROM %s' % table_name).fetchone()[0]
        logging.info('Inserted %d rows into %s' % (row_count, table_name))


if __name__ == '__main__':
    # parse arguments
    parser = option_parser.OptionParser(
        epilog=('\nPartitions the sqlite database into training, testing, '
                'validation and development folds.'
                '\n\n~~~~~Schema~~~~~'
                '\ntrain_ids(*image_id)'
                '\ntest_ids(*image_id)'
                '\nvalid_ids(*image_id)'
                '\ndevo_ids(*image_id)'))
    parser.add_positional_argument('/path/to/db', type='file')
    parser.add_positional_argument('%train', type='float')
    parser.add_positional_argument('%test', type='float')
    parser.add_positional_argument('%valid', type='float', default=0)
    parser.add_positional_argument('%devo', type='float', default=0)
    opts, args = parser.parse_args()
    # unpack opts and args
    db_path = os.path.abspath(args[0])
    split_ratios = dict(('%s_set' % fold, arg)
                        for (fold, arg) in zip(learning.FOLDS, args[1:]))
    split_mass = sum(split_ratios.values())
    if split_mass < 1 - FLT_EPS or split_mass > 1 + FLT_EPS:
        sys.exit('Split ratios should add up to one (have %g)' % split_mass)

    # connect to db, split data and store new tables
    with sqlite3.connect(db_path) as db_connection:
        create_split(db_connection, split_ratios)
