import collections
import glob
import logging
import os
import sqlite3
import util.option_parser as option_parser


DATA_DELIM = '\t'
DATA_EXT = 'txt'
SENTENCES_DIR = 'Sentences'


def descriptions2db(data_dir, db_connection):
    db_connection.execute(
        'CREATE TABLE IF NOT EXISTS descriptions ('
        '  image_id INTEGER,'
        '  description_id INTEGER,'
        '  raw_text TEXT,'
        '  PRIMARY KEY (image_id, description_id),'
        '  FOREIGN KEY (image_id) REFERENCES images ON DELETE CASCADE'
        ')')
    paths = glob.glob(os.path.join(data_dir, SENTENCES_DIR, '*.' + DATA_EXT))
    logging.info('loading descriptions from %d paths' % len(paths))
    description_ids = collections.defaultdict(int)
    for i, path in enumerate(sorted(paths)):
        with open(path) as f:
            lines = [l.strip() for l in f]
        for line in lines:
            # the line "X Y ..." is the Yth description for the Xth image
            image_id, _, raw_text = line.split(DATA_DELIM)
            description_id = description_ids[image_id]
            description_ids[image_id] += 1
            db_connection.execute(
                'INSERT OR IGNORE INTO descriptions VALUES (?, ?, ?)',
                (image_id, description_id, raw_text))
    db_connection.commit()


if __name__ == '__main__':
    # parse arguments
    parser = option_parser.OptionParser(
        epilog=('\nPuts the descriptions from the AbstractScenes_v1 dataset '
                'into a sqlite database.'
                '\n\n~~~~~Schema~~~~~'
                '\ndescriptions(*image_id, *desription_id, raw_text)'))
    parser.add_positional_argument('path/to/abstract-scenes-v1', type='file')
    parser.add_positional_argument('path/to/db', type='string')
    parser.add_option('--data-ext', metavar='E', type='string',
                      dest='data_ext', action='store', default=DATA_EXT,
                      help='specify file extension of data files')
    opts, args = parser.parse_args()
    # unpack opts and args
    DATA_EXT = opts.data_ext
    data_dir = os.path.abspath(args[0])
    db_path = os.path.abspath(args[1])

    try:
        # connect to db
        db_connection = sqlite3.connect(db_path)
        # process data and store in db
        descriptions2db(data_dir, db_connection)
    finally:
        db_connection.close()
