import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;
import java.util.logging.Logger;
import util.DatabaseUtils;
import util.MapUtils;
import util.OptionParser;
import util.StringUtils;


public class db_lemmavectorize {
    private db_lemmavectorize() {}
    private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());

    private static final double REQ_FREQ_MASS = 0.95;
    private static final boolean CONSIDER_DEPENDENCY_TAG = false;

    public static void main(String[] args)
    throws Exception {
        // process arguments
        OptionParser parser = new DatabaseUtils.DatabaseScriptOptionParser(
            "Pools previous knowledge to analyze the descriptions in the " +
            "sqlite database. " +
            "\n" +
            "Merges the lemmas of non-punctuation/non-stopword tokens in " +
            "every description d into a vector V. V_i = k means that the ith " +
            "most common lemma occurred k times in description d. " +
            "\n" +
            "\n~~~~~Schema~~~~~" +
            "\ndescriptions(*image_id, *description_id, raw_text, " +
            "tokens_vector, tokens_vector_labels)" +
            "\n" +
            "\n~~~~~Notes~~~~~" +
            "\nThe `tokens_vector` and `tokens_vector_labels` fields in " +
            "|descriptions| are comma-separated strings representing arrays.");
        parser.add_option(
            null, "--req-freq-mass", "req_freq_mass", "double",
            "ignore lemmas after M% of the frequency mass are accounted for", "M", REQ_FREQ_MASS);
        parser.add_option(
            null, "--consider-dependency-tag", "consider_dependency_tag", "boolean",
            "use |lemma-dependency_tag| tokens instead of |lemma| tokens", null, CONSIDER_DEPENDENCY_TAG);
        parser.parse_args(args);
        Integer limit = parser.get_integer_option(DatabaseUtils.DatabaseScriptOptionParser.LIMIT_OPT);
        Integer offset = parser.get_integer_option(DatabaseUtils.DatabaseScriptOptionParser.OFFSET_OPT);
        Integer batch_size = parser.get_integer_option(DatabaseUtils.DatabaseScriptOptionParser.BATCH_SIZE_OPT);
        String path_to_db = parser.get_file_argument(DatabaseUtils.DatabaseScriptOptionParser.PATH_TO_DB_ARG).getAbsolutePath();
        Double req_freq_mass = parser.get_double_option("req_freq_mass");
        Boolean consider_dependency_tag = parser.get_boolean_option("consider_dependency_tag");

        // create a database connection
        Class.forName("org.sqlite.JDBC");
        Connection db_connection = null;
        try {
            db_connection = DriverManager.getConnection("jdbc:sqlite:" + path_to_db);

            db_connection.setAutoCommit(false);
            LOGGER.info("fetching lemma alphabet");
            LinkedHashMap<String, Integer> alphabet = to_idx_map(get_tokens(db_connection, req_freq_mass, consider_dependency_tag));
            String tokens_vector_labels = StringUtils.join(alphabet.keySet(), ',');
            int[] token_occurrences = new int[alphabet.size()];

            try {
                PreparedStatement ps = alter_descriptions_table(db_connection);
                Statement s = db_connection.createStatement();
                try {
                    ResultSet r = s.executeQuery(
                        "SELECT image_id, description_id, " +
                        "       GROUP_CONCAT(token) AS token_csv " +
                        "FROM ( " +
                        "  SELECT image_id, description_id, " +
                        "    lemma " + (consider_dependency_tag ? "|| '_' || dependency" : "") + " AS token " +
                        "  FROM words " +
                        "  WHERE is_punctuation = 0 AND " +
                        "        is_function_word = 0 " +
                        "  ORDER BY sentence_idx, word_idx) " +
                        "GROUP BY image_id, description_id" +
                        ((limit != null)  ? (" LIMIT "  + limit)  : "") +
                        ((offset != null) ? (" OFFSET " + offset) : ""));
                    try {
                        for (int row_num = 1; r.next(); row_num++) {
                            // unpack row
                            int image_id = r.getInt("image_id");
                            int description_id = r.getInt("description_id");
                            String[] token_csv = r.getString("token_csv").split(",");
                            // get number of occurrences of lemma in description
                            for (String l : token_csv) {
                                Integer idx = alphabet.get(l);
                                if (idx != null) { token_occurrences[idx]++; }
                            }
                            String tokens_vector = StringUtils.join(token_occurrences, ',');
                            // store lemma vector
                            ps.clearParameters();
                            ps.setString(1, tokens_vector);
                            ps.setString(2, tokens_vector_labels);
                            ps.setInt(3, image_id);
                            ps.setInt(4, description_id);
                            ps.executeUpdate();
                            // reset lemma occurrences array
                            for (String l : token_csv) {
                                Integer idx = alphabet.get(l);
                                if (idx != null) { token_occurrences[idx]--; }
                            }
                            // commit if necessary
                            if (row_num % batch_size == 0) {
                                db_connection.commit();
                                LOGGER.info("processed " + row_num);
                            }
                        }
                    } finally { r.close(); }
                } finally { s.close(); ps.close(); }
            } finally { db_connection.commit(); LOGGER.info("processed rest"); }
        } finally { if (db_connection != null) { db_connection.close(); } }
    }

    private static PreparedStatement alter_descriptions_table(Connection db_connection)
    throws SQLException {
        Statement s = db_connection.createStatement();
        try {
            s.executeUpdate("ALTER TABLE descriptions ADD tokens_vector TEXT");
            s.executeUpdate("ALTER TABLE descriptions ADD tokens_vector_labels TEXT");
            if (!db_connection.getAutoCommit()) { db_connection.commit(); }
        } catch (SQLException e) {
        } finally { s.close(); }
        return db_connection.prepareStatement(
            "UPDATE descriptions " +
            "SET tokens_vector = ?, " +
            "    tokens_vector_labels = ? " +
            "WHERE image_id = ? AND " +
            "      description_id = ?");
    }

    /** @return a Map M of `lemma` tokens sorted in descending
     *          frequency order.
     *          ie: `M.get("L,D") = i` means that lemma L occurred i times in D
     *          N.B.: Ignores tokens after |required_mass|% of the total
     *          frequency mass have been accounted for.
     */
    private static LinkedHashMap<String, Integer> get_tokens(Connection connection, double required_mass, boolean consider_dependency_tag)
    throws SQLException {
        Statement s = connection.createStatement();
        try {
            String query =
                "SELECT token, COUNT(*) AS frequency " +
                "FROM ( " +
                "  SELECT lemma " + (consider_dependency_tag ? "|| '_' || dependency" : "") + " AS token " +
                "  FROM words " +
                "  WHERE is_punctuation = 0 AND " +
                "        is_function_word = 0) " +
                "GROUP BY token " +
                "ORDER BY frequency DESC";
            int num_tokens = DatabaseUtils.get_size(connection, query);
            ResultSet r = s.executeQuery(query);
            try {
                Map<Integer, List<String>> frequencies = new LinkedHashMap<>(num_tokens);
                int frequency_total = 0;
                while (r.next()) {
                    String token = r.getString("token");
                    int frequency = r.getInt("frequency");
                    MapUtils.append(frequencies, frequency, token);
                    frequency_total += frequency;
                }
                LinkedHashMap<String, Integer> tokens = new LinkedHashMap<>();
                int frequency_cumulative = 0;
                int frequency_cutoff = 0;
                for (Map.Entry<Integer, List<String>> entry : frequencies.entrySet()) {
                    frequency_cutoff = entry.getKey();
                    List<String> words = entry.getValue();
                    for (String word : words) { tokens.put(word, frequency_cutoff); }
                    frequency_cumulative += (frequency_cutoff * words.size());
                    if (frequency_cumulative >= frequency_total * required_mass) { break; }
                }
                LOGGER.info("frequency cutoff: " + frequency_cutoff + " occurrences");
                LOGGER.info("selected " + tokens.size() + " tokens");
                return tokens;
            } finally { r.close(); }
        } finally { s.close(); }
    }

    private static <K, V> LinkedHashMap<K, Integer> to_idx_map(LinkedHashMap<K, V> map) {
        LinkedHashMap<K, Integer> idx_map = new LinkedHashMap<>(map.size());
        int i = 0;
        for (Map.Entry<K, V> entry : map.entrySet()) {
            idx_map.put(entry.getKey(), i++);
        }
        return idx_map;
    }
}
