#!/usr/bin/env python


from util.constants import COL_DELIM, VECT_DELIM
import collections
import logging
import numpy
import random
import scipy.spatial
import sqlite3
import util.learning as learning
import util.option_parser as option_parser
import util.paths as paths
import util.stats as stats
import util.utils as utils


def get_data(db_path, fold):
    with sqlite3.connect(db_path) as db_connection:
        unpack = lambda (a, b): (int(a), numpy.fromstring(b, sep=VECT_DELIM))
        data = [unpack(row) for row in db_connection.execute(
            'SELECT image_id, visual_vector '
            'FROM %s_set '
            'NATURAL JOIN visual_features_discrete' % fold)]
    return data


def get_descriptions(db_path, fold=learning.FOLDS.TRAINING):
    d = collections.defaultdict(list)
    with sqlite3.connect(db_path) as db_connection:
        unpack = lambda (a, b, c): (int(a), int(b), c)
        data = (unpack(row) for row in db_connection.execute(
            'SELECT image_id, description_id, GROUP_CONCAT(raw_word, " ") '
            'FROM ( '
            '  SELECT image_id, description_id, raw_word '
            '  FROM %s_set '
            '  NATURAL JOIN words '
            '  ORDER BY sentence_idx, word_idx) '
            'GROUP BY image_id, description_id' % fold))
        for image_id, _, description in data:
            d[image_id].append(description)
    logging.info('retrieved descriptions for %d images' % len(d))
    return d


def build_index(db_path, fold=learning.FOLDS.TRAINING):
    d = collections.defaultdict(list)
    for image_id, vect in get_data(db_path, fold=fold):
        num_set = vect.sum()
        d[num_set].append((image_id, vect))
    lens = map(len, d.itervalues())
    logging.info('built latent semantic hash')
    logging.info('maximum bucket size: %d' % max(lens))
    logging.info('minimum bucket size: %d' % min(lens))
    logging.info('average bucket size: %.2f' % stats.mean(lens))
    return d


def retrieve(index, descriptions, fold=learning.FOLDS.VALIDATION,
             no_hash=False, distfun=scipy.spatial.distance.hamming):
    def choose(image_id):
        return random.choice(descriptions[image_id])

    def keygen(v1):
        return lambda (_, v2): distfun(v1, v2)

    def candidates(num_set):
        if no_hash:
            return utils.flatten(index.itervalues())
        return index[num_set]

    def get(num_set):
        best_image_id, _ = min(candidates(num_set), key=keygen(vect))
        return best_image_id

    data = get_data(db_path, fold=fold)
    for i, (image_id, vect) in enumerate(data):
        num_set = vect.sum()
        try:
            best_image_id = get(num_set)
        except ValueError:
            logging.warning('no similar images for %d' % image_id)
            nset_up = nset_down = num_set
            while True:
                nset_down -= 1
                try:
                    get(nset_down)
                    break
                except KeyError:
                    # bottomed out
                    break
                except ValueError:
                    pass
            while True:
                nset_up += 1
                if (nset_up - num_set) > (num_set - nset_down):
                    # early-out
                    break
                try:
                    get(nset_up)
                    break
                except KeyError:
                    # maxed out
                    break
                except ValueError:
                    pass
            if num_set - nset_down < nset_up - num_set:
                best_image_id = get(nset_down)
                logging.warning('found replacement at delta -%d' % nset_down)
            else:
                best_image_id = get(nset_up)
                logging.warning('found replacement at delta +%d' % nset_up)
        else:
            logging.info('described %d/%d' % (i + 1, len(data)))
        yield image_id, choose(best_image_id)


if __name__ == '__main__':
    DIST = collections.namedtuple('_D', 'HAMMING COSINE')('hamming', 'cosine')
    parser = option_parser.OptionParser(
        epilog=('Baseline image description model. Given an unseen image, '
                'find the most similar image seen at training time and return '
                'a random human generated description of that image'))
    parser.add_positional_argument('/path/to/db', type='file')
    parser.add_option('--no-hash', dest='no_hash', action='store_true',
                      default=False,
                      help=('do full 1NN search instead of local 1NN search '
                            'in a semantic hashing neighborhood'))
    parser.add_option('--random-seed', dest='seed', metavar='S', default=None,
                      help='use S to seed the random number generator')
    parser.add_option('--distance-metric', dest='dist', default=DIST.COSINE,
                      choices=list(DIST),
                      help='set the distance metric of the 1NN search')
    parser.add_option('--training-fold', choices=list(learning.FOLDS),
                      dest='training_fold', default=learning.FOLDS.DEVELOPMENT,
                      help='set subset of data to use for training')
    parser.add_option('--evaluation-fold', choices=list(learning.FOLDS),
                      dest='evaluation_fold',
                      default=learning.FOLDS.DEVELOPMENT,
                      help='set subset of data to use for evaluation')
    opts, args = parser.parse_args()
    db_path = paths.normalize(args[0])
    no_hash = opts.no_hash
    seed = opts.seed
    dist = opts.dist
    training_fold = opts.training_fold
    evaluation_fold = opts.evaluation_fold

    if seed is not None:
        random.seed(seed)
    distfun = {DIST.HAMMING: scipy.spatial.distance.hamming,
               DIST.COSINE:  scipy.spatial.distance.cosine, }[opts.dist]

    index = build_index(db_path, fold=training_fold)
    descriptions = get_descriptions(db_path, fold=training_fold)
    retrieved = retrieve(index, descriptions, no_hash=no_hash, distfun=distfun,
                         fold=evaluation_fold)

    print ('# Model:picture_retrieval_baseline('
           'distance-metric={dist}, semhash={dohash})'
           .format(dist=dist, dohash=not no_hash))
    print '# Image Id%s# Predicted Description' % COL_DELIM
    for image_id, description in retrieved:
        print '%s%s%s' % (image_id, COL_DELIM, description)
