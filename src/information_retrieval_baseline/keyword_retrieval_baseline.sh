#!/bin/bash
# Baseline image description generation model. Given an unseen image, generate
# some keywords using the template prediction model. Then. use these keywords as
# a search query against all human generated descriptions seen at training time
# and return the best match.

[[ $# -lt 1 ]] && echo "not enough arguments to $0" 1>&2 && exit 1

scriptdir="$(dirname $(readlink -f $0))"
tldir="$(readlink -f $(git rev-parse --show-toplevel))"
source "$tldir/environs.sh"

set -o nounset
set -o errexit

# script start

training_fold="$1"
evaluation_fold="$2"
outfile="$(readlink -f $3)"
srcdir="$tldir/src"
datadir="$tldir/data"
bindir="$tldir/bin/classifiers"
docsdir="$tldir/bin/search_engine/docs"
indexdir="$tldir/bin/search_engine/index"
keywords="$tldir/bin/search_engine/query-keywords"

if [[ ! -d "$docsdir" ]]; then
    mkdir -p "$docsdir"
    corpus="$(mktemp)"
    db_exec "SELECT GROUP_CONCAT(raw_word, ' ')
             FROM (
               SELECT raw_word, image_id, description_id
               FROM ${training_fold}_set
               NATURAL JOIN words
               ORDER BY sentence_idx, word_idx)
             GROUP BY image_id, description_id" > "$corpus"
    log "retrieved descriptions from database"
    split --lines=1 --suffix-length=10 "$corpus" "$docsdir/"
    log "created search file system"
    rm -f "$corpus"
else
    log "found search file system at '$docsdir'"
fi
if [[ ! -d "$indexdir" ]]; then
    mkdir -p "$indexdir"
    java org.apache.lucene.demo.IndexFiles -docs "$docsdir" -index "$indexdir"
    log "created search index"
else
    log "found search index at '$indexdir'"
fi
if [[ ! -f "$keywords" ]]; then
    kw_predict="$srcdir/template_baseline/template_baseline.py"
    kw_model="$bindir/template_baseline-logistic_regression-raw_words.model.gz"
    dbpath="$datadir/sqlite3.db"

    python "$kw_predict" -v \
        'predict' "$dbpath" "$kw_model" \
        --template-override='all-template' \
        --n-candidate-templates=1 \
        --n-candidate-words=1 \
        --fold="$evaluation_fold" \
        --outpath="$keywords"
    log "generated keywords"
else
    log "found keywords at '$keywords'"
fi


> "$outfile"
echo "# Model:keyword_retrieval_baseline()" >> "$outfile"
echo "# Image Id	# Predicted Description" >> "$outfile"

image_ids="$(mktemp)"
queries="$(mktemp)"
retrieved="$(mktemp)"

grep -v '^#' "$keywords" | cut -f1 > "$image_ids"
grep -v '^#' "$keywords" | cut -f2 > "$queries"

java org.apache.lucene.demo.SearchFiles \
    -index "$indexdir" -queries "$queries" \
| grep '^1\.' | cut -d' ' -f2 | xargs cat > "$retrieved"
log "resolved queries"

paste "$image_ids" "$retrieved" >> "$outfile"
rm -f "$image_ids" "$queries" "$retrieved"
