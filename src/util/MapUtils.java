package util;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MapUtils {
    private MapUtils() {}

    public static <K, V> void append(Map<K, List<V>> map, K key, V value) {
        List<V> values = map.get(key);
        if (values == null) {
            values = new ArrayList<>();
            map.put(key, values);
        }
        values.add(value);
    }
}
