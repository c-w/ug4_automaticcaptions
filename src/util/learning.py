import collections
import heapq
import numpy
import sklearn.ensemble
import sklearn.linear_model
import sklearn.naive_bayes
import sklearn.svm
import sklearn.tree
import utils


_mode_names = 'TRAIN TUNE PREDICT'
_mode_labels = 'train tune predict'
MODES = collections.namedtuple('MODES', _mode_names)(*_mode_labels.split())

_fold_names = 'FULL_TRAINING TRAINING EVALUATION VALIDATION DEVELOPMENT'
_fold_labels = 'full_training training evaluation validation development'
FOLDS = collections.namedtuple('FOLDS', _fold_names)(*_fold_labels.split())

_model_names = (
    'DECISION_TREE '
    'GAUSSIAN_NAIVE_BAYES '
    'LINEAR_REGRESSION '
    'LOGISTIC_REGRESSION '
    'MULTINOMIAL_NAIVE_BAYES '
    'RANDOM_FOREST '
    'RIDGE_REGRESSION '
    'SUPPORT_VECTOR_MACHINE '
)
_model_labels = (
    'decision-tree '
    'gaussian-naive-bayes '
    'linear-regression '
    'logistic-regression '
    'multinomial-naive-bayes '
    'random-forest '
    'ridge-regression '
    'svm-rbf'
)
MODELS = collections.namedtuple('MODELS', _model_names)(*_model_labels.split())
CLASSIFIERS = {
    MODELS.DECISION_TREE: sklearn.tree.DecisionTreeClassifier,
    MODELS.GAUSSIAN_NAIVE_BAYES: sklearn.naive_bayes.GaussianNB,
    MODELS.LINEAR_REGRESSION: sklearn.linear_model.LinearRegression,
    MODELS.LOGISTIC_REGRESSION: sklearn.linear_model.LogisticRegression,
    MODELS.MULTINOMIAL_NAIVE_BAYES: sklearn.naive_bayes.MultinomialNB,
    MODELS.RANDOM_FOREST: sklearn.ensemble.RandomForestClassifier,
    MODELS.RIDGE_REGRESSION: sklearn.linear_model.RidgeCV,
    MODELS.SUPPORT_VECTOR_MACHINE: sklearn.svm.SVC,
}
MULTI_MODELS = (MODELS.LINEAR_REGRESSION, MODELS.RIDGE_REGRESSION)


class Model(object):
    def __init__(self, model_label, metaparams=None):
        self._model_label = model_label
        self.is_multimodel = model_label in MULTI_MODELS
        self._metaparams = metaparams if metaparams is not None else {}

    def __repr__(self):
        model_name = self.__class__.__name__
        base_classifier = self._model_label
        return '%s(base_classifier=%s)' % (model_name, base_classifier)

    def _new_base_classifier(self):
        return CLASSIFIERS[self._model_label](**self._metaparams)

    def _labels_are_trivial(self, labels):
        try:
            is_trivial = len(set(map(tuple, labels))) == 1
        except:
            is_trivial = len(set(labels)) == 1
        return is_trivial

    def fit(self, features, labels):
        if self._labels_are_trivial(labels):
            self._trivial_prediction = labels[0]
            self.classes = [labels[0]]
            return self
        self._trivial_prediction = None
        self._model = self._new_base_classifier()
        self._model.fit(features, labels)
        try:
            self.classes = self._model.classes_
        except AttributeError:
            self.classes = []
        return self

    def predict_log_proba(self, features):
        if self._trivial_prediction is not None:
            return [[1.0]] * len(features)
        return self._model.predict_log_proba(features)

    def predict(self, features):
        if self._trivial_prediction is not None:
            return [self._trivial_prediction] * len(features)
        return self._model.predict(features)


class ConstantModel(Model):
    def __init__(self, prediction):
        self._prediction = prediction

    def __repr__(self):
        return 'ConstantModel(%s)' % self._prediction

    def fit(self, features, labels):
        self._trivial_prediction = self._prediction
        self.classes = [self._prediction]
        return self


class TranslatingModel(Model):
    def fit(self, features, raw_labels):
        translation_table = utils.indexed_dict(raw_labels)
        labels = numpy.array([translation_table[lbl] for lbl in raw_labels])
        Model.fit(self, features, labels)
        self._translation_table = translation_table

    def predict_log_proba(self, features, nbest=False):
        translation_table = utils.reversedict(self._translation_table)
        translated_labels = [translation_table[c] for c in self.classes]
        raw_predictions = Model.predict_log_proba(self, features)
        predictions = [zip(confidences, translated_labels)
                       for confidences in raw_predictions]
        if nbest:
            key = lambda (conf, pred): conf
            if nbest > 0:
                nlargest = lambda p: heapq.nlargest(nbest, p, key=key)
                return map(nlargest, predictions)
            else:
                return sorted(predictions, key=key)
        else:
            return predictions

    def predict(self, features):
        raw_predictions = Model.predict(self, features)
        translation_table = utils.reversedict(self._translation_table)
        predictions = numpy.array([translation_table[lbl]
                                   for lbl in raw_predictions])
        return predictions
