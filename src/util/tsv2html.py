#!/usr/bin/python2.7


import collections
import base64
import itertools
import os
import sqlite3
import util.utils as utils


DB_PATH = os.path.join(utils.tldir(), 'data', 'sqlite3.db')
CSS = (
    'table, td {'
    '  border: 1px solid gray;'
    '  text-align: center;'
    '  padding: 5px;'
    '}'
    'table.meta-info {'
    '  font-weight: bold;'
    '}'
    'p.meta-info {'
    '  font-size: larger;'
    '}'
)


class DataContainer(object):
    def __init__(self, lines, **kwargs):
        self._process_kwargs(kwargs)
        self._process_lines(lines)

    def _process_lines(self, lines):
        data = []
        meta_info = collections.OrderedDict()
        for line in lines:
            line = self._strip(line)
            if line.startswith(self._comment):
                line = line.lstrip(self._comment)
                try:
                    key, value = line.split(self._metakv)
                    meta_info[key.strip().lower()] = value.strip()
                except ValueError:
                    continue
            else:
                data.append(line.split(self._sep))
        self.meta_info = meta_info
        self.data = data

    def _strip(self, line):
        if not line.startswith(self._sep):
            line = line.lstrip()
        if not line.endswith(self._sep):
            line = line.rstrip()
        return line

    def _process_kwargs(self, kwargs):
        self._metakv = kwargs.get('metakv', ':')
        self._comment = kwargs.get('comment', '#')
        self._sep = kwargs.get('sep', '\t')

    def __iter__(self):
        return iter(self.data)


def image_path(image_id, db_path=DB_PATH):
    try:
        return image_path._d[image_id]
    except AttributeError:
        with sqlite3.connect(db_path) as db_connection:
            d = dict(db_connection.execute(
                'SELECT image_id, image_path '
                'FROM images'))
        image_path._d = d
        return d[image_id]


def true_description(image_id, db_path=DB_PATH):
    try:
        return true_description._d[image_id]
    except AttributeError:
        with sqlite3.connect(db_path) as db_connection:
            d = dict((image_id, descriptions.lower().split('\t'))
                     for (image_id, descriptions) in db_connection.execute(
                         'SELECT image_id, GROUP_CONCAT(raw_text, "\t") '
                         'FROM descriptions '
                         'GROUP BY image_id'))
        true_description._d = d
        return d[image_id]


def html_true_description(image_id):
    return '<br>'.join(true_description(image_id))


def html_image(image_id, inline=True):
    src = image_path(image_id)
    alt, ext = os.path.splitext(os.path.basename(src))
    if inline:
        with open(src, 'rb') as f:
            src = 'data:image/{ext};base64,{b64}'.format(
                ext=ext[1:], b64=base64.b64encode(f.read()))
    return '<img src="%s" alt="%s" />' % (src, alt)


def unpack(line):
    image_id = int(line[0])
    predictions = line[1:] if len(line) >= 2 else []
    return image_id, predictions


def html_predictions(data):
    aggr = collections.defaultdict(list)
    for image_id, predictions in itertools.imap(unpack, data):
        aggr[image_id].extend(predictions)
    for image_id, predictions in sorted(aggr.iteritems()):
        yield (image_id, '<br>'.join(predictions))


def print_html(data):
    # header
    print '<html>'
    print '<head>'
    print '<title>%s</title>' % data.meta_info.get('model', '')
    print '<style>%s</style>' % CSS
    print '</head>'
    print '<body>'

    # prediction meta information
    if data.meta_info:
        print '<h1>Meta-Info</h1>'
        for meta_k, meta_v in data.meta_info.iteritems():
            print '<p class="meta-info">%s: %s</p>' % (meta_k, meta_v)
        print '<h1>Predictions</h1>'

    # predictions
    print '<table>'
    print '  <tr>'
    print '    <td><div class="meta-info">ImageId</div></td>'
    print '    <td><div class="meta-info">Image</div></td>'
    print '    <td><div class="meta-info">Predicted Description</div></td>'
    print '    <td><div class="meta-info">True Descriptions</div></td>'
    print '  </tr>'
    for image_id, predictions in html_predictions(data):
        print '  <tr>'
        print '    <td>%d</td>' % image_id
        print '    <td>%s</td>' % html_image(image_id)
        print '    <td>%s</td>' % predictions
        print '    <td>%s</td>' % html_true_description(image_id)
        print '  </tr>'
    print '</table>'

    # footer
    print '</body>'
    print '</html>'


if __name__ == '__main__':
    import fileinput
    data = DataContainer(fileinput.input())
    print_html(data)
