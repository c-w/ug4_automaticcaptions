import abc
import ast
import itertools
import logging
import optparse
import os
import sys


# Exceptions


class ArgumentValidationException(Exception):
    pass


class InsufficientArgumentsException(ArgumentValidationException):
    def __init__(self, need, have):
        self.message = ('Need to provide at least %d arguments (got %d)'
                        % (need, have))


class InvalidArgumentException(ArgumentValidationException):
    def __init__(self, arg, cause):
        self.message = 'Invalid argument "%s": %s' % (arg, cause)


# Argument validators


class PositionalArgument(object):
    __metaclass__ = abc.ABCMeta
    __subclass_dict__ = {}

    def __init__(self, **kwargs):
        self.name = kwargs.get('name', 'arg')
        self.default = kwargs.get('default', None)

    @abc.abstractmethod
    def validate(self, arg):
        pass

    @staticmethod
    def create(name, type, **kwargs):
        try:
            arg_t = PositionalArgument.__subclass_dict__[type.lower()]
        except KeyError:
            raise ValueError('unsupported positional argument type %s' % type)
        kwargs['name'] = name
        return arg_t(**kwargs)


class ChoiceArgument(PositionalArgument):
    def __init__(self, **kwargs):
        PositionalArgument.__init__(self, **kwargs)
        self.choices = kwargs.get('choices', [])

    def validate(self, arg=None):
        arg = arg if arg is not None else self.default
        if arg in self.choices:
            return arg
        else:
            msg = 'should be one of [%s]' % ', '.join(map(str, self.choices))
            raise InvalidArgumentException(arg, msg)
PositionalArgument.__subclass_dict__['choice'] = ChoiceArgument


class IntegerArgument(PositionalArgument):
    def validate(self, arg=None):
        arg = arg if arg is not None else self.default
        try:
            return int(arg)
        except ValueError:
            raise InvalidArgumentException(arg, 'integer required')
PositionalArgument.__subclass_dict__['int'] = IntegerArgument


class FloatArgument(PositionalArgument):
    def validate(self, arg=None):
        arg = arg if arg is not None else self.default
        try:
            return float(arg)
        except ValueError:
            raise InvalidArgumentException(arg, 'float required')
PositionalArgument.__subclass_dict__['float'] = FloatArgument


class FileArgument(PositionalArgument):
    def validate(self, arg=None):
        arg = arg if arg is not None else self.default
        if os.path.exists(os.path.abspath(arg)):
            return arg
        else:
            raise InvalidArgumentException(arg, 'file does not exist')
PositionalArgument.__subclass_dict__['file'] = FileArgument


class StringArgument(PositionalArgument):
    def validate(self, arg=None):
        arg = arg if arg is not None else self.default
        return str(arg)
PositionalArgument.__subclass_dict__['string'] = StringArgument


# default options


class VerbosityOption(optparse.Option):
    def __init__(self):
        optparse.Option.__init__(
            self, '-v', '--verbose', default=0, dest='verbose', action='count',
            help='log more details')

    def __call__(self, verbosity):
        logging.basicConfig(
            format='[%(asctime)s:%(name)s:%(levelname)s] %(message)s',
            level=max(logging.WARNING - 10 * verbosity, logging.DEBUG))


# option parser with validation and default options

class OptionParser(optparse.OptionParser):
    def __init__(self, *args, **kwargs):
        optparse.OptionParser.__init__(self, *args, **kwargs)
        self._add_standard_options()
        self.positional_args = []

    def _add_standard_options(self):
        # enable verbosity flag by default
        self.add_option('-v', '--verbose', default=0,
                        dest='verbose', action='count',
                        help='log more details')

    def _process_standard_options(self, opts):
        # set up logging intensity
        logging.basicConfig(
            format='[%(asctime)s:%(name)s:%(levelname)s] %(message)s',
            level=max(logging.WARNING - 10 * opts.verbose, logging.DEBUG))

    def add_positional_argument(self, name=None, type='string', **kwargs):
        name = name if name else 'arg%d' % len(self.positional_args)
        arg = PositionalArgument.create(name, type, **kwargs)
        self.positional_args.append(arg)

    def format_epilog(self, formatter):
        return self.epilog + '\n'

    def get_usage(self):
        if self.usage == '%prog [options]':
            args = ' '.join('(%s=%s)' % (arg.name, arg.default)
                            if arg.default is not None else '%s' % arg.name
                            for arg in self.positional_args)
            self.usage += ' %s' % args
        return optparse.OptionParser.get_usage(self)

    def parse_args(self, *args, **kwargs):
        opts, argv = optparse.OptionParser.parse_args(self, *args, **kwargs)
        try:
            argv = list(self._validate_args(argv))
        except ArgumentValidationException as e:
            sys.exit('%s\n%s' % (e.message, self.get_usage()))
        self._process_standard_options(opts)
        return opts, argv

    def _validate_args(self, argv):
        # make sure we have all the required arguments
        argc = len(argv)
        n_positional = sum(1 if arg.default is None else 0
                           for arg in self.positional_args)
        if argc < n_positional:
            raise InsufficientArgumentsException(n_positional, argc)
        # validate types of required arguments
        # non-required and default arguments just pass through
        it = itertools.izip_longest(self.positional_args, argv)
        for positional, arg in it:
            if positional is None:
                yield arg
                continue
            if arg is None:
                arg = positional.default
            yield positional.validate(arg)


# utility functions

def parse_kwargs(s, default=None, kvdelim='=', tokdelim=','):
    if s is None:
        return default
    if s.startswith('{') and s.endswith('}'):
        s = s[1:-1]
    kwargs = {}
    for k, v in (kv.split(kvdelim) for kv in s.split(tokdelim)):
        try:
            v = ast.literal_eval(v)
        except ValueError:
            v = str(v)
        kwargs[k] = v
    return kwargs
