package util;


public class Utils {
    private Utils() {}

    public static int bool2int(boolean b) { return b ? 1 : 0; }

    public static String get_main_class() {
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        StackTraceElement main = stack[stack.length - 1];
        return main.getClassName();
    }
}
