package util;


import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.HashSet;
import java.util.Set;


public class LingUtils {
    private LingUtils() {}

    private static Set<String> FUNCTION_WORDS = null;  // lazily initialized
    private static Set<String> DICTIONARY = null;  // lazily initialized
    private static Set<String> PUNCTUATION = null;  // lazily initialized

    public static boolean is_punctuation(String word) {
        return get_punctuation().contains(word.toLowerCase());
    }

    public static boolean is_function_word(String word) {
        try {
            String num = word.replace("%", "");
            Double.parseDouble(num);
            return true;
        } catch (NumberFormatException e) {}
        return get_function_words().contains(word.toLowerCase());
    }

    public static boolean is_misspelt(String word) {
        return get_dictionary_words().contains(word.toLowerCase());
    }

    private static Set<String> get_function_words() {
        if (FUNCTION_WORDS != null) { return FUNCTION_WORDS; }

        Set<String> FUNCTION_WORDS = new HashSet<>((int)(320 / 0.75) + 1);
        FUNCTION_WORDS.add("a"); FUNCTION_WORDS.add("about");
        FUNCTION_WORDS.add("above"); FUNCTION_WORDS.add("after");
        FUNCTION_WORDS.add("again"); FUNCTION_WORDS.add("ago");
        FUNCTION_WORDS.add("all"); FUNCTION_WORDS.add("almost");
        FUNCTION_WORDS.add("along"); FUNCTION_WORDS.add("already");
        FUNCTION_WORDS.add("also"); FUNCTION_WORDS.add("although");
        FUNCTION_WORDS.add("always"); FUNCTION_WORDS.add("am");
        FUNCTION_WORDS.add("among"); FUNCTION_WORDS.add("an");
        FUNCTION_WORDS.add("and"); FUNCTION_WORDS.add("another");
        FUNCTION_WORDS.add("any"); FUNCTION_WORDS.add("anybody");
        FUNCTION_WORDS.add("anything"); FUNCTION_WORDS.add("anywhere");
        FUNCTION_WORDS.add("are"); FUNCTION_WORDS.add("aren't");
        FUNCTION_WORDS.add("around"); FUNCTION_WORDS.add("as");
        FUNCTION_WORDS.add("at"); FUNCTION_WORDS.add("back");
        FUNCTION_WORDS.add("be"); FUNCTION_WORDS.add("been");
        FUNCTION_WORDS.add("before"); FUNCTION_WORDS.add("being");
        FUNCTION_WORDS.add("below"); FUNCTION_WORDS.add("beneath");
        FUNCTION_WORDS.add("beside"); FUNCTION_WORDS.add("between");
        FUNCTION_WORDS.add("beyond"); FUNCTION_WORDS.add("billion");
        FUNCTION_WORDS.add("billionth"); FUNCTION_WORDS.add("both");
        FUNCTION_WORDS.add("but"); FUNCTION_WORDS.add("by");
        FUNCTION_WORDS.add("can"); FUNCTION_WORDS.add("can't");
        FUNCTION_WORDS.add("could"); FUNCTION_WORDS.add("couldn't");
        FUNCTION_WORDS.add("did"); FUNCTION_WORDS.add("didn't");
        FUNCTION_WORDS.add("do"); FUNCTION_WORDS.add("does");
        FUNCTION_WORDS.add("doesn't"); FUNCTION_WORDS.add("doing");
        FUNCTION_WORDS.add("don't"); FUNCTION_WORDS.add("done");
        FUNCTION_WORDS.add("down"); FUNCTION_WORDS.add("during");
        FUNCTION_WORDS.add("each"); FUNCTION_WORDS.add("eight");
        FUNCTION_WORDS.add("eighteen"); FUNCTION_WORDS.add("eighteenth");
        FUNCTION_WORDS.add("eighth"); FUNCTION_WORDS.add("eightieth");
        FUNCTION_WORDS.add("eighty"); FUNCTION_WORDS.add("either");
        FUNCTION_WORDS.add("eleven"); FUNCTION_WORDS.add("eleventh");
        FUNCTION_WORDS.add("else"); FUNCTION_WORDS.add("enough");
        FUNCTION_WORDS.add("even"); FUNCTION_WORDS.add("ever");
        FUNCTION_WORDS.add("every"); FUNCTION_WORDS.add("everybody");
        FUNCTION_WORDS.add("everyone"); FUNCTION_WORDS.add("everything");
        FUNCTION_WORDS.add("everywhere"); FUNCTION_WORDS.add("except");
        FUNCTION_WORDS.add("far"); FUNCTION_WORDS.add("few");
        FUNCTION_WORDS.add("fewer"); FUNCTION_WORDS.add("fifteen");
        FUNCTION_WORDS.add("fifteenth"); FUNCTION_WORDS.add("fifth");
        FUNCTION_WORDS.add("fiftieth"); FUNCTION_WORDS.add("fifty");
        FUNCTION_WORDS.add("first"); FUNCTION_WORDS.add("five");
        FUNCTION_WORDS.add("for"); FUNCTION_WORDS.add("fortieth");
        FUNCTION_WORDS.add("forty"); FUNCTION_WORDS.add("four");
        FUNCTION_WORDS.add("fourteen"); FUNCTION_WORDS.add("fourteenth");
        FUNCTION_WORDS.add("fourth"); FUNCTION_WORDS.add("from");
        FUNCTION_WORDS.add("get"); FUNCTION_WORDS.add("gets");
        FUNCTION_WORDS.add("getting"); FUNCTION_WORDS.add("got");
        FUNCTION_WORDS.add("had"); FUNCTION_WORDS.add("hadn't");
        FUNCTION_WORDS.add("has"); FUNCTION_WORDS.add("hasn't");
        FUNCTION_WORDS.add("have"); FUNCTION_WORDS.add("haven't");
        FUNCTION_WORDS.add("having"); FUNCTION_WORDS.add("he");
        FUNCTION_WORDS.add("he'd"); FUNCTION_WORDS.add("he'll");
        FUNCTION_WORDS.add("he's"); FUNCTION_WORDS.add("hence");
        FUNCTION_WORDS.add("her"); FUNCTION_WORDS.add("here");
        FUNCTION_WORDS.add("hers"); FUNCTION_WORDS.add("herself");
        FUNCTION_WORDS.add("him"); FUNCTION_WORDS.add("himself");
        FUNCTION_WORDS.add("his"); FUNCTION_WORDS.add("hither");
        FUNCTION_WORDS.add("how"); FUNCTION_WORDS.add("however");
        FUNCTION_WORDS.add("hundred"); FUNCTION_WORDS.add("hundredth");
        FUNCTION_WORDS.add("i"); FUNCTION_WORDS.add("i'd");
        FUNCTION_WORDS.add("i'll"); FUNCTION_WORDS.add("i'm");
        FUNCTION_WORDS.add("i've"); FUNCTION_WORDS.add("if");
        FUNCTION_WORDS.add("in"); FUNCTION_WORDS.add("into");
        FUNCTION_WORDS.add("is"); FUNCTION_WORDS.add("isn't");
        FUNCTION_WORDS.add("it"); FUNCTION_WORDS.add("it's");
        FUNCTION_WORDS.add("its"); FUNCTION_WORDS.add("itself");
        FUNCTION_WORDS.add("last"); FUNCTION_WORDS.add("less");
        FUNCTION_WORDS.add("many"); FUNCTION_WORDS.add("may");
        FUNCTION_WORDS.add("me"); FUNCTION_WORDS.add("might");
        FUNCTION_WORDS.add("million"); FUNCTION_WORDS.add("millionth");
        FUNCTION_WORDS.add("mine"); FUNCTION_WORDS.add("more");
        FUNCTION_WORDS.add("most"); FUNCTION_WORDS.add("much");
        FUNCTION_WORDS.add("must"); FUNCTION_WORDS.add("mustn't");
        FUNCTION_WORDS.add("my"); FUNCTION_WORDS.add("myself");
        FUNCTION_WORDS.add("near"); FUNCTION_WORDS.add("nearby");
        FUNCTION_WORDS.add("nearly"); FUNCTION_WORDS.add("neither");
        FUNCTION_WORDS.add("never"); FUNCTION_WORDS.add("next");
        FUNCTION_WORDS.add("nine"); FUNCTION_WORDS.add("nineteen");
        FUNCTION_WORDS.add("nineteenth"); FUNCTION_WORDS.add("ninetieth");
        FUNCTION_WORDS.add("ninety"); FUNCTION_WORDS.add("ninth");
        FUNCTION_WORDS.add("no"); FUNCTION_WORDS.add("nobody");
        FUNCTION_WORDS.add("none"); FUNCTION_WORDS.add("noone");
        FUNCTION_WORDS.add("nor"); FUNCTION_WORDS.add("not");
        FUNCTION_WORDS.add("nothing"); FUNCTION_WORDS.add("now");
        FUNCTION_WORDS.add("nowhere"); FUNCTION_WORDS.add("of");
        FUNCTION_WORDS.add("off"); FUNCTION_WORDS.add("often");
        FUNCTION_WORDS.add("on"); FUNCTION_WORDS.add("once");
        FUNCTION_WORDS.add("one"); FUNCTION_WORDS.add("only");
        FUNCTION_WORDS.add("or"); FUNCTION_WORDS.add("other");
        FUNCTION_WORDS.add("others"); FUNCTION_WORDS.add("ought");
        FUNCTION_WORDS.add("oughtn't"); FUNCTION_WORDS.add("our");
        FUNCTION_WORDS.add("ours"); FUNCTION_WORDS.add("ourselves");
        FUNCTION_WORDS.add("out"); FUNCTION_WORDS.add("over");
        FUNCTION_WORDS.add("quite"); FUNCTION_WORDS.add("rather");
        FUNCTION_WORDS.add("round"); FUNCTION_WORDS.add("second");
        FUNCTION_WORDS.add("seven"); FUNCTION_WORDS.add("seventeen");
        FUNCTION_WORDS.add("seventeenth"); FUNCTION_WORDS.add("seventh");
        FUNCTION_WORDS.add("seventieth"); FUNCTION_WORDS.add("seventy");
        FUNCTION_WORDS.add("shall"); FUNCTION_WORDS.add("shan't");
        FUNCTION_WORDS.add("she"); FUNCTION_WORDS.add("she'd");
        FUNCTION_WORDS.add("she'll"); FUNCTION_WORDS.add("she's");
        FUNCTION_WORDS.add("should"); FUNCTION_WORDS.add("shouldn't");
        FUNCTION_WORDS.add("since"); FUNCTION_WORDS.add("six");
        FUNCTION_WORDS.add("sixteen"); FUNCTION_WORDS.add("sixteenth");
        FUNCTION_WORDS.add("sixth"); FUNCTION_WORDS.add("sixtieth");
        FUNCTION_WORDS.add("sixty"); FUNCTION_WORDS.add("so");
        FUNCTION_WORDS.add("some"); FUNCTION_WORDS.add("somebody");
        FUNCTION_WORDS.add("someone"); FUNCTION_WORDS.add("something");
        FUNCTION_WORDS.add("sometimes"); FUNCTION_WORDS.add("somewhere");
        FUNCTION_WORDS.add("soon"); FUNCTION_WORDS.add("still");
        FUNCTION_WORDS.add("such"); FUNCTION_WORDS.add("ten");
        FUNCTION_WORDS.add("tenth"); FUNCTION_WORDS.add("than");
        FUNCTION_WORDS.add("that"); FUNCTION_WORDS.add("that's");
        FUNCTION_WORDS.add("the"); FUNCTION_WORDS.add("their");
        FUNCTION_WORDS.add("theirs"); FUNCTION_WORDS.add("them");
        FUNCTION_WORDS.add("themselves"); FUNCTION_WORDS.add("then");
        FUNCTION_WORDS.add("thence"); FUNCTION_WORDS.add("there");
        FUNCTION_WORDS.add("therefore"); FUNCTION_WORDS.add("these");
        FUNCTION_WORDS.add("they"); FUNCTION_WORDS.add("they'd");
        FUNCTION_WORDS.add("they'll"); FUNCTION_WORDS.add("they're");
        FUNCTION_WORDS.add("third"); FUNCTION_WORDS.add("thirteen");
        FUNCTION_WORDS.add("thirteenth"); FUNCTION_WORDS.add("thirtieth");
        FUNCTION_WORDS.add("thirty"); FUNCTION_WORDS.add("this");
        FUNCTION_WORDS.add("thither"); FUNCTION_WORDS.add("those");
        FUNCTION_WORDS.add("though"); FUNCTION_WORDS.add("thousand");
        FUNCTION_WORDS.add("thousandth"); FUNCTION_WORDS.add("three");
        FUNCTION_WORDS.add("thrice"); FUNCTION_WORDS.add("through");
        FUNCTION_WORDS.add("thus"); FUNCTION_WORDS.add("till");
        FUNCTION_WORDS.add("to"); FUNCTION_WORDS.add("today");
        FUNCTION_WORDS.add("tomorrow"); FUNCTION_WORDS.add("too");
        FUNCTION_WORDS.add("towards"); FUNCTION_WORDS.add("twelfth");
        FUNCTION_WORDS.add("twelve"); FUNCTION_WORDS.add("twentieth");
        FUNCTION_WORDS.add("twenty"); FUNCTION_WORDS.add("twice");
        FUNCTION_WORDS.add("two"); FUNCTION_WORDS.add("under");
        FUNCTION_WORDS.add("underneath"); FUNCTION_WORDS.add("unless");
        FUNCTION_WORDS.add("until"); FUNCTION_WORDS.add("up");
        FUNCTION_WORDS.add("us"); FUNCTION_WORDS.add("very");
        FUNCTION_WORDS.add("was"); FUNCTION_WORDS.add("wasn't");
        FUNCTION_WORDS.add("we"); FUNCTION_WORDS.add("we'd");
        FUNCTION_WORDS.add("we'll"); FUNCTION_WORDS.add("we're");
        FUNCTION_WORDS.add("we've"); FUNCTION_WORDS.add("were");
        FUNCTION_WORDS.add("weren't"); FUNCTION_WORDS.add("what");
        FUNCTION_WORDS.add("when"); FUNCTION_WORDS.add("whence");
        FUNCTION_WORDS.add("where"); FUNCTION_WORDS.add("whereas");
        FUNCTION_WORDS.add("which"); FUNCTION_WORDS.add("while");
        FUNCTION_WORDS.add("whither"); FUNCTION_WORDS.add("who");
        FUNCTION_WORDS.add("whom"); FUNCTION_WORDS.add("whose");
        FUNCTION_WORDS.add("why"); FUNCTION_WORDS.add("will");
        FUNCTION_WORDS.add("with"); FUNCTION_WORDS.add("within");
        FUNCTION_WORDS.add("without"); FUNCTION_WORDS.add("won't");
        FUNCTION_WORDS.add("would"); FUNCTION_WORDS.add("wouldn't");
        FUNCTION_WORDS.add("yes"); FUNCTION_WORDS.add("yesterday");
        FUNCTION_WORDS.add("yet"); FUNCTION_WORDS.add("you");
        FUNCTION_WORDS.add("you'd"); FUNCTION_WORDS.add("you'll");
        FUNCTION_WORDS.add("you're"); FUNCTION_WORDS.add("you've");
        FUNCTION_WORDS.add("your"); FUNCTION_WORDS.add("yours");
        FUNCTION_WORDS.add("yourself"); FUNCTION_WORDS.add("yourselves");

        return FUNCTION_WORDS;
    }

    private static Set<String> get_punctuation() {
        if (PUNCTUATION != null) { return PUNCTUATION; }

        Set<String> PUNCTUATION = new HashSet<>((int)(50 / 0.75) + 1);
        PUNCTUATION.add("!"); PUNCTUATION.add("\""); PUNCTUATION.add("#");
        PUNCTUATION.add("$"); PUNCTUATION.add("%"); PUNCTUATION.add("&");
        PUNCTUATION.add("'"); PUNCTUATION.add("("); PUNCTUATION.add(")");
        PUNCTUATION.add("*"); PUNCTUATION.add("+"); PUNCTUATION.add(",");
        PUNCTUATION.add("."); PUNCTUATION.add("/"); PUNCTUATION.add(":");
        PUNCTUATION.add(";"); PUNCTUATION.add("<"); PUNCTUATION.add("=");
        PUNCTUATION.add(">"); PUNCTUATION.add("?"); PUNCTUATION.add("@");
        PUNCTUATION.add("["); PUNCTUATION.add("]"); PUNCTUATION.add("\\");
        PUNCTUATION.add("]"); PUNCTUATION.add("^"); PUNCTUATION.add("_");
        PUNCTUATION.add("`"); PUNCTUATION.add("{"); PUNCTUATION.add("}");
        PUNCTUATION.add("~");
        PUNCTUATION.add("``"); PUNCTUATION.add("''"); PUNCTUATION.add("-");

        return PUNCTUATION;
    }

    private static Set<String> get_dictionary_words() {
        if (DICTIONARY != null) { return DICTIONARY; }

        try {
            Set<String> DICTIONARY = new HashSet<>((int)(500000 / 0.75) + 1);
            FileReader fr = new FileReader("/usr/share/dict/words");
            BufferedReader br = new BufferedReader(fr);
            try {
                String line = br.readLine();
                DICTIONARY.add(line.toLowerCase());
            } finally {
                br.close();
            }
            return DICTIONARY;
        } catch (IOException e) {
            System.err.println("Couldn't load dictionary");
            return new HashSet<>();
        }
    }

}
