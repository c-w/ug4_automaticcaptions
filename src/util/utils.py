import functools
import logging
import os
import random
import string
import subprocess
import sys


FLT_EPS = sys.float_info.epsilon


def flt_eq(a, b, eps=0.000001):
    return abs(a - b) < eps


def flatten(nested):
    return [el for li in nested for el in li]


def is_int(x):
    """Checks if x can be parsed as an integer

    >>> is_int(1)
    True
    >>> is_int(1.2)
    False
    >>> is_int('1')
    True
    >>> is_int('1.2')
    False

    >>> is_int('1e6')
    True
    >>> is_int('1.00')
    True
    >>> is_int('inf')
    False

    >>> is_int('string')
    Traceback (most recent call last):
        ...
    ValueError: not a number: string
    """

    try:
        x = float(x)
    except ValueError:
        raise ValueError('not a number: %s' % x)
    x = str(x)
    if '.' in x:
        integer, fractional = x.split('.')
        if all(f == '0' for f in fractional):
            return True
    try:
        int(x)
        return True
    except ValueError:
        pass
    return False


def reversedict(d):
    """Turns a key->value mapping into a value->key map.

    >>> kv = {'a': 1, 'b': 2, 'c': 3}
    >>> vk = reversedict(kv)
    >>> all(vk[kv[k]] == k for k in kv)
    True
    >>> all(kv[vk[v]] == v for v in vk)
    True
    """
    return dict((v, k) for (k, v) in d.iteritems())


def indexed_dict(it):
    """
    >>> d = indexed_dict('aaabbc')
    >>> sorted(d.values())
    [0, 1, 2]
    >>> sorted(d.keys())
    ['a', 'b', 'c']
    """
    return dict((el, i) for (i, el) in enumerate(sorted(set(it))))


def memoize(fn):
    cache = {}

    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        key = str(args) + str(kwargs)
        if key in cache:
            return cache[key]
        value = fn(*args, **kwargs)
        cache[key] = value
        return value
    return wrapper


def tldir():
    gitcommand = 'git rev-parse --show-toplevel'
    p = subprocess.Popen(gitcommand.split(), stdout=subprocess.PIPE)
    out, err = p.communicate()
    return os.path.abspath(out.strip())


def run(command, log_stdout=True, log_stderr=True):
    p = subprocess.Popen(
        command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = map(lambda s: s.strip(), p.communicate())
    if log_stdout:
        for line in (l for l in stdout.split('\n') if l):
            logging.info(line)
    if log_stderr:
        for line in (l for l in stderr.split('\n') if l):
            logging.info(line)
    return stdout, stderr


def random_string(nchars, alphabet=None):
    if alphabet is None:
        alphabet = string.ascii_uppercase + string.digits
    return ''.join(random.choice(alphabet) for _ in xrange(nchars))


if __name__ == '__main__':
    import doctest
    doctest.testmod()
