import __builtin__
import errno
import gzip
import os
import sys


def open(path, mode='r'):
    if not path:
        return sys.stdout
    path = normalize(path)
    f = (gzip.open(path, mode) if path.endswith('.gz')
         else __builtin__.open(path, mode))
    return f


def normalize(path):
    return os.path.abspath(os.path.expanduser(path))


def makedirs(path):
    path = normalize(path)
    dirname = os.path.dirname(path)
    try:
        os.makedirs(dirname)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
