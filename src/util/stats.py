def mean(it):
    seq = list(it)
    return sum(seq) / float(len(seq))


def sdev(it):
    seq = list(it)
    mu = mean(seq)
    return mean([(x - mu) ** 2 for x in seq]) ** 0.5
