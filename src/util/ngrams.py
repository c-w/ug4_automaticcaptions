#!/usr/bin/env python
"""Script to count ngram-frequency.  Reads text from a file or stdin."""


from collections import defaultdict
from errno import EPIPE
from nltk import word_tokenize, sent_tokenize
from nltk.util import ngrams


WORD_SEP = ' '


def tokenize(s):
    for sent in sent_tokenize(s):
        for word in word_tokenize(sent):
            yield word


def most_common_ngrams(infile):
    counts = defaultdict(int)
    for line in infile:
        tokens = list(tokenize(line.strip().lower()))
        for n in xrange(1, len(tokens) + 1):
            for ngram in ngrams(tokens, n):
                counts[ngram] += 1
    return sorted(((count, WORD_SEP.join(ngram))
                   for (ngram, count) in counts.iteritems()), reverse=True)


if __name__ == '__main__':
    import fileinput
    try:
        for count, ngram in most_common_ngrams(fileinput.input()):
            print '{count}\t{ngram}'.format(count=count, ngram=ngram)
    except IOError as e:
        if e.errno == EPIPE:
            pass
        else:
            raise
