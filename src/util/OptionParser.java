package util;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.Level;


public class OptionParser {
    private final String epilog;
    private Map<String, Option> options = new LinkedHashMap<>();
    private Map<String, Argument> positional_arguments = new LinkedHashMap<>();

    public OptionParser() { this(""); }
    public OptionParser(String epilog) {
        this.epilog = epilog;
        // add default options
        this.add_option("-h", "--help", "help", "boolean", "print this help message and exit", null, false);
        this.add_option("-v", "--verbose", "verbose", "boolean", "log more detail", null, false);
    }

    public void add_option(String short_name, String long_name, String dest, String type, String help, String metavar, Object default_value) {
        this.options.put(dest, new Option(short_name, long_name, type, help, metavar, default_value));
    }

    public void add_positional_argument(String name, String dest, String type, Object default_value) {
        this.positional_arguments.put(dest, new Argument(name, type, default_value));
    }

    protected void validate_args() throws ParsingException {}

    public void parse_args(String[] argv) {
        List<String> args = new ArrayList<>(Arrays.asList(argv));
        // check for help request
        Option help_option = this.options.get("help");
        for (String arg : args) {
            if (help_option.matches(arg)) {
                help_option.value = true;
                print_help();
            }
        }

        try {
            // process options
            for (Iterator<String> iterator = args.iterator(); iterator.hasNext();) {
                String arg = iterator.next();
                for (Option opt : this.options.values()) {
                    if (opt.matches(arg)) {
                        iterator.remove();
                        if (opt.is_positional) {
                            String value;
                            try {
                                value = iterator.next();
                            } catch (NoSuchElementException e) {
                                throw new ParsingException("Missing value for option " + arg);
                            }
                            try {
                                opt.value = opt.parse(value);
                                iterator.remove();
                            } catch (ParsingException e) {
                                throw new ParsingException("Invalid value '" + value + "' for option " + arg + ": " + e.getMessage());
                            }
                        } else {
                            opt.value = true;
                        }
                        break;
                    }
                }
            }

            if ((Boolean) this.options.get("verbose").value) {
                Logger log = LogManager.getLogManager().getLogger("");
                for (Handler h : log.getHandlers()) { h.setLevel(Level.ALL); }
            }

            // process positional arguments
            for (Iterator<String> iterator = args.iterator(); iterator.hasNext();) {
                String arg = iterator.next();
                for (Argument positional : this.positional_arguments.values()) {
                    if (positional.matches(arg)) {
                        try {
                            positional.value = positional.parse(arg);
                            iterator.remove();
                        } catch (ParsingException e) {
                            throw new ParsingException("Invalid argument '" + arg + "': " + e.getMessage());
                        }
                        break;
                    }
                }
            }
            int n_positional_args = 0;
            for (Argument positional : this.positional_arguments.values()) { if (positional.value != null) { n_positional_args++; } }
            if (n_positional_args < this.positional_arguments.size()) {
                throw new ParsingException("Need to provide at least " + this.positional_arguments.size() + " arguments (got " + n_positional_args + ")");
            }
            this.validate_args();
        } catch (ParsingException e) {
            this.print_error(e.getMessage());
        }
    }

    private void print_error(String message) {
        System.err.println(message);
        System.err.println("\n" + this.get_usage());
        System.exit(1);
    }

    private String get_usage() {
        // script name
        String script_name = Utils.get_main_class();
        // positional argument list
        StringBuilder sb = new StringBuilder(256);
        for (Argument positional : this.positional_arguments.values()) { sb.append(" " + positional.name); }
        return "Usage: java " + script_name + " [options]" + sb.toString();
    }

    private void print_help() {
        // usage
        String usage = this.get_usage();
        StringBuilder sb = new StringBuilder(usage.length() + 512);
        sb.append(usage);
        // options
        sb.append("\n\nOptions:");
        Map<Option, Integer> opt_lens = new HashMap<>(this.options.size());
        for (Option opt : this.options.values()) {
            int opt_len = 0;
            int metavar_len = (opt.metavar != null) ? opt.metavar.length() : 0;
            if (opt.short_name != null) { opt_len += opt.short_name.length() + metavar_len + 3; }
            if (opt.long_name != null)  { opt_len += opt.long_name.length() + metavar_len + 1; }
            opt_lens.put(opt, opt_len);
        }
        int max_opt_len = Collections.max(opt_lens.values());
        for (Option opt : this.options.values()) {
            int opt_len = opt_lens.get(opt);
            // option name
            sb.append("\n  ");
            if (opt.short_name != null) {
                sb.append(opt.short_name);
                if (opt.metavar != null) { sb.append(' ').append(opt.metavar); }
                sb.append(", ");
            }
            if (opt.long_name != null) {
                sb.append(opt.long_name);
                if (opt.metavar != null) { sb.append('=').append(opt.metavar); }
            }
            // padding
            int pad_len = 2 + max_opt_len - opt_len + ((opt.metavar == null) ? 2 : 0);
            sb.append(StringUtils.replicate(' ', pad_len));
            // help message
            sb.append(opt.help);
        }
        // epilog
        sb.append("\n\n" + this.epilog);
        System.err.println(sb.toString());
        System.exit(0);
    }

    public Integer get_integer_option(String dest) {
        Option opt = this.options.get(dest);
        if (opt == null) { return null; }
        return (Integer) opt.value;
    }
    public Double get_double_option(String dest) {
        Option opt = this.options.get(dest);
        if (opt == null) { return null; }
        return (Double) opt.value;
    }
    public String get_string_option(String dest) {
        Option opt = this.options.get(dest);
        if (opt == null) { return null; }
        return (String) opt.value;
    }
    public Character get_character_option(String dest) {
        Option opt = this.options.get(dest);
        if (opt == null) { return null; }
        return (Character) opt.value;
    }
    public File get_file_option(String dest) {
        Option opt = this.options.get(dest);
        if (opt == null) { return null; }
        return (File) opt.value;
    }
    public Boolean get_boolean_option(String dest) {
        Option opt = this.options.get(dest);
        if (opt == null) { return null; }
        return (Boolean) opt.value;
    }

    public Integer get_integer_argument(String dest) {
        Argument arg = this.positional_arguments.get(dest);
        if (arg == null) { return null; }
        return (Integer) arg.value;
    }
    public Double get_double_argument(String dest) {
        Argument arg = this.positional_arguments.get(dest);
        if (arg == null) { return null; }
        return (Double) arg.value;
    }
    public String get_string_argument(String dest) {
        Argument arg = this.positional_arguments.get(dest);
        if (arg == null) { return null; }
        return (String) arg.value;
    }
    public Character get_character_argument(String dest) {
        Argument arg = this.positional_arguments.get(dest);
        if (arg == null) { return null; }
        return (Character) arg.value;
    }
    public File get_file_argument(String dest) {
        Argument arg = this.positional_arguments.get(dest);
        if (arg == null) { return null; }
        return (File) arg.value;
    }
    public Boolean get_boolean_argument(String dest) {
        Argument arg = this.positional_arguments.get(dest);
        if (arg == null) { return null; }
        return (Boolean) arg.value;
    }
}


abstract class Parsable {
    Object value;
    final ParsableType type;

    Parsable(String type, Object default_value) {
        this.type = ParsableType.from_string(type);
        this.value = default_value;
    }

    public Object parse(String arg) throws ParsingException { return this.type.parse(arg); }
    public abstract boolean matches(String arg);
}


class Argument extends Parsable {
    final String name;
    private boolean matched;

    public Argument(String name, String type, Object default_value) {
        super(type, default_value);
        this.name = name;
        this.matched = false;
    }

    public boolean matches(String arg) {
        if (!this.matched) { this.matched = true; return true; }
        return false;
    }
}


class Option extends Parsable {
    final String short_name;
    final String long_name;
    final String help;
    final String metavar;
    final boolean is_positional;

    public Option(String short_name, String long_name, String type, String help, String metavar, Object default_value) {
        super(type, default_value);
        this.short_name = short_name;
        this.long_name = long_name;
        this.help = help;
        this.metavar = metavar;
        this.is_positional = metavar != null;
        if ((short_name == null) && (long_name == null)) {
            throw new IllegalArgumentException("need to set at least one of short_name or long_name");
        }
        switch (this.type) {
            case INTEGER:
            case DOUBLE:
            case STRING:
            case CHARACTER:
            case FILE:
                if (this.metavar == null) { throw new IllegalArgumentException("need to specify metavar when creating a positional option"); }
                break;
            case BOOLEAN:
                if (this.metavar != null) { throw new IllegalArgumentException("conflicting arguments: can't specify metavar for a boolean option"); }
                break;
        }
    }

    public boolean matches(String arg) { return arg != null && (arg.equals(this.short_name) || arg.equals(this.long_name)); }
}


enum ParsableType {
    INTEGER("integer"),
    DOUBLE("double"),
    STRING("string"),
    CHARACTER("character"),
    FILE("file"),
    BOOLEAN("boolean"),
    ;

    final String name;
    private ParsableType(String name) { this.name = name; }

    static ParsableType from_string(String s) {
        switch (s.toLowerCase()) {
            case "integer":   return ParsableType.INTEGER;
            case "double":     return ParsableType.DOUBLE;
            case "string":    return ParsableType.STRING;
            case "character": return ParsableType.CHARACTER;
            case "file":      return ParsableType.FILE;
            case "boolean":   return ParsableType.BOOLEAN;
            default:          throw new IllegalArgumentException("unsupported argument type " + s);
        }
    }

    public Object parse(String arg) throws ParsingException {
        if (arg == null) { return null; }
        try {
            switch (this) {
                case INTEGER:
                    return Integer.parseInt(arg);
                case DOUBLE:
                    return Double.parseDouble(arg);
                case STRING:
                    return arg;
                case CHARACTER:
                    // FIXME this is a bit of an abuse of notation...
                    if (arg.length() != 1) { throw new NumberFormatException(); }
                    return arg.charAt(0);
                case FILE:
                    File file = new File(arg);
                    if (!file.exists()) { throw new ParsingException("file does not exist"); }
                    return file;
                case BOOLEAN:
                    arg = arg.toLowerCase();
                    if (arg.equals("true")  || arg.equals("1") || arg.equals("y") || arg.equals("yes")) { return true; }
                    if (arg.equals("false") || arg.equals("0") || arg.equals("n") || arg.equals("no")) { return false; }
                    throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            throw new ParsingException(this.name + " expected");
        }
        return null;
    }
}

class ParsingException extends Exception {
    private static final long serialVersionUID = 1583278901L;
    ParsingException(String message) { super(message); }
}
