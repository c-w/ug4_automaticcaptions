package util;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class DatabaseUtils {
    private DatabaseUtils() {};

    public static int[][] get_word_features(Connection db_connection)
    throws SQLException {
        Statement s = db_connection.createStatement();
        try {
            String query =
                "SELECT tokens_vector " +
                "FROM descriptions " +
                "ORDER BY image_id, description_id";
            int num_wfs = DatabaseUtils.get_size(db_connection, query);
            int[][] wfs = new int[num_wfs][];
            ResultSet r = s.executeQuery(query);
            try {
                for (int i = 0; r.next(); i++) {
                    String[] token_csv = r.getString("tokens_vector").split(",");
                    wfs[i] = ArrayUtils.floor(ArrayUtils.to_double(token_csv));
                }
            } finally { r.close(); }
            return wfs;
        } finally { s.close(); }
    }

    public static double[][] get_visual_features_continuous(Connection db_connection)
    throws SQLException {
        return get_visual_features(db_connection, false);
    }

    public static double[][] get_visual_features_discrete(Connection db_connection)
    throws SQLException {
        return get_visual_features(db_connection, true);
    }

    private static double[][] get_visual_features(Connection db_connection, boolean discrete)
    throws SQLException {
        Statement s = db_connection.createStatement();
        try {
            String query =
                "SELECT visual_vector " +
                "FROM visual_features_" + (discrete ? "discrete" : "continuous") + " " +
                "NATURAL JOIN descriptions " +
                "ORDER BY image_id, description_id";
            int num_vfs = DatabaseUtils.get_size(db_connection, query);
            double[][] vfs = new double[num_vfs][];
            ResultSet r = s.executeQuery(query);
            try {
                for (int i = 0; r.next(); i++) {
                    String[] token_csv = r.getString("visual_vector").split(",");
                    vfs[i] = ArrayUtils.to_double(token_csv);
                }
            } finally { r.close(); }
            return vfs;
        } finally { s.close(); }
    }

    public static int get_size(Connection connection, String query)
    throws SQLException {
        Statement s = connection.createStatement();
        try {
            ResultSet r = s.executeQuery("SELECT COUNT(*) FROM (" + query + ")");
            try {
                int query_size = r.getInt(1);
                return query_size;
            } finally { r.close(); }
        } finally { s.close(); }
    }

    public static class DatabaseScriptOptionParser extends OptionParser {
        public static final String LIMIT_OPT = "limit";
        public static final String OFFSET_OPT = "offset";
        public static final String BATCH_SIZE_OPT = "batch_size";
        public static final String PATH_TO_DB_ARG = "path_to_db";

        public DatabaseScriptOptionParser() { this(""); }
        public DatabaseScriptOptionParser(String epilog) {
            super(epilog);
            this.add_option(
                "-l", "--limit", LIMIT_OPT, "integer",
                "only process X descriptions", "X", null);
            this.add_option(
                "-o", "--offset", OFFSET_OPT, "integer",
                "skip the top X descriptions", "X", null);
            this.add_option(
                "-b", "--batch-size", BATCH_SIZE_OPT, "integer",
                "commit after processing every R rows", "R", 500);
            this.add_positional_argument("/path/to/db", PATH_TO_DB_ARG, "file", null);
        }

        protected void validate_args()
        throws ParsingException {
            Integer offset = this.get_integer_option(OFFSET_OPT);
            Integer limit = this.get_integer_option(LIMIT_OPT);
            if (offset != null && limit == null) { throw new ParsingException("OFFSET only allowed in combination with LIMIT"); }
            if ((offset != null && offset < 0) || (limit != null && limit < 0)) { throw new ParsingException("OFFSET and LIMIT should be positive"); }
        }
    }
}
