function ternary_plot(datapath, varargin)
    if nargin >= 2
        outpath = varargin{1};
    else
        [pathstr, fname, ~] = fileparts(datapath);
        outpath = fullfile(pathstr, strcat(fname, '-ternplot.png'));
    end

    data = importdata(datapath);
    labels = data.colheaders;
    range = data.data(:,1:3);
    values = data.data(:,4);

    fig = figure('Visible', 'off');
    colormap(jet);
    [hg, htick, hcb] = tersurf(range(:,1), range(:,2), range(:,3), values);
    hlabels = terlabel(labels(1), labels(2), labels(3));
    title(hcb, labels(4));

    set(fig, 'PaperUnits', 'inches');
    set(fig, 'PaperSize', [10 10]);
    set(fig, 'PaperPosition', [0 0 10 10]);
    print(fig, outpath, '-dpng', '-r100');
    close(fig);
end
