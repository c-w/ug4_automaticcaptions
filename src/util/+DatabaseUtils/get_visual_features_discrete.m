function vfc = get_visual_features_discrete(db_path)
    db_connection = mksqlite(0, 'open', db_path);
    sql = ['SELECT visual_vector '...
           'FROM visual_features_discrete '...
           'NATURAL JOIN descriptions '...
           'ORDER BY image_id, description_id '];
    rows = mksqlite(db_connection, sql);
    nrows = size(rows, 1);
    ncols = size(str2num(rows(1).visual_vector), 2);
    vfc = zeros(nrows, ncols);
    for j = 1:nrows
        vfc(j,:) = str2num(rows(j).visual_vector);
    end
    mksqlite(db_connection, 'close');
end
