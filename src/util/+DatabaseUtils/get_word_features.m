function wfs = get_word_features(db_path)
    db_connection = mksqlite(0, 'open', db_path);
    sql = ['SELECT tokens_vector '...
           'FROM descriptions '...
           'ORDER BY image_id, description_id '];
    rows = mksqlite(db_connection, sql);
    nrows = size(rows, 1);
    ncols = size(str2num(rows(1).tokens_vector), 2);
    wfs = zeros(nrows, ncols);
    for j = 1:nrows
        wfs(j,:) = str2num(rows(j).tokens_vector);
    end
    mksqlite(db_connection, 'close');
end
