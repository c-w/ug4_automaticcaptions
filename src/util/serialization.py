import cPickle as pickle
import paths


class WritableObject(object):
    def write(self, path):
        paths.makedirs(path)
        f = paths.open(path, 'wb')
        try:
            pickle.dump(self, f)
        finally:
            f.close()

    @staticmethod
    def read(path):
        f = paths.open(path, 'rb')
        try:
            model = pickle.load(f)
        finally:
            f.close()
        return model
