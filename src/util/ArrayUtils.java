package util;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ArrayUtils {
    private ArrayUtils() {}

    public static void write(double[] array, String path)
    throws IOException {
        if (array == null) { return; }
        int num_rows = array.length;
        int buf_size = num_rows * 2;
        BufferedWriter br = new BufferedWriter(new FileWriter(path), buf_size);
        try {
            for (double elem : array) {
                br.write(Double.toString(elem));
                br.write('\n');
            }
        } finally {
            br.close();
        }
    }

    public static double[][] read(String path, char col_delim)
    throws IOException {
        String delim = String.valueOf(col_delim);
        BufferedReader br = new BufferedReader(new FileReader(path));
        try {
            List<double[]> buf = new ArrayList<>();
            String line = br.readLine();
            while (line != null) {
                buf.add(to_double(line.split(delim)));
                line = br.readLine();
            }
            double[][] values = new double[buf.size()][];
            for (int i = 0; i < buf.size(); i++) { values[i] = buf.get(i); }
            return values;
        } finally {
            br.close();
        }
    }

    public static int[] get_column(int c, int[][] matrix) {
        if (matrix == null) { return null; }
        if (c < 0) { throw new IllegalArgumentException("column index can't be negative"); }
        int[] col = new int[matrix.length];
        for (int i = 0; i < matrix.length; i++) { col[i] = matrix[i][c]; }
        return col;
    }

    public static int[] floor(double[] vector) {
        if (vector == null) { return null; }
        int[] floored = new int[vector.length];
        for (int i = 0; i < vector.length; i++) {
            floored[i] = (int) Math.floor(vector[i]);
        }
        return floored;
    }

    public static int[][] floor(double[][] matrix) {
        if (matrix == null) { return null; }
        int[][] floored = new int[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            double[] row = matrix[i];
            floored[i] = new int[row.length];
            for (int j = 0; j < row.length; j++) {
                floored[i][j] = (int) Math.floor(row[j]);
            }
        }
        return floored;
    }

    public static double[] to_double(String[] A) {
        if (A == null) { return null; }
        double[] B = new double[A.length];
        for (int i = 0; i < B.length; i++) { B[i] = Double.parseDouble(A[i]); }
        return B;
    }

    public static double[] binarize(double[] vector, double threshold) {
        if (vector == null) { return null; }
        for (int i = 0; i < vector.length; i++) {
            double v = vector[i];
            vector[i] = (v > threshold) ? 1 : 0;
        }
        return vector;
    }

    public static double[][] binarize(double[][] matrix, double threshold) {
        if (matrix == null) { return null; }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                double v = matrix[i][j];
                matrix[i][j] = (v > threshold) ? 1 : 0;
            }
        }
        return matrix;
    }

    public static int[][] binarize(int[][] matrix, int threshold) {
        if (matrix == null) { return null; }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int v = matrix[i][j];
                matrix[i][j] = (v > threshold) ? 1 : 0;
            }
        }
        return matrix;
    }
}
