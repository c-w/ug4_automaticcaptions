package util;


import java.util.Collection;


public class StringUtils {
    private StringUtils() {}

    public static <T> String join(final Collection<T> collection, char delim) {
        if (collection == null) { return null; }
        StringBuilder sb = new StringBuilder(collection.size() * 2);
        for (T elem : collection) {
            if (elem == null) { continue; }
            sb.append(elem.toString()).append(delim);
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public static String join(final double[] array, char delim) {
        if (array == null) { return null; }
        StringBuilder sb = new StringBuilder(array.length * 2);
        for (double elem : array) { sb.append(elem).append(delim); }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public static String join(final int[] array, char delim) {
        if (array == null) { return null; }
        StringBuilder sb = new StringBuilder(array.length * 2);
        for (int elem : array) { sb.append(elem).append(delim); }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public static String replicate(char c, int ntimes) {
        if (ntimes <= 0) { return ""; }
        return new String(new char[ntimes]).replace('\0', c);
    }
}
