#!/usr/bin/env python


import csv
import re


SYSTEMS = {1: 'human', 2: 'keyword baseline', 3: 'template model'}
IMAGES = range(1, 20 + 1)


def read_csv(infile):
    rows = list(csv.reader(infile))
    header = [re.sub('[^a-zA-Z0-9_]', '_', field) for field in rows[0]]
    for row in rows[1:]:
        yield dict(map(None, header, row))


if __name__ == '__main__':
    import fileinput
    print ','.join(map(str, (
        'subject id',
        'image id',
        'system id',
        'relevance score',
    )))
    for hit in read_csv(fileinput.input()):
        subject_id = hit['WorkerId']
        for image_id in IMAGES:
            image_name = hit['Input_IMG%d' % image_id]
            for system_id, system_name in SYSTEMS.iteritems():
                key = 'Answer_IMG%d_DESC%d' % (image_id, system_id)
                relevance = hit[key + '_R']
                print ','.join(map(str, (
                    subject_id,
                    image_name,
                    system_name,
                    relevance,
                )))
