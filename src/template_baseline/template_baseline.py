#!/usr/bin/env python


from util.constants import COL_DELIM, VECT_DELIM, WORD_DELIM
import collections
import json
import logging
import os
import sqlite3
import template_generator
import util.graph as graph
import util.learning as learning
import util.option_parser as option_parser
import util.paths as paths
import util.serialization as serialization
from template_generator import TemplateGenerator  # import for pickle


class SingleKeywordGenerator(learning.TranslatingModel):
    def __init__(self, dependency_type, model_type, metaparams=None):
        super(SingleKeywordGenerator, self).__init__(model_type,
                                                     metaparams=metaparams)
        self._dependency_type = dependency_type.lower()


class MultiKeywordGenerator(serialization.WritableObject):
    def __init__(self, model_type, label_candidates, template_model=None,
                 metaparams=None):
        self._model_type = model_type
        kwgen = lambda dep: SingleKeywordGenerator(dep, model_type, metaparams)
        self.experts = dict((dep, kwgen(dep)) for dep in label_candidates)
        self._template_model = template_model

    def __repr__(self):
        return self._model_type

    def fit(self, features, targets, dependency):
        self.experts[dependency].fit(features, targets)
        return self

    def predict(self, features, nbest_templates=1, nbest_words=1,
                template_override=None):
        template_model = self._template_model
        candidates_for_features = []
        for feature_vector in features:
            if template_model is None or template_override is not None:
                templates = [(1.0, template_override)]
            else:
                templates = template_model.predict_log_proba(
                    [feature_vector], nbest_templates)[0]
            candidates_for_vector = []
            for template_confidence, template in templates:
                description = []
                for dependency in template.split(VECT_DELIM):
                    model = self.experts[dependency]
                    prediction = [(template_confidence + word_confidence, word)
                                  for (word_confidence, word) in
                                  model.predict_log_proba([feature_vector],
                                                          nbest_words)[0]]
                    description.append(prediction)
                candidates_for_vector.append(description)
            candidates_for_features.append(candidates_for_vector)
        return candidates_for_features


def train(model_path, db_path, model_type, label_candidates,
          template_model=None, use_raw_words=False,
          fold=learning.FOLDS.TRAINING, metaparams=None):
    data = get_training_data(db_path, label_candidates, use_raw_words, fold)
    model = MultiKeywordGenerator(model_type, label_candidates,
                                  template_model, metaparams=metaparams)
    for dependency, (_, features, labels) in data.iteritems():
        model.fit(features, labels, dependency)
        logging.info('trained model for %s using %d instances'
                     % (dependency, len(features)))
    model.write(model_path)
    logging.info('wrote model to %s' % model_path)


def all_sentences(template):
    template_graph = {}
    scores = {}
    for i, slot in enumerate(template):
        for score, element in slot:
            node = (i, element)
            try:
                neighbours = [(i + 1, el) for _, el in template[i + 1]]
            except IndexError:
                neighbours = []
            template_graph[node] = neighbours
            scores[node] = score
    A, Z = 0, len(template) - 1
    paths = [path
             for _, start in template[A]
             for _, end in template[Z]
             for path in graph.all_paths(template_graph, (A, start), (Z, end))]
    scored_paths = [(sum(scores[node] for node in path),
                     (element for _, element in path))
                    for path in paths]
    scored_paths.sort(reverse=True)
    return [path for _, path in scored_paths]


def predict(model_path, db_path, n_templates=5, n_words=1,
            outpath=None, fold=learning.FOLDS.VALIDATION,
            template_override=None):
    model = MultiKeywordGenerator.read(model_path)
    image_ids, features, labels = get_evaluation_data(db_path, fold)
    predicted = model.predict(features,
                              nbest_templates=n_templates,
                              nbest_words=n_words,
                              template_override=template_override)
    logging.info('generated %d predictions' % len(predicted))
    predicted_sentences = collections.defaultdict(list)
    for i, candidates in enumerate(predicted):
        image_id = image_ids[i]
        for candidate_template in candidates:
            predicted_sentences[image_id].extend(
                WORD_DELIM.join(s) for s in all_sentences(candidate_template))
        logging.info('expanded prediction %d/%d' % (i + 1, len(predicted)))
    logging.info('expanded predictions to %d candidates'
                 % sum(len(v) for v in predicted_sentences.itervalues()))
    outfile = paths.open(outpath, 'w')
    try:
        outfile.write('# Model:template_baseline('
                      'candidate_templates=%d, candidate_words=%d)\n'
                      % (n_templates, n_words))
        outfile.write('# Classifier:%s\n' % model)
        outfile.write('# Image Id%s# Predicted Description\n' % COL_DELIM)
        for image_id, predictions in sorted(predicted_sentences.items()):
            for prediction in predictions:
                outfile.write('%d%s%s\n' % (image_id, COL_DELIM, prediction))
    finally:
        outfile.close()


def get_evaluation_data(db_path, fold=learning.FOLDS.VALIDATION):
    unpack = lambda (a, b, c): (
        int(a), map(float, b.split(VECT_DELIM)), c)
    with sqlite3.connect(db_path) as db_connection:
        data = (unpack(row) for row in db_connection.execute(
                'SELECT image_id, '
                '       visual_vector, '
                '       GROUP_CONCAT(lemma, "%s") '
                'FROM ( '
                '  SELECT * '
                '  FROM words '
                '  WHERE image_id IN %s_set '
                '    AND is_punctuation = 0 '
                '  ORDER BY image_id, description_id) '
                'NATURAL JOIN visual_features_discrete '
                'GROUP BY image_id' % (WORD_DELIM, fold)))
        image_ids, features, labels = zip(*data)
    logging.info('loaded %d %s data' % (len(labels), fold))
    return image_ids, features, labels


def get_training_data(db_path, label_candidates, use_raw_words=False,
                      fold=learning.FOLDS.TRAINING):
    word_feature = 'raw_word' if use_raw_words else 'lemma'
    unpack = lambda (a, b, c): (int(a), map(float, b.split(VECT_DELIM)), c)
    data = {}
    with sqlite3.connect(db_path) as db_connection:
        for dependency, candidates in label_candidates.iteritems():
            features = (unpack(row) for row in db_connection.execute(
                'SELECT image_id, visual_vector, %s '
                'FROM ( '
                '  SELECT DISTINCT image_id, %s '
                '  FROM %s_set '
                '  NATURAL JOIN words '
                '  WHERE dependency LIKE "%s" '
                '    AND lemma LIKE "%s") '
                'NATURAL JOIN visual_features_discrete'
                % (word_feature, word_feature, fold, dependency,
                   '" OR lemma LIKE "'.join(candidates) if len(candidates) > 0
                   else '%')))
            image_ids, features, labels = zip(*features)
            data[dependency] = (image_ids, features, labels)
    for dependency, (image_ids, features, labels) in data.iteritems():
        logging.info('loaded %d %s data for %s'
                     % (len(image_ids), fold, dependency))
    return data


def get_default_label_candidates(db_path):
    with sqlite3.connect(db_path) as db_connection:
        labels = dict((row[0], []) for row in db_connection.execute(
            'SELECT DISTINCT dependency '
            'FROM words'))
    return labels


if __name__ == '__main__':
    parser = option_parser.OptionParser(
        epilog=('\nIf mode=%s:'
                '\nUse the model at path/to/model to generate descriptions '
                'for images.'
                '\n\nIf mode=%s:'
                '\nTrain the description generation model and save it at '
                'path/to/model.'
                % (learning.MODES.PREDICT, learning.MODES.TRAIN)))
    parser.add_positional_argument('mode', type='choice',
                                   choices=learning.MODES)
    parser.add_positional_argument('path/to/db', type='file')
    parser.add_positional_argument('path/to/model', type='string')
    parser.add_option('--model-type', metavar='T', type='choice',
                      choices=list(learning.MODELS), dest='model_type',
                      default=learning.MODELS.LOGISTIC_REGRESSION,
                      help='set type of base classifer to use')
    parser.add_option('--raw-words', action='store_true', dest='raw_words',
                      default=False,
                      help='predict words instead of lemma')
    parser.add_option('--label-candidates', metavar='P',
                      dest='label_candidates', default='',
                      help=('only learn template-fill models for the '
                            'dependency tags and lemma-candidates specified '
                            'in the JSON file P (default: anything goes)'))
    parser.add_option('--template-model', metavar='P',
                      dest='template_model', default='',
                      help=('use the serialized template prediction model at '
                            'path P as a source for the predicted description '
                            'templates (default: always predict the most '
                            'common template)'))
    parser.add_option('--fold', choices=list(learning.FOLDS),
                      dest='fold', default=learning.FOLDS.DEVELOPMENT,
                      help=('set subset of data to use for training or '
                            'evaluation purposes'))
    parser.add_option('--n-candidate-templates', metavar='N', type='int',
                      dest='n_candidate_templates', default=5,
                      help=('generate N candidate template descriptions for '
                            'every image'))
    parser.add_option('--n-candidate-words', metavar='N', type='int',
                      dest='n_candidate_words', default=1,
                      help=('generate N candidate words for every slot in '
                            'every template'))
    parser.add_option('--outpath', metavar='P',
                      dest='outpath', default=None,
                      help='save results at path P (default: print to stdout)')
    parser.add_option('--template-override', metavar='T',
                      dest='template_override', default=None,
                      help='force model to use template T')
    parser.add_option('--model-metaparams', metavar='M',
                      dest='model_metaparams', default='penalty=l1,C=0.5',
                      help=('if M is in the form "a=b,c=d" then pass the '
                            'kwargs {a:b, c:d} to the constructor of the base '
                            'classifiers'))
    opts, args = parser.parse_args()
    # unpack opts and args
    mode = args[0]
    db_path = os.path.abspath(args[1])
    model_path = os.path.abspath(args[2])
    label_candidates_path = opts.label_candidates or None
    template_model_path = opts.template_model or None
    model_type = opts.model_type
    use_raw_words = opts.raw_words
    n_templates = opts.n_candidate_templates
    n_words = opts.n_candidate_words
    outpath = opts.outpath
    template_override = template_generator.parse_string(opts.template_override)
    metaparams = option_parser.parse_kwargs(opts.model_metaparams, None)
    fold = opts.fold

    # train or use model
    if mode == learning.MODES.TRAIN:
        # get label candidates
        if label_candidates_path is not None:
            path = paths.normalize(label_candidates_path)
            with open(path) as f:
                label_candidates = json.load(f)
        else:
            label_candidates = get_default_label_candidates(db_path)
            logging.warning('no label candidates specified --- using defaults')
        # get template model
        if template_model_path is not None:
            template_model = TemplateGenerator.read(template_model_path)
        else:
            template_model = None
            logging.warning('no template model specified -- will have to use '
                            'template override option at prediction time')
        # train model
        train(model_path=model_path,
              db_path=db_path,
              model_type=model_type,
              label_candidates=label_candidates,
              template_model=template_model,
              use_raw_words=use_raw_words,
              fold=fold,
              metaparams=metaparams)
    elif mode == learning.MODES.PREDICT:
        # generate predictions
        predict(model_path=model_path,
                db_path=db_path,
                n_templates=n_templates,
                n_words=n_words,
                outpath=outpath,
                fold=fold,
                template_override=template_override)
