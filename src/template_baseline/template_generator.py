from util.constants import COL_DELIM, VECT_DELIM
import collections
import logging
import nltk
import os
import sqlite3
import util.learning as learning
import util.option_parser as option_parser
import util.serialization as serialization
import util.stats as stats
import util.utils as utils


BEST_MODEL = learning.MODELS.LOGISTIC_REGRESSION
ALL_TEMPLATE = ('root,advmod,amod,aux,auxpass,cc,conj,cop,'
                'det,dobj,expl,nn,nsubj,nsubjpass,pobj,prep')
BEST_TEMPLATE = 'nsubj,aux,root,det,dobj'


class TemplateGenerator(serialization.WritableObject,
                        learning.TranslatingModel):
    pass


def parse_string(s):
    if s is None:
        return None
    s = s.lower()
    if s == 'all-template':
        return ALL_TEMPLATE
    if s == 'best-template':
        return BEST_TEMPLATE
    allowed = set(ALL_TEMPLATE.split(','))
    if not all(dep in allowed for dep in s.split(',')):
        raise ValueError('unrecognized template "%s"' % s)
    return s


@utils.memoize
def edit_distance(a, b):
    return nltk.metrics.edit_distance(a.split(VECT_DELIM), b.split(VECT_DELIM))


def get_data(db_path, fold):
    unpack = lambda (a, b, c): (int(a), map(float, b.split(VECT_DELIM)), c)
    with sqlite3.connect(db_path) as db_connection:
        data = (unpack(row) for row in db_connection.execute(
                'SELECT image_id, visual_vector, template '
                'FROM descriptions '
                'NATURAL JOIN visual_features_discrete '
                'WHERE template IN ( '
                '    SELECT template '
                '    FROM template_histogram) '
                '  AND image_id IN %s_set' % fold))
        image_ids, features, labels = zip(*data)
    logging.info('loaded %d %s data' % (len(labels), fold))
    return image_ids, features, labels


def train(model_path, db_path, model_type, fold=learning.FOLDS.TRAINING):
    data = get_data(db_path, fold)
    _, training_features, training_labels = data
    model = TemplateGenerator(model_type)
    model.fit(training_features, training_labels)
    logging.info('trained model with %d instances' % len(training_features))
    model.write(model_path)
    logging.info('wrote model to %s' % model_path)


def predict(model_path, db_path, fold=learning.FOLDS.VALIDATION):
    data = get_data(db_path, fold)
    image_ids, evaluation_features, evaluation_labels = data
    model = TemplateGenerator.read(model_path)
    predictions = model.predict(evaluation_features)
    logging.info('generated %d predictions' % len(evaluation_features))
    oracle = zip(image_ids, predictions, evaluation_labels)
    print_results(model, oracle)
    print_evaluation(model, oracle)


def print_results(model, oracle):
    print '# %s' % str(model)
    print COL_DELIM.join(('# Predicted template', '# True template'))
    for _, predicted_label, true_label in oracle:
        print COL_DELIM.join((predicted_label, true_label))


def print_evaluation(model, oracle):
    print '# %s' % str(model)
    confusion = collections.defaultdict(list)
    for _, predicted_label, true_label in oracle:
        badness = edit_distance(true_label, predicted_label)
        confusion[true_label].append(badness)
    means, sdevs, maxs, mins = [], [], [], []
    for label, dists in confusion.iteritems():
        dist_mean = stats.mean(dists)
        dist_sdev = stats.sdev(dists)
        dist_max = max(dists)
        dist_min = min(dists)
        means.append(dist_mean)
        sdevs.append(dist_sdev)
        maxs.append(dist_max)
        mins.append(dist_min)
        print '# label %s - edit distance mean: %f' % (label, dist_mean)
        print '# label %s - edit distance sdev: %f' % (label, dist_sdev)
        print '# label %s - edit distance max: %f' % (label, dist_max)
        print '# label %s - edit distance min: %f' % (label, dist_min)
    print '# overall - edit distance mean: %f' % stats.mean(means)
    print '# overall - edit distance sdev: %f' % stats.mean(sdevs)
    print '# overall - edit distance max: %f' % stats.mean(maxs)
    print '# overall - edit distance min: %f' % stats.mean(mins)


if __name__ == '__main__':
    parser = option_parser.OptionParser(
        epilog=('\nIf mode=%s:'
                '\nUse the model at path/to/model to predict which type of '
                'description template should be used with which kinds of '
                'images. Predicted and true templates are printed to '
                'standard-out (TSV format).'
                '\n\nIf mode=%s:'
                '\nTrain the template-type prediction model and save it at '
                'path/to/model.'
                % (learning.MODES.PREDICT, learning.MODES.TRAIN)))
    parser.add_positional_argument('mode', type='choice',
                                   choices=learning.MODES)
    parser.add_positional_argument('path/to/db', type='file')
    parser.add_positional_argument('path/to/model', type='string')
    parser.add_option('--model-type', metavar='T', type='choice',
                      choices=list(learning.MODELS), dest='model_type',
                      default=learning.MODELS.LOGISTIC_REGRESSION,
                      help='set type of base classifer to use')
    parser.add_option('--fold', choices=list(learning.FOLDS),
                      dest='fold', default=learning.FOLDS.DEVELOPMENT,
                      help=('set subset of data to use for training or '
                            'evaluation purposes'))
    opts, args = parser.parse_args()
    # unpack opts and args
    mode = args[0]
    db_path = os.path.abspath(args[1])
    model_path = os.path.abspath(args[2])
    model_type = opts.model_type
    fold = opts.fold

    # train or use model
    if mode == learning.MODES.TRAIN:
        train(model_path, db_path, model_type, fold=fold)
    elif mode == learning.MODES.PREDICT:
        predict(model_path, db_path, fold=fold)
