#!/bin/bash
# Fetches datasets into the directory of the script
# Adds the dataset directories to .gitignore to avoid cluttering the repository


set -e


# utility declarations
scriptdir="$(dirname $(readlink -f $0))"

function fchop() {
    local tmpfile=$(mktemp)
    tail -n +$(($1+1)) $3 > $tmpfile && mv $tmpfile $3
    head -n -$2 $3 > $tmpfile && mv $tmpfile $3
}

function chop() {
    local tmp=${3#$1}
    echo ${tmp%$2}
}

function echo_emph() {
    echo -e "\e[44m$1\e[49m"
}


# -----------------------------------------------------------------------------#
# Abstract Scenes Dataset
# Reference:
#   C. L. Zitnick, and D. Parikh, Bringing Semantics Into Focus Using Visual
#   Abstraction, In CVPR, 2013.
# -----------------------------------------------------------------------------#
function get_abstract_scene_dataset() {
    local outpath="$scriptdir/AbstractScenes_v1"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Abstract Scenes Dataset"
    local data_url='http://ftp.research.microsoft.com/downloads/73537628-df14-44e2-847a-45f369131e87/AbstractScenes_v1.zip'
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    unzip $tmpfile -d $scriptdir
    echo $(basename $outpath) >> $scriptdir/.gitignore
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# UIUC Pascal Sentence Dataset
# Reference:
#   Cyrus Rashtchian, Peter Young, Micah Hodosh, and Julia Hockenmaier.
#   Collecting Image Annotations Using Amazon's Mechanical Turk. In Proceedings
#   of the NAACL HLT 2010 Workshop on Creating Speech and Language Data with
#   Amazon's Mechanical Turk.
# -----------------------------------------------------------------------------#
function get_pascal_sentence_dataset() {
    local outpath="$scriptdir/PascalSentences"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Pascal Sentence Dataset"
    local data_url="http://vision.cs.uiuc.edu/pascal-sentences"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    fchop 15 3 $tmpfile
    local class=
    local entry=
    local descn=0
    while IFS= read -r; do
        line="$REPLY"
        case $line in
            \<td\>\<img\ src=*\>\</td\>)
                img=$(echo $line | cut -d\" -f2)
                class="$outpath/$(dirname $img)"
                entry="$(basename $img .jpg)"
                mkdir -p $class
                wget -O $class/img_$class_$entry.jpg $data_url/$img
                descn=0
                ;;
            \<tr\>\<td\>*\</td\>\</tr\>)
                desc=$(chop '<tr><td>' '</td></tr>' "$line")
                descn=$((descn + 1))
                echo $desc > $class/descr_$class_$entry.$descn.txt
                ;;
        esac
    done < $tmpfile
    echo $(basename $outpath) >> $scriptdir/.gitignore
    rm -f $tmpfile
    echo "done"
}


# -----------------------------------------------------------------------------#
# Attribute Discovery Dataset
# Reference:
#   Tamara L. Berg, Alexander C. Berg, Jonathan Shih, Automatic Attribute
#   Discovery and Characterization from Noisy Web Images, European Conference
#   on Computer Vision (ECCV), 2010
# -----------------------------------------------------------------------------#
function get_attribute_discovery_dataset() {
    local outpath="$scriptdir/AttributeDiscovery"
    [[ -d $outpath ]] && return
    echo_emph "Fetching Attribute Discovery Dataset"
    local data_url="http://www.tamaraberg.com/attributesDataset/attributedata.tar.gz"
    local tmpfile=$(mktemp)
    wget -O $tmpfile $data_url
    mkdir -p $outpath
    tar -vzxf $tmpfile -C $outpath
    echo $(basename $outpath) >> $scriptdir/.gitignore
    rm -f $tmpfile
    echo "done"
}


# parse arguments
[[ -z $@ ]] && echo "please specify dataset to fetch" && exit 1
for arg in "$@"; do
    case $arg in
        zitnick|abstract-scene) get_abstract_scene_dataset ;;
        rashtchian|pascal-sentence) get_pascal_sentence_dataset ;;
        berg|attribute-discovery) get_attribute_discovery_dataset ;;
        *) echo "unknown dataset $arg"
    esac
done
